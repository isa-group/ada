package es.usa.isa.ada.testing.states;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import es.us.isa.ada.document.Range;
import es.us.isa.ada.wsag.values.AgreementState;
import es.us.isa.ada.wsag.values.AgreementStateImpl;

/*
 * Clase para cargar desde fichero properties 
 * un AgreementState
 * ####################
 * ##### ATENCI�N #####
 * ####################
 * Esta clase se mantiene para que funcionen las pruebas ya implementadas
 * pero no es necesario su uso
 */
public class AgreementStateLoader {

	
	public AgreementState loadState(String path){
		AgreementState res = new AgreementStateImpl();
		
		try {
			Properties props = new Properties();
			props.load(new FileInputStream(path));
			
			Set<Object> keys = props.keySet();
			
			for (Object k:keys){
				String propName = k.toString();
				String value = props.getProperty(propName);
				//ahora analizamos el valor para saber si es un valor fijo
				//un rango
				//o un conjunto de enumerados
				if (value.contains("..")){
					//rango
					StringTokenizer st = new StringTokenizer(value, "..");
					String smin = st.nextToken(), smax = st.nextToken();
					int min = Integer.parseInt(smin);
					int max = Integer.parseInt(smax);
					Range r = new Range(min, max);
					res.putVariableValue(propName, value);
				}
				else if (value.contains(",")){
					//enumerado
					StringTokenizer st = new StringTokenizer(value, ",");
					Collection<String> values = new LinkedList<String>();
					while (st.hasMoreTokens()){
						String aux = st.nextToken();
						values.add(aux);
					}
					res.putVariableValue(propName, value);
				}
				else{
					//entero
					int val = Integer.parseInt(value);
					res.putVariableValue(propName, value);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
}
