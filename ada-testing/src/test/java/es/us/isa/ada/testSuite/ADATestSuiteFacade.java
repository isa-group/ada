package es.us.isa.ada.testSuite;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import es.us.isa.ada.Analyzer;
import es.us.isa.ada.Operation;
import es.us.isa.ada.choco.ChocoAnalyzer;
import es.us.isa.ada.choco.questions.ChocoAgreementFulfilmentOp;
import es.us.isa.ada.choco.questions.ChocoComplianceOp;
import es.us.isa.ada.choco.questions.ChocoConsistencyOp;
import es.us.isa.ada.choco.questions.ChocoDeadTermsOp;
import es.us.isa.ada.choco.questions.ChocoDecomposeIntoViewsOp;
import es.us.isa.ada.choco.questions.ChocoExplainComplianceOp;
import es.us.isa.ada.choco.questions.ChocoExplainDeadTermsOp;
import es.us.isa.ada.choco.questions.ChocoExplainLudicrousTermsOp;
import es.us.isa.ada.choco.questions.ChocoExplainNoConsistencyOp;
import es.us.isa.ada.choco.questions.ChocoExplainViolationOp;
import es.us.isa.ada.choco.questions.ChocoLudicrousTermsOp;
import es.us.isa.ada.choco.questions.ChocoNumberOfAlternativeDocsOp;
import es.us.isa.ada.choco.questions.ChocoQuickxplainNoComplianceOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.io.IDocumentParser;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.DeadTermsOperation;
import es.us.isa.ada.operations.ExplainDeadTerms;
import es.us.isa.ada.operations.ExplainLudicrousTerms;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;
import es.us.isa.ada.operations.LudicrousTermsOperation;
import es.us.isa.ada.wsag10.Term;
import es.us.isa.ada.wsag10.parsers.DefaultWSAgParser;

public class ADATestSuiteFacade {

protected IDocumentParser parser;
	
	private Map<String,Class<? extends Operation>> mapOps;
	
	private Analyzer analyzer;
	
	
	public ADATestSuiteFacade(){
		parser = new DefaultWSAgParser();
		analyzer = new ChocoAnalyzer();
		loadOps();
	}
	
	public Boolean checkDocumentConsistency(String docPath){
		ConsistencyOperation op = (ConsistencyOperation) createOperation("consistency");
		AbstractDocument doc = loadDocument(docPath);
		op.addDocument(doc);
		analyze(op);
		return op.isConsistent();
	}
	
	public Map<AgreementElement, Collection<AgreementElement>> explainInconsistencies(String docPath){
		ExplainNoConsistencyOperation op = (ExplainNoConsistencyOperation) createOperation("explainConsistency");
		AbstractDocument doc = loadDocument(docPath);
		op.addDocument(doc);
		analyze(op);
		return op.explainErrors();
	}
	
	public Collection<Term> getDeadTerms(String docPath){
		DeadTermsOperation op = (DeadTermsOperation) createOperation("deadTerms");
		AbstractDocument doc = loadDocument(docPath);
		op.addDocument(doc);
		analyze(op);
		return op.getDeadTerms();
	}
	
	public Map<Term, Collection<AgreementElement>> explainDeadTerms(String docPath){
		ExplainDeadTerms op = (ExplainDeadTerms) createOperation("explainDeadTerms");
		AbstractDocument doc = loadDocument(docPath);
		op.addDocument(doc);
		analyze(op);
		return op.explainDeadTerms();
	}
	
	public Collection<Term> getLudicrousTerms(String docPath){
		LudicrousTermsOperation op = (LudicrousTermsOperation) createOperation("ludicrousTerms");
		AbstractDocument doc = loadDocument(docPath);
		op.addDocument(doc);
		analyze(op);
		return op.getLudicrousTerms();
	}
	
	public Map<Term, Collection<AgreementElement>> explainLudicrousTerms(String docPath){
		ExplainLudicrousTerms op = (ExplainLudicrousTerms) createOperation("explainLudicrousTerms");
		AbstractDocument doc = loadDocument(docPath);
		op.addDocument(doc);
		analyze(op);
		return op.explainLudicrousTerms();
	}
	
	private void loadOps() {
		mapOps = new HashMap<String, Class<? extends Operation>>();
		mapOps.put("consistency", ChocoConsistencyOp.class);
		mapOps.put("explainConsistency", ChocoExplainNoConsistencyOp.class);
		mapOps.put("compliance", ChocoComplianceOp.class);
		mapOps.put("explainCompliance", ChocoQuickxplainNoComplianceOp.class);
		mapOps.put("slowExplainCompliance", ChocoExplainComplianceOp.class);
		mapOps.put("Fulfilment", ChocoAgreementFulfilmentOp.class);
		mapOps.put("ViolationExps", ChocoExplainViolationOp.class);
		mapOps.put("OpViews", ChocoDecomposeIntoViewsOp.class);
		mapOps.put("deadTerms", ChocoDeadTermsOp.class);
		mapOps.put("explainDeadTerms", ChocoExplainDeadTermsOp.class);
		mapOps.put("ludicrousTerms", ChocoLudicrousTermsOp.class);
		mapOps.put("explainLudicrousTerms", ChocoExplainLudicrousTermsOp.class);
		mapOps.put("numberOfAlternatives", ChocoNumberOfAlternativeDocsOp.class);
//		mapOps.
	}


	private AbstractDocument loadDocument(String path){
		return parser.parseFile(path);
	}
	
	private Operation createOperation(String id){
		Class<? extends Operation> clazz = mapOps.get(id);
		Operation res = null;
		if (clazz != null){
			try {
				res = clazz.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	
	private void analyze(Operation op){
		analyzer.analyze(op);
	}
	
	private Collection<String> getOperationIds(){
		return mapOps.keySet();
	}
	
}
