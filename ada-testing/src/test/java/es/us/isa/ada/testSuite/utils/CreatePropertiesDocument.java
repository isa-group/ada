package es.us.isa.ada.testSuite.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import es.us.isa.ada.exceptions.BadSyntaxException;

public class CreatePropertiesDocument {
	
	private static final String PATH = "black-box-tests/";
//	private static final String CONSISTENCY_PATH = "black-box-tests/01 CheckDocumentConsistency (without temp)/";
//	private static final String EXPLAIN_CONSISTENCY_PATH = "black-box-tests/02 ExplainInconsistencies (without temp)/";
//	private static final String GET_DEAD_TERMS_PATH = "black-box-tests/03 GetDeadTerms (without temp)/";
//	private static final String EXPLAIN_DEAD_TERMS_PATH = "black-box-tests/04 ExplainDeadTerms (without temp)/";
//	private static final String GET_LUDICROUS_TERMS_PATH = "black-box-tests/05 GetLudicrousTerms (without temp)/";
//	private static final String EXPLAIN_LUDICROUS_TERMS_PATH = "black-box-tests/06 ExplainLudicrousTerms (without temp)/";
//	private static final String COMPLIANCE_PATH = "black-box-tests/07 IsCompliant (without temp)/";
//	private static final String EXPLAIN_COMPLIANCE_PATH = "black-box-tests/08 ExplainNonCompliance (without temp)/";
		
	public static void main(String args[]){
		createPropertiesDocuments();
	}
	
	private static void createPropertiesDocuments(){
		setFiles(new File(PATH));
	}
	
	// m�todo que crea un �nico properties
	// el path del documento es la key
//	private static Collection<File> setFiles(File path, String propsPath){
//		Collection<File> files = new LinkedList<File>();
//		File props = new File(path+"\\expectedResults.properties");
//		// si exist�a lo borramos
//		if(props.exists()){
//			props.delete();
//		}
//		File[] subfiles = path.listFiles();
//		for (int i = 0; i < subfiles.length; i++) {
//			if(subfiles[i].isDirectory()){
//				files.addAll(setFiles(subfiles[i], propsPath));
//			}else{
//				if(!subfiles[i].getPath().endsWith(".svn") && !subfiles[i].getPath().endsWith(".properties")){
//					Object result = parseResult(subfiles[i].getName());
//					try{						
//						FileWriter fw = new FileWriter(propsPath, true);
//						BufferedWriter bw = new BufferedWriter(fw);
//						bw.write(subfiles[i].getPath()+" = "+result.toString()+"\n");
//						bw.close();
//					}catch(IOException e){
//						e.printStackTrace();
//					}
//					files.add(subfiles[i]);
//				}
//			}
//		}
//		
//		return files;
//	}
	
	// m�todo que crea un properties en cada carpeta hoja
	private static Collection<File> setFiles(File path){
		Collection<File> files = new LinkedList<File>();
		File[] subfiles = path.listFiles();
		// Si existe un fichero de properties en esta ruta lo borramos
		File props = new File(path.getPath()+"\\expectedResults.properties");
		if(props.exists()){
			props.delete();
		}
		for (int i = 0; i < subfiles.length; i++) {
			if(subfiles[i].isDirectory()){
				files.addAll(setFiles(subfiles[i]));
			}else{
				// hay algunos casos donde el resultado va en
				// el nombre de la carpeta en vez de en el nombre
				// del documento
				if(!subfiles[i].getPath().endsWith(".svn") && !subfiles[i].getPath().endsWith(".properties")){
					Object result = parseResult(subfiles[i].getName(), path.getPath());
					try{						
						FileWriter fw = new FileWriter(path.getPath()+"\\expectedResults.properties", true);
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(subfiles[i].getName()+" = "+result.toString()+"\n");
						bw.close();
					}catch(IOException e){
						e.printStackTrace();
					}
					files.add(subfiles[i]);
				}
			}
		}
		
		return files;
	}
	
	/**
	 * Obtiene el resultado esperado del documento dado bas�ndose
	 * en el nombre del mismo
	 * @param docName
	 */
	private static String parseResult(String docName, String docPath) {
		String result = null;
		String stringResult = null;
		
		Integer initPos = docName.lastIndexOf("RES-")+4; // sumamos cuatro para tener la posici�n justo despu�s de 'RES-'
		if(initPos == 3){
			// si es 3 significa que la llamada a indexOf
			// devolvi� -1 porque no encontr� la subcadena
			// 'RES-'. En este caso buscamos 'RES-' en el
			// nombre de la carpeta
			initPos = docPath.lastIndexOf("RES-")+4;
			try {
				stringResult = docPath.substring(initPos);
			} catch (IndexOutOfBoundsException e) {
				stringResult = "";
			}
		}else{
			Integer endPos = docName.lastIndexOf(".wsag");
			try{			
				stringResult = docName.substring(initPos, endPos);
			}catch(IndexOutOfBoundsException e){
				stringResult = "";
			}
		}
		
		if(stringResult.equalsIgnoreCase("EXCContext")){
			result = new BadSyntaxException().getClass().getName();
		}else{
			result = stringResult;
		}
		return result;
	}
}
