package es.us.isa.ada.testSuite;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.exceptions.BadSyntaxException;
import es.us.isa.ada.exceptions.InconsistenciesException;
import es.us.isa.ada.wsag10.Term;

public class GetLudicrousTermsTest extends ADATest{

private static final String DOCS_FOLDER = "black-box-tests/05 GetLudicrousTerms (without temp)/";
	
	public GetLudicrousTermsTest(String docName, String docPath){
		super(docName, docPath);
	}
	
	@Override
	protected Object analyse(String docPath){
//		if(numberOfTest == 119){
//			System.out.println("Ah� estamos");
//		}
		return ada.getLudicrousTerms(docPath);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		return getParameters(DOCS_FOLDER);
	}

	@Override
	protected Object parseExpectedResult(String docName) {
		Collection<String> result = new LinkedList<String>();
		
		// sacamos el resultado asociado a docName del properties
		Properties props = getPropertiesDocument();
		String stringResult = props.getProperty(docName);
		
		if(stringResult == null){
			System.err.println("El documento "+this.docPath+" no se encuentra en el documento de properties");
		}else if(stringResult.equalsIgnoreCase("es.us.isa.ada.exceptions.BadSyntaxException")){
			return new BadSyntaxException();
		}else if(stringResult.equalsIgnoreCase("NULL")){
			// no hacemos nada porque el resultado que se espera es una colecci�n vac�a
		}else if(stringResult.equalsIgnoreCase("NOT_NULL")){
			// el resultado que se espera es una colecci�n con alg�n resultado,
			// as� que metemos cualquier cadena en la colecci�n
			result.add("Antonio Jurado");
		}else{
			// la cadena obtenida del properties tiene el formato: deadTerm1, deadTerm2, deadTerm3...
			String regex = "\\w+(-\\w+)*";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(stringResult);
			while(matcher.find()){
				result.add(matcher.group());
			}
		}
		
		return result;
	}

	@Override
	public void assertResult(Object expected, Object result) {
		Boolean iguales = null;
		if(expected == null || result == null){
			assertTrue("Expected or result is null - "+getTestInfo(), false);
		}else if(expected instanceof Exception && result instanceof Exception){
			iguales = expected.getClass().getName().equalsIgnoreCase(result.getClass().getName());
		}else if(expected instanceof Collection && result instanceof Collection){
			Collection<String> exp = (Collection<String>) expected;
			Collection<Term> res = (Collection<Term>) result;
			
			// ambos resultados deben tener el mismo tama�o
			if(exp.size() != res.size()){
				iguales = false;
			}else{
				iguales = true;
				// por cada t�rmino del resultado obtenido comprobamos que est� en el resultado esperado
				for(Term t: res){
					if(!exp.contains(t.getName())){
						iguales = false;
					}
				}
			}
		}else if(result instanceof InconsistenciesException && expectedResult instanceof Collection){
			Collection<String> exp = (Collection<String>) expectedResult;
			if(exp.isEmpty()){
				// en caso de que el resultado esperado sea una
				// colecci�n vac�a y devuelva una excepci�n por inconsistencias,
				// consideramos este caso como bueno
				iguales = true;
			}else{
				iguales = false;
			}
		}else{
//			if(result instanceof Exception){
//				((Exception) result).printStackTrace();
//			}
			iguales = false;
		}
		if(!iguales){
			System.out.println("\n"+getTestInfo());
			System.out.println("   ==> Expected "+expected+" but was "+result);
		}
		assertTrue("Expected "+expected+" but was "+result+" - "+getTestInfo(), iguales);
	}
}
