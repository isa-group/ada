package es.us.isa.ada.testSuite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public abstract class ADATest {
	
	protected ADATestSuiteFacade ada;
	
	protected String docName;
	
	protected String docPath;
	
	protected Object expectedResult;
	
	/*
	 * Con pruebas parametrizadas no tenemos control sobre los n�meros
	 * que aparecen en la vista JUnit para cada test. Con este indice
	 * numeramos cada test con el n�mero que tiene el documento en el
	 * excel de google docs.
	 */
	protected static Integer numberOfTest = 0;
	
	public ADATest(String docName, String docPath){
		this.docName = docName;
		this.docPath = docPath;
//		if(numberOfTest == 18){
//			System.out.println(getTestInfo());
//		}
		this.expectedResult = parseExpectedResult(docName);
		numberOfTest++;
	}
	
	@Before
	public void setUp(){
		ada = new ADATestSuiteFacade();
	}
	
	@Test
	public void test(){
		Object result = null;
		try{
			result = analyse(docPath);
		}catch(Exception e){
			result = e;
		}
		assertResult(expectedResult, result);
	}
	
	protected abstract Object analyse(String docPath);
	
	/**
	 * Crea un objeto con el resultado esperado asociado al documento dado
	 * @param docName
	 * @return resultado esperado
	 */
	protected abstract Object parseExpectedResult(String docName);
	
	/**
	 * Compara dos resultados
	 * @param expected
	 * @param result
	 */
	protected abstract void assertResult(Object expected, Object result);
	
	protected static Collection<Object[]> getParameters(String path){
		String docName, docPath;
		Collection<Object[]> res = new LinkedList<Object[]>();
		
		File filesPath = new File(path);
		Collection<File> files = getFiles(filesPath);
		for(File f: files){
			docName = f.getName();
			docPath = f.getPath();
			
			Object[] o = {docName, docPath};
			res.add(o);
		}
		return res;
	}
	
	/**
	 * Recorre las carpetas de los documentos para cargarlos todos
	 * @param path
	 * @return todos los documentos dentro del �rbol de carpetas de path
	 */
	protected static Collection<File> getFiles(File path){
		Collection<File> files = new LinkedList<File>();
		File[] subfiles = path.listFiles();
		for (int i = 0; i < subfiles.length; i++) {
			if(!subfiles[i].getPath().endsWith(".svn")){
				if(subfiles[i].isDirectory()){
					files.addAll(getFiles(subfiles[i]));
				}else{
					if(!subfiles[i].getPath().endsWith(".properties")){
						files.add(subfiles[i]);
					}
				}
			}
		}
		
		return files;
	}
	
	protected Properties getPropertiesDocument(){
		Properties props = new Properties();
		try {
			String propsPath = docPath.substring(0, docPath.lastIndexOf("\\"));
			props.load(new FileInputStream(propsPath+"\\expectedResults.properties"));
		} catch (FileNotFoundException e) {
			System.err.println("Documento de properties no encontrado");
		} catch (IOException e) {
			System.err.println("Documento de properties no encontrado");
		}
		return props;
	}
	
	protected String getTestInfo(){
		String res = numberOfTest+". "+docName+" - "+docPath;
		return res;
	}

}
