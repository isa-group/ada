package es.us.isa.ada.testSuite.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternsGruposTest {

        public static void main(String[] args) {

            /*PROGRAMA HECHO EN LENGUAJE PERL
            $_="Renacimiento: Victoria (1548-1611), Guerrero (1527-1599) y Morales (1500-1553)";
            $fecha="\\d\\d\\d\\d"; # Cadena alternativa �\d\d\d\d�
            print "[$1 - $2]\n" if /($fecha)-($fecha)/; # imprime [1548 - 1611]
            print "[$1-$3]\n" if /((\d\d)00)-\2(\d\d)/; # imprime [1500-53]
            */
            Matcher mat=null;
            Pattern pat=null;

            String texto="Renacimiento: Victoria (1548-1611), Guerrero (1527-1599) y Morales (1500-1553)";
            String fecha="\\d\\d\\d\\d";
            pat=Pattern.compile("("+fecha+")-("+fecha+")");
            mat=pat.matcher(texto);
            if(mat.find()) System.out.println("["+mat.group()+"]");

            pat=Pattern.compile("((\\d\\d)00)-\\2(\\d\\d)");
            mat=pat.matcher(texto);
//            if(mat.find()) System.out.println("["+mat.group(1)+" - "+mat.group(3)+"]");
            if(mat.find()) System.out.println(mat.group(0)+", "+mat.group(1)+", "+mat.group(2)+", "+mat.group(3));

        }
}
