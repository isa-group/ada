package testing.SLARepo;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.DecomposeIntoViewsOperation;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;
import es.us.isa.ada.operations.NumberOfAlternativeDocsOperation;
import es.us.isa.ada.wsag10.AbstractAgreementDocument;
import es.us.isa.ada.wsag10.GuaranteeTerm;
import es.us.isa.ada.wsag10.ServiceScope;
import es.us.isa.ada.wsag10.Term;

@RunWith(Parameterized.class)
public class SLARepository {
	
	private ADATestingFacade ada;
	
	private String docName;
	
	private String docPath;
	
	public SLARepository(String docName, String docPath){
		this.docPath = docPath;
		this.docName = docName;
	}

	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}
	
	@Test
	public void getAllInformation(){
		//hay que pillar:
		// - #GT (Con getAllTerms y parseando)
		// - #Variantes (Con NumberOfAlternativeDocs)
		// - #Scopes (Con DecomposeIntoViews)
		// - #Conflicts (Con explainConsistency)
		Integer numGt = 0, numVariants = 0, numScopes = 0, numConflicts = 0;
		//Load the document
		AbstractAgreementDocument doc = (AbstractAgreementDocument)ada.loadDocument(docPath);
		//Number of GT
		Collection<Term> terms = doc.getAllTerms();
		for(Term t:terms){
			if(t instanceof GuaranteeTerm){
				numGt++;
			}
		}
		
		//Number of alternative documents
		NumberOfAlternativeDocsOperation altDocsOp = (NumberOfAlternativeDocsOperation) ada.createOperation("numberOfAlternatives");
		altDocsOp.addDocument(doc);
		ada.analyze(altDocsOp);
		numVariants = altDocsOp.getNumberOfAlternativeDocs();
		
//		//Number of views
//		DecomposeIntoViewsOperation viewsOp = (DecomposeIntoViewsOperation) ada.createOperation("OpViews");
//		viewsOp.addDocument(doc);
//		ada.analyze(viewsOp);
//		Map<ServiceScope, AbstractDocument> views = viewsOp.getScopes2Views();
//		numScopes = views.keySet().size();
//		
//		//Number of conflicts
//		//First, check consistency
////		ConsistencyOperation consOp = (ConsistencyOperation) ada.createOperation("consistency");
////		consOp.addDocument(doc);
////		ada.analyze(consOp);
////		Boolean isConsistent = consOp.isConsistent();
////		System.out.println("�Documento "+docName+" es consistente? "+isConsistent);
////		//If it�s not consistent then explain conflicts
////		if(!isConsistent){
////			ExplainNoConsistencyOperation expConsOp = (ExplainNoConsistencyOperation) ada.createOperation("explainConsistency");
////			expConsOp.addDocument(doc);
////			ada.analyze(expConsOp);
////			Map<AgreementElement, Collection<AgreementElement>> conflicts = expConsOp.explainErrors();
////			numConflicts = conflicts.keySet().size();
////		}
		
		//Print results
		System.out.println(generateHTMLForANewRow(docName, numGt.toString(), numVariants.toString(), numScopes.toString(), numConflicts.toString(), docPath));
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> params = new LinkedList<Object[]>();
		
		File f = new File("front-end");
		File[] subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			String name = subfiles[i].getName();
			if (!path.endsWith(".svn")){
				Object[] aux = {name, path};
				params.add(aux);
			}
		}
		return params;
	}
	
	private String generateHTMLForANewRow(String nombre, String nGt, String nVars, String nViews, String nConfs, String path){
		String tabulador = "  ";
		StringBuilder sb = new StringBuilder();
		sb.append(tabulador+tabulador+tabulador+"<tr>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td class=\"nombre\">"+nombre+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>WS-Agreement</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nGt+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nVars+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nViews+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nConfs+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td><a href=\"http://www.isa.us.es/ada.source/docs/"+nombre+"\">Download</a></td>\n");
		sb.append(tabulador+tabulador+tabulador+"</tr>\n");
		return sb.toString();
	}
}
