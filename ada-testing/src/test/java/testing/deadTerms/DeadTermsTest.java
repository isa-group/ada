package testing.deadTerms;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.DeadTermsOperation;
import es.us.isa.ada.operations.ExplainDeadTerms;
import es.us.isa.ada.wsag10.Term;

@RunWith(Parameterized.class)
public class DeadTermsTest {

	private String path;
	private Boolean expectedResult;
	
	private ADATestingFacade ada;
	
	public DeadTermsTest(String path, Boolean expectedResult){
		this.path = path;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> param = new LinkedList<Object[]>();
		File f = new File("black-box-tests/04 ExplainDeadTerms (without temp)/3 ErrorGuess/1 Templates/");
		
		File[] subfiles = f.listFiles();
		for(File aux:subfiles){
			if(!aux.getAbsolutePath().endsWith(".svn") && !aux.getAbsolutePath().endsWith(".properties")){
				if(aux.getPath().endsWith("07_DT_by_SDT_RES-NOT_NULL.wsag")){
					Object[] doc = new Object[] {aux.getPath(), Boolean.TRUE};
					param.add(doc);
				}
			}
		}
		return param;
	}
	
	@Test
	public void testConsistency(){
		AbstractDocument doc = ada.loadDocument(path);
		DeadTermsOperation op = (DeadTermsOperation) ada.createOperation("deadTerms");
		op.addDocument(doc);
		ada.analyze(op);
		Collection<Term> terms = op.getDeadTerms();
		System.out.println("Result: "+terms+" Document: "+path);
		if(!terms.isEmpty()){
			ExplainDeadTerms expOp = (ExplainDeadTerms) ada.createOperation("explainDeadTerms");
			expOp.addDocument(doc);
			expOp.setDeadTerms(terms);
			ada.analyze(expOp);
			System.out.println("  => Explanations: "+expOp.explainDeadTerms());
		}
	}
}