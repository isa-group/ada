package testing.dates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import es.us.isa.ada.exceptions.BadSyntaxException;

public class ParseoFechas {

	public static void main(String[] args){
		
		//Creamos la fecha
		String fecha = "05/01/2012";
		System.out.println("La fecha es: "+fecha);
		//El GMT
		String gmt = "+1";
		//Y DateFormat
		String dateFormat = "MM/DD/YYYY";
		
		parseDate(fecha, gmt, dateFormat);
	}
	
	private static Calendar parseDate(String dateString, String gmtZone, String dateFormat){
//		Calendar result = Calendar.getInstance(TimeZone.getTimeZone("GMT"+gmtZone));
		DateFormat formatter = null;
		// solo permitimos los formatos MM/DD/YYYY y DD/MM/YYYY
		// le a�adimos el gmt zone para el parseo
		dateString = dateString+":GMT"+gmtZone;
		if(dateFormat.equalsIgnoreCase("MM/DD/YYYY")){
			formatter = new SimpleDateFormat("MM/dd/yyyy");
		}else if(dateFormat.equalsIgnoreCase("DD/MM/YYYY")){
			formatter = new SimpleDateFormat("dd/MM/yyyy");
		}
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"+gmtZone));
		Date date;
		try{
			date = formatter.parse(dateString);
		}catch(ParseException e){
			e.printStackTrace();
			throw new BadSyntaxException("There was a problem parsing "+dateString);
		}catch(NullPointerException e){
			throw new BadSyntaxException("There was a problem parsing DateFormat. Just MM/DD/YYYY and DD/MM/YYYY patterns can be used");
		}
		Calendar result = Calendar.getInstance();
		result.setTime(date);
		return result;
	}
}
