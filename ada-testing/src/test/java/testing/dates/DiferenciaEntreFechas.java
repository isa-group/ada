package testing.dates;

import java.util.Calendar;

public class DiferenciaEntreFechas
{
    public static void main(String[] args)
    {
        // Crear 2 instancias de Calendar
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        // Establecer las fechas
        cal1.set(2011, 10, 4, 0, 00);
        cal2.set(2011, 10, 11, 0, 00);

        // conseguir la representacion de la fecha en milisegundos
        long milis1 = cal1.getTimeInMillis();
        long milis2 = cal2.getTimeInMillis();

        // calcular la diferencia en milisengundos
        long diff = milis2 - milis1;

        // calcular la diferencia en segundos
        long diffSeconds = diff / 1000;

        // calcular la diferencia en minutos
        long diffMinutes = diff / (60 * 1000);

        // calcular la diferencia en horas
        long diffHours = diff / (60 * 60 * 1000);

        // calcular la diferencia en dias
        long diffDays = diff / (24 * 60 * 60 * 1000);

        System.out.println("En milisegundos: " + diff + " milisegundos");
        System.out.println("En segundos: " + diffSeconds + " segundos");
        System.out.println("En minutos: " + diffMinutes + " minutos");
        System.out.println("En horas: " + diffHours + " horas");
        System.out.println("En dias: " + diffDays + " dias");
    }
}