package testing.compliance.conflictive;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import testing.wsag4j.TestingFacadeWsag4j;
import es.us.isa.ada.choco.ChocoAnalyzer;
import es.us.isa.ada.choco.questions.ChocoQuickxplainNoComplianceOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.ComplianceOperation;

@RunWith(Parameterized.class)
public class ConflictiveComplianceTests {

	private ADATestingFacade ada;
	
	private String templatePath;
	private String offerPath;
	private boolean res;
	
	private static final String templatesPath = "front-end/";
	private static final String offersPath = "front-end/";
	
	public ConflictiveComplianceTests(String template, String offer, boolean res){
		this.templatePath = templatesPath+template;
		this.offerPath = offersPath+offer;
		this.res = res;
	}
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
			
		res.add(new Object[]{"Template(term_errors_by_several_GTs_and_CCs).wsag", "AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag", Boolean.FALSE});
		res.add(new Object[]{"Template(term_errors_by_several_GTs_and_CCs).wsag", "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag", Boolean.FALSE});
		res.add(new Object[]{"Template(term_errors_by_several_GTs_and_CCs).wsag", "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag", Boolean.FALSE});
		
		res.add(new Object[]{"Template(term_errors_by_SDT_and_CC_with_themselves).wsag", "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag", Boolean.FALSE});
		res.add(new Object[]{"Template(term_errors_by_SDT_and_CC_with_themselves).wsag", "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag", Boolean.FALSE});
		
		res.add(new Object[]{"ConsistentTemplate(with_deadTerm_GT_CC).wsag", "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag", Boolean.TRUE});
		res.add(new Object[]{"ConsistentTemplate(with_deadTerm_GT_CC).wsag", "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag", Boolean.FALSE});
		
		res.add(new Object[]{"Template(warning_by_SDT_CC).wsag", "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag", Boolean.FALSE});
		res.add(new Object[]{"Template(warning_by_SDT_CC).wsag", "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag", Boolean.FALSE});
		
		res.add(new Object[]{"Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag", "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag", Boolean.FALSE});
		res.add(new Object[]{"Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag", "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag", Boolean.FALSE});
		res.add(new Object[]{"Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag", "AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag", Boolean.FALSE});

		return res;
	}
	
	@Test
	public void test1(){
		AbstractDocument template = ada.loadDocument(templatePath);
		AbstractDocument offer = ada.loadDocument(offerPath);
		
		ComplianceOperation op = (ComplianceOperation)ada.createOperation("compliance");
		op.addDocument(template);
		op.addDocument(offer);
		ada.analyze(op);
		boolean isCompliant = op.isCompliant();
		System.out.println("Vamos a comprobar "+templatePath+" contra "+offerPath+"\n     Resultado esperado "+res+" resultado obtenido "+isCompliant+"\n");
		
		if (isCompliant != res){
			ChocoQuickxplainNoComplianceOp qop = new ChocoQuickxplainNoComplianceOp();
			qop.addDocument(template);
			qop.addDocument(offer);
			qop.setExplanationLevel(ChocoQuickxplainNoComplianceOp.REFINE_ALL);
			
			qop.execute(new ChocoAnalyzer());
			Map<AgreementError,Explanation> res = qop.explainErrors();
			Set<Entry<AgreementError,Explanation>> entries = res.entrySet();
			for (Entry<AgreementError,Explanation> entry:entries){
				System.out.print(entry.getKey() + " => " + entry.getValue());
				System.out.println();
			}
			System.out.println();
		}
		assertEquals(res, isCompliant);
	}
	
}
