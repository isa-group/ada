//package testing.ws;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.rmi.RemoteException;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import es.us.isa.ada.document.AgreementElement;
//import es.us.isa.ada.service.ADAServiceV2PortType;
//import es.us.isa.ada.service.ADAServiceV2PortTypeProxy;
//import es.us.isa.ada.service.AgreementElement2ArrayOfAgreementElementMapEntry;
//import es.us.isa.ada.service.Term2ArrayOfAgreementElementMapEntry;
//import es.us.isa.ada.wsag10.Term;
//
//public class WSTesting {
//
//	private ADAServiceV2PortType ada;
//	
//	@Before
//	public void setUp(){
////		ADAServiceV2 locator = new ADAServiceV2Locator();
////		try {
//			ada = new ADAServiceV2PortTypeProxy("http://150.214.188.147:8081/ADAService");
////			ada = locator.getADAServiceV2Port();
////		} catch (ServiceException e) {
////			e.printStackTrace();
////		}
//	}
//	
//	@Test
//	public void ludicrousTermsTest(){
//		byte[] bytes = getBytesFromFile(new File("ws-files/Ludicrous Term CC.wsag"));
//		testLudicrousTerms(bytes);
//	}
//	
//	@Test
//	public void consistencyTest(){
//		byte[] bytes = getBytesFromFile(new File("ws-files/vbleSP-vblesSDT.wsag"));
//		testConsistency(bytes);
//	}
//	
//	@Test
//	public void deadTermsTest(){
//		byte[] bytes = getBytesFromFile(new File("ws-files/Dead Term intrinseco.wsag"));
//		testDeadTerms(bytes);
//	}
//	
//	private void testConsistency(byte[] bytes){
//		try {
////			Term[] res = ada.getLudicrousTerms(bytes);
//			boolean res = ada.checkDocumentConsistency(bytes);
//			if (!res){
//				AgreementElement2ArrayOfAgreementElementMapEntry[] explainers = ada.explainInconsistencies(bytes);
//				for (AgreementElement2ArrayOfAgreementElementMapEntry ag:explainers){
//					System.out.print(ag.getKey().getName() + " => ");
//					AgreementElement[] ags = ag.getValue();
//					for (AgreementElement ag2:ags){
//						System.out.print(ag2.getName() + " ");
//					}
//					System.out.println();
//					assert true;
//				}
//			}
//		} catch (RemoteException e) {
//			e.printStackTrace();
//			assert false;
//		}
//	}
//	
//	private void testLudicrousTerms(byte[] bytes){
//		try {
//			Term[] res = ada.getLudicrousTerms(bytes);
//			if (res.length > 0){
//				Term2ArrayOfAgreementElementMapEntry[] explainers = ada.explainLudicrousTerms(bytes, res);
//				for (Term2ArrayOfAgreementElementMapEntry t:explainers){
//					System.out.print(t.getKey().getName() + " => ");
//					AgreementElement[] ags = t.getValue();
//					for (AgreementElement ag:ags){
//						System.out.print(ag.getName() + " ");
//					}
//					System.out.println();
//				}
//				assert true;
//			}
//			
////			assert (res.length > 0);
//		} catch (RemoteException e) {
//			e.printStackTrace();
//			assert false;
//		}
//	}
//	
//	private void testDeadTerms(byte[] bytes){
//		try {
//			Term[] res = ada.getDeadTerms(bytes);
//			Term2ArrayOfAgreementElementMapEntry[] explainers = ada.explainDeadTerms(bytes, res);
//			for (Term2ArrayOfAgreementElementMapEntry t:explainers){
//				System.out.print(t.getKey().getName() + " => ");
//				AgreementElement[] ags = t.getValue();
//				for (AgreementElement ag:ags){
//					System.out.print(ag.getName() + " ");
//				}
//				System.out.println();
//			}
//			assert true;
//		} catch (RemoteException e) {
//			e.printStackTrace();
//			assert false;
//		}
//	}
//	
//	@Test
//	public void testConflictiveFiles(){
////		byte[] bytes = getBytesFromFile(new File("files-to-test-deploy/consistent_template.wsag"));
////		testLudicrousTerms(bytes);
////		testConsistency(bytes);
////		testDeadTerms(bytes);
//		byte[] bytes = getBytesFromFile(new File("files-to-test-deploy/template_term_several_errors.wsag"));
//		testLudicrousTerms(bytes);
//		testDeadTerms(bytes);
//		//excepcion aqui debajo
//		testConsistency(bytes);
//		bytes = getBytesFromFile(new File("files-to-test-deploy/template_warning_by_sdt.wsag"));
//		testLudicrousTerms(bytes);
//		testConsistency(bytes);
//		testDeadTerms(bytes);
//	}
//	
//	private byte[] getBytesFromFile(File f){
//		int size = (int) f.length();
//		byte[] res = new byte[size];
//		InputStream in;
//		try {
//			in = new FileInputStream(f);
//			in.read(res);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return res;
//	}
//	
//}
