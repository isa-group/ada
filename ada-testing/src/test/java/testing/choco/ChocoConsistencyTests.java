package testing.choco;

import org.junit.Before;
import org.junit.Test;

import es.us.isa.ada.choco.ChocoAnalyzer;
import es.us.isa.ada.choco.questions.ChocoConsistencyOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.wsag10.parsers.DefaultWSAgParser;


public class ChocoConsistencyTests {

	private AbstractDocument doc;
	
	private DefaultWSAgParser parser;
	
//	private ChocoTranslator t;
	
	private ChocoAnalyzer ca;
	
	private ConsistencyOperation op;
	
	
	@Before
	public void setUp(){
		parser = new DefaultWSAgParser();
		ca = new ChocoAnalyzer();
		op = new ChocoConsistencyOp();	
	}
	
////	@Test
//	public void test1(){
//		doc = parser.parseFile("resources/Figure 1a - (Template).wsag");
//		t = new ChocoTranslator(doc);
//		t.translate();
//		System.out.println("�Todo Ok?");
//	}
	
	@Test
	public void test1(){
		op = new ChocoConsistencyOp();
		doc = parser.parseFile("resources/Figure 1a - (Template).wsag");
		op.addDocument(doc);
		ca.analyze(op);
		boolean consistent = op.isConsistent();
		System.out.println("Is the document consistent?: "+consistent);
		assert (consistent == true);
	}
	
	@Test
	public void test2(){
		op = new ChocoConsistencyOp();
		doc = parser.parseFile("resources/Figure 1c - (Compliant Agreement Offer).wsag");
		op.addDocument(doc);
		ca.analyze(op);
		boolean consistent = op.isConsistent();
		System.out.println("Is the document consistent?: "+consistent);
		assert (consistent == true);
	}
	
	@Test
	public void test3(){
		op = new ChocoConsistencyOp();
		doc = parser.parseFile("resources/Figure 1d - (Non-Compliant Agreement Offer).wsag");
		op.addDocument(doc);
		ca.analyze(op);
		boolean consistent = op.isConsistent();
		System.out.println("Is the document consistent?: "+consistent);
		assert (consistent == true);
	}
	
	
}
