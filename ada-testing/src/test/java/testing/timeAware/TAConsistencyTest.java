package testing.timeAware;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareConsistencyOp;
import es.us.isa.ada.wsag10.AbstractAgreementDocument;
import es.us.isa.ada.wsag10.GuaranteeTerm;
import es.us.isa.ada.wsag10.Template;
import es.us.isa.ada.wsag10.Term;
import es.us.isa.ada.wsag10.TermCompositor;
import es.us.isa.ada.wsag10.timeAwareWsag.TimeAwareContext;
import es.us.isa.ada.wsag10.timeAwareWsag.TimeAwareGuaranteeTerm;
import es.us.isa.temporalAlgorithms.RealTemporalInterval;
import es.us.isa.temporalAlgorithms.SinglePeriodicRealTemporalInterval;
import es.us.isa.temporalAlgorithms.SingleRealTemporalInterval;
import es.us.isa.temporalAlgorithms.TemporalMetric;

@RunWith(Parameterized.class)
public class TAConsistencyTest {
	
	private ADATestingFacade ada;
	
	private List<RealTemporalInterval[]> intervals;
	
	private static final String DOCUMENTS_PATH = "frontendDefaultDocuments/";
	
	private String docPath;
	
	private String docName;
	
	private Collection<String> termsNameToAddTemporality;
	
	public TAConsistencyTest(String documentPath, String documentName, Collection<String> termsNameToAddTemporality){
		this.docPath = documentPath;
		this.docName = documentName;
		this.termsNameToAddTemporality = termsNameToAddTemporality;
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
		Collection<String> terms = new LinkedList<String>();
		
		terms.add("TranslationTime1");
		terms.add("TranslationTime2");
		Object[] o1 = {"templates/ConsistentTemplate.wsag", "ConsistentTemplate.wsag", terms};
	
		res.add(o1);
		
		return res;
	}

	@Before
	public void setUp(){
		ada = new ADATestingFacade();
		intervals = new ArrayList<RealTemporalInterval[]>();
		setIntervals();
	}
	
	@Test
	public void testConsistency(){
		Template template = (Template) parseaDocumento(DOCUMENTS_PATH+docPath);
		for(int i=0; i<intervals.size(); i++){
			// print
			System.out.println("########################### TestCase 1 ###########################");
			System.out.println("     Document: "+docName);
			System.out.println("     Con temporalidad en: "+termsNameToAddTemporality);
			
			addGlobalPeriodToDoc(i, template);
			addPeriodsToTerms(i, template, termsNameToAddTemporality);
			
			Boolean isConsistent = checkConsistency(template);

			// print
			if(isConsistent){
				System.out.println("     Resultado: Consistente");
			}else{
				System.out.println("     Resultado: Conflictivo");
			}
			System.out.println("##################################################################");
		}
	}
	
	private void addGlobalPeriodToDoc(int intervalsIndex, AbstractAgreementDocument doc){
		// Global period
		TimeAwareContext tac = new TimeAwareContext(doc.getContext());
		tac.setGlobalPeriod(intervals.get(intervalsIndex)[0]);
		doc.setContext(tac);
	}
	
	private void addPeriodsToTerms(int intervalsIndex, AbstractAgreementDocument doc, Collection<String> termsName){
		// empezamos en uno porque el cero es el globalValidityPeriod
		int i = 1;
		TermCompositor tc = doc.getTerms();
		Collection<Term> termsToAdd = new LinkedList<Term>();
		Iterator<Term> it = tc.getComprisedTerms().iterator();
		while(it.hasNext()){
			Term t = it.next();
			if(t instanceof GuaranteeTerm){
				if(termsName.contains(t.getTermName())){
					TimeAwareGuaranteeTerm tagt = new TimeAwareGuaranteeTerm((GuaranteeTerm) t);
					tagt.setValidityPeriod(intervals.get(intervalsIndex)[i]);
					it.remove();
					termsToAdd.add(tagt);
					i++;
				}
			}
		}
		for(Term t: termsToAdd){
			tc.addComprisedTerm(t);
		}
	}
	
//	private void addPeriodsToCC(int intervalsIndex, AbstractAgreementDocument doc){
//		
//	}
	
	private void setIntervals(){
		Calendar c1, c2, c3, c4;
		RealTemporalInterval gvp, lvp1, lvp2, lvp3;
		
		//Interval array 1: consistent with temporal warning
		c1 = Calendar.getInstance(); c1.set(2011,3,11,9,0,0); c1.set(Calendar.MILLISECOND,0);
		lvp1 = new SinglePeriodicRealTemporalInterval(c1,7,TemporalMetric.HOURS,5,1,TemporalMetric.DAYS);
		c2 = Calendar.getInstance(); c2.set(2011,3,11,15,0,0); c2.set(Calendar.MILLISECOND,0);
		lvp2 = new SinglePeriodicRealTemporalInterval(c2,3,TemporalMetric.HOURS,5,1,TemporalMetric.DAYS);
		c3 = Calendar.getInstance(); c3.set(2011,3,11,0,0,0); c3.set(Calendar.MILLISECOND,0);
		gvp = new SingleRealTemporalInterval(c3,7,TemporalMetric.DAYS);
		
		RealTemporalInterval[] aux1 = {gvp, lvp1, lvp2};
		intervals.add(aux1);
		
		//Interval array 2: consistent
		c1 = Calendar.getInstance(); c1.set(2011,3,11,9,0,0); c1.set(Calendar.MILLISECOND,0);
		lvp1 = new SinglePeriodicRealTemporalInterval(c1,6,TemporalMetric.HOURS,5,1,TemporalMetric.DAYS);
		c2 = Calendar.getInstance(); c2.set(2011,3,11,0,0,0); c2.set(Calendar.MILLISECOND,0);
		lvp2 = new SinglePeriodicRealTemporalInterval(c2,9,TemporalMetric.HOURS,5,1,TemporalMetric.DAYS);
		c3 = Calendar.getInstance(); c3.set(2011,3,11,15,0,0); c3.set(Calendar.MILLISECOND,0);
		lvp3 = new SinglePeriodicRealTemporalInterval(c3,9,TemporalMetric.HOURS,5,1,TemporalMetric.DAYS);
		c4 = Calendar.getInstance(); c4.set(2011,3,11,0,0,0); c4.set(Calendar.MILLISECOND,0);
		gvp = new SingleRealTemporalInterval(c4,5,TemporalMetric.DAYS);
	}
	
	private AbstractAgreementDocument parseaDocumento(String path){
		return (AbstractAgreementDocument) ada.loadDocument(path);
	}
	
	private Boolean checkConsistency(AbstractAgreementDocument doc){
		ChocoTimeAwareConsistencyOp ctac = new ChocoTimeAwareConsistencyOp();
		ctac.addDocument(doc);
		ada.analyze(ctac);
        Assert.assertTrue("hasWarnings is not implemented", false);
//        if(ctac.hasWarnings()){
//			System.out.println("The document "+docName+" has temporal warnings");
//		}
		return ctac.isConsistent();
	}
}
