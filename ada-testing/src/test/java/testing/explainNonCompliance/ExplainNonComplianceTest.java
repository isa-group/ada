package testing.explainNonCompliance;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.choco.questions.ChocoQuickxplainNoComplianceOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ExplainNonComplianceOperation;

public class ExplainNonComplianceTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ADATestingFacade ada = new ADATestingFacade();
		
		String templateName = "explaining-non-compliance/sample-group-carlos/sample3/phdTemplate.wsag";
		String offerName = "explaining-non-compliance/sample-group-carlos/sample3/phdOffer.wsag";
		
		AbstractDocument template = ada.loadDocument(templateName);
		AbstractDocument offer = ada.loadDocument(offerName);
		
		System.out.println(offerName);
		System.out.println(templateName);
		
		ComplianceOperation op1 = (ComplianceOperation) ada.createOperation("compliance");
		op1.addDocument(template);
		op1.addDocument(offer);
		ada.analyze(op1);
		
		if(!op1.isCompliant()){
			ExplainNonComplianceOperation op2 = (ExplainNonComplianceOperation) ada.createOperation("explainCompliance");
			
			op2.addDocument(template);
			op2.addDocument(offer);
			ada.analyze(op2);

			//ChocoQuickxplainNoComplianceOp op2 = new ChocoQuickxplainNoComplianceOp();
			//op2.setExplanationLevel(ChocoQuickxplainNoComplianceOp.REFINE_ALL);
			
			//op2.execute(new ChocoAnalyzer());
			
			ChocoQuickxplainNoComplianceOp exp = (ChocoQuickxplainNoComplianceOp) op2;
			Map<AgreementError,Explanation> res = op2.explainErrors();
			Set<Entry<AgreementError,Explanation>> entries = res.entrySet();
			for (Entry<AgreementError,Explanation> entry:entries){
				System.out.print(entry.getKey() + " => " + entry.getValue());
				System.out.println();
			}
			System.out.println(res);
		}
	}
}