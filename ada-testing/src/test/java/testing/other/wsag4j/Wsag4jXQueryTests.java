package testing.other.wsag4j;

import es.us.isa.ada.choco.ProxyChocoAnalyzer;
import es.us.isa.ada.loaders.BasicExtensionsLoader;
import es.us.isa.ada.repository.ADAAgreementRepository;
import es.us.isa.ada.subfacades.ProxyAnalyzer;
import es.us.isa.ada.users.ADASessionManager;
import es.us.isa.ada.wsag4j.ADA4Wsag4J;
import org.junit.Test;

import java.util.Arrays;

public class Wsag4jXQueryTests {

	@Test
	public void test1(){
		//test sobre un doc que referencia a un agreement properties
		//en un service location
        BasicExtensionsLoader loader = new BasicExtensionsLoader();
        loader.setProxyAnalyzers(Arrays.asList((ProxyAnalyzer) new ProxyChocoAnalyzer()));
        loader.setAgreementRepository(new ADAAgreementRepository());
        loader.setSessionManager(new ADASessionManager());
		ADA4Wsag4J ada = new ADA4Wsag4J(loader);
	}
	
}
