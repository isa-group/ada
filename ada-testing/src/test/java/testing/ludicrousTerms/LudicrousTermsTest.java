package testing.ludicrousTerms;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ExplainLudicrousTerms;
import es.us.isa.ada.operations.LudicrousTermsOperation;
import es.us.isa.ada.wsag10.Term;

@RunWith(Parameterized.class)
public class LudicrousTermsTest {

	private String path;
	private Boolean expectedResult;
	
	private ADATestingFacade ada;
	
	public LudicrousTermsTest(String path, Boolean expectedResult){
		this.path = path;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> param = new LinkedList<Object[]>();
		File f = new File("black-box-tests/05 GetLudicrousTerms (without temp)/3 ErrorGuess/1 Templates/");
		
		File[] subfiles = f.listFiles();
		for(File aux:subfiles){
			if(!aux.getAbsolutePath().endsWith(".svn") && !aux.getAbsolutePath().endsWith(".properties")){
				if(aux.getPath().endsWith("10_LT_by_GC_RES-NOT_NULL.wsag")){
					Object[] doc = new Object[] {aux.getPath(), Boolean.TRUE};
					param.add(doc);
				}
			}
		}
		return param;
	}
	
	@Test
	public void testConsistency(){
		AbstractDocument doc = ada.loadDocument(path);
		LudicrousTermsOperation op = (LudicrousTermsOperation) ada.createOperation("ludicrousTerms");
		op.addDocument(doc);
		ada.analyze(op);
		Collection<Term> terms = op.getLudicrousTerms();
		System.out.println("Result: "+terms+" Document: "+path);
		if(!terms.isEmpty()){
			ExplainLudicrousTerms expOp = (ExplainLudicrousTerms) ada.createOperation("explainLudicrousTerms");
			expOp.addDocument(doc);
			expOp.setLudicrousTerms(terms);
			ada.analyze(expOp);
			System.out.println("  => Explanations: "+expOp.explainLudicrousTerms());
		}
	}
}