package testing.wsag4j;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.wsag10.parsers.WSAg4jParser;

public class TestingFacadeWsag4j extends ADATestingFacade {

	public TestingFacadeWsag4j(){
		super();
		this.parser = new WSAg4jParser();
	}
	
}
