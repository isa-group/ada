package testing.wsag4j;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;

import testing.facade.ADATestingFacade;

@RunWith(Parameterized.class)
public class Wsag4jConsistencyTests {

	private ADATestingFacade ada;
	
	private String path;
	private boolean res;
	
	public Wsag4jConsistencyTests(String path, boolean res){
		this.path = path;
		this.res = res;
	}
	
	@Before
	public void setUp(){
		ada = new TestingFacadeWsag4j();
	}
	
	@Test
	public void test1(){
		runConsistency(path,res);
	}
	
	private void runConsistency(String path, boolean res){
		AbstractDocument doc = ada.loadDocument(path);
		ConsistencyOperation op = 
				(ConsistencyOperation) ada.createOperation("consistency");
		op.addDocument(doc);
		ada.analyze(op);
		boolean isConsistent = op.isConsistent();
		assertEquals(isConsistent,res);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
		
		File f = new File("wsag4j/offers");
		File[] subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			if (!path.endsWith(".svn")){
				Object[] aux = {path,Boolean.TRUE};
				res.add(aux);
			}
		}
		
		f = new File("wsag4j/templates");
		subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			if (!path.endsWith(".svn")){
				Object[] aux = {path,Boolean.TRUE};
				res.add(aux);
			}
		}
		
		f = new File("wsag4j/inconsistencies");
		subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			if (!path.endsWith(".svn")){
				Object[] aux = {path,Boolean.FALSE};
				res.add(aux);
			}
		}
		
		return res;
	}
	
}
