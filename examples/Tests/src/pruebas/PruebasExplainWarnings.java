//package pruebas;
//
//import java.io.File;
//import java.io.FileWriter;
//import java.util.Collection;
//import java.util.Iterator;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import com.csvreader.CsvWriter;
//
//import es.us.isa.ada.document.AbstractDocument;
//import es.us.isa.ada.errors.Explanation;
//import es.us.isa.ada.operations.ExplainNoConsistencyOperation;
//import es.us.isa.ada.operations.ExplainWarningsOperation;
//
//public class PruebasExplainWarnings {
//	
//	boolean errores = true;
//	private ADATestingFacade ada;
//	CsvWriter writercsv = null;
//	File carpeta = new File("pruebasWarnings");
//	String[] lista = carpeta.list();
//	
//	@Before
//	public void setUp() {
//		ada = new ADATestingFacade();
//	}
//	
//	@Test
//	public void test1() throws Exception {
//		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
//		try {
//			File fichero = new File("CSV/explainWarnings-JUNIT.csv");
//			FileWriter fwriter = new FileWriter(fichero);
//			writercsv = new CsvWriter(fwriter, ';');
//			writercsv.write("Nombre del Archivo");
//			writercsv.write("Tipo de Operación");
//			writercsv.write("Contiene Warnings");
//			writercsv.write("Explain");
//			writercsv.write("¿Excepcion?");
//			writercsv.endRecord();
//		} catch (Exception e) {
//			throw e;
//		}
//		/*---------------------------------------------------------------------------------*/
//		for (int i = 0; i < lista.length; i++) {
//			if (lista[i].endsWith(".wsag")) {
//					AbstractDocument doc = ada.loadDocument(carpeta.getName() + "/"+ lista[i]);
//					ExplainWarningsOperation op = (ExplainWarningsOperation) ada.createOperation("explainWarnings");
//					op.addDocument(doc);
//					boolean aux=true;
//					Collection<Explanation> col;
//					Explanation ex=null;
//					boolean excepcion = false;
//					try {
//						ada.analyze(op);
//						col = op.explainWarnings();
//						Iterator<Explanation> it =  col.iterator();
//						
//						if(col.isEmpty()){
//							aux=false;
//						}
//						//solo metemos la primera explanation
//						if(it.hasNext()){
//							ex=it.next();
//						}
//					} catch (Exception e) {
//						aux = false;
//						excepcion = true;
//						e.printStackTrace();
//					}
//					
//					errores = errores && aux;
//					System.out.println("Does the document contains warnings?: " + aux);
//						try {
//						writercsv.write(lista[i]);
//						writercsv.write("ExplainWarnings");
//						writercsv.write("" + aux);
//						writercsv.write("" + ex);
//						writercsv.write("" + excepcion);
//						writercsv.endRecord();
//						writercsv.endRecord();
//
//					} catch (Exception e) {
//						throw e;
//					}			
//					
//			}
//		}
//		/*---------------------------------------------------------------------------------*/
//		if (writercsv != null) {
//			writercsv.close();
//		}
//		assert (errores == true);
//	}
//}
