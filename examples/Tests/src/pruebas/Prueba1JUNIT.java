package pruebas;

import java.io.File;
import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;

public class Prueba1JUNIT{

	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	boolean consistent = false;	
	File carpeta = new File("pruebas(true)");
	String[] lista = carpeta.list();
	

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	
	}

	@Test
	public void test1() throws Exception {
		try {
			File fichero = new File("prueba1-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Resultado Esperado");
			writercsv.write("Resultado Obtenido");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
				AbstractDocument doc = ada.loadDocument(carpeta.getName() + "/"+ lista[i]);
				ConsistencyOperation op = (ConsistencyOperation) ada.createOperation("consistency");
				op.addDocument(doc);
				ada.analyze(op);
				consistent = op.isConsistent();
				System.out.println("Is the offer compliant?: " + consistent);
//				assert (consistent == true);
				try {
					writercsv.write(lista[i]);
					writercsv.write("Consistencia");
					writercsv.write("true");
					writercsv.write("" + consistent);
					writercsv.endRecord();
					writercsv.endRecord();

				} catch (Exception e) {
					throw e;
				}
			}
		}
		if(writercsv!=null) {
			writercsv.close(); 
	    }
		assert (consistent == true);
	}
}