package pruebas.ficheros;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import es.us.isa.ada.wsag10.AgreementDocument;
import es.us.isa.ada.wsag10.parsers.WSAgParser;


public class FileFormatTests {

	private WSAgParser parser;
	
	@Before
	public void setUp(){
		parser = new WSAgParser();
	}
	
	@Test
	public void test1(){
		File dir = new File("fileformat");
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++){
			if (files[i].getName().endsWith(".wsag")){
				AgreementDocument doc = parser.parseFile(files[i].getPath());
				assert (doc != null);
			}
		}
	}
	
}
