1ª PRUEBA: ALL{OR[GT1,GT2],GT3,XOR[GT4,GT5]}->6:
.- Probamos una plantilla como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):6.
   Resultado obtenido(numero de alternative documents):128->SOLUCIONADO:6 OK
   
2ª PRUEBA: XOR(GT1,GT2)->2:
.- Probamos una plantilla como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):2.
   Resultado obtenido(numero de alternative documents):2 OK
   
3ª PRUEBA: OR[OR(GT1,GT2),OR(GT3,GT4)]->15:
.- Probamos una plantilla como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):15
   Resultado obtenido(numero de alternative documents):15 OK
   
4ª PRUEBA: OR[XOR(GT1,GT2),XOR(GT3,GT4)]->8:
.- Probamos una plantilla como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):8
   Resultado obtenido(numero de alternative documents):12->SOLUCIONADO:8 OK
   
5ª PRUEBA: XOR[XOR(GT1,GT2),XOR(GT3,GT4)]->4:
.- Probamos una plantilla como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):4
   Resultado obtenido(numero de alternative documents):8->SOLUCIONADO:4 OK
   
6ª PRUEBA: XOR[OR(GT1,GT2),OR(GT3,GT4)]->6:
.- Probamos una plantilla como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):6
   Resultado obtenido(numero de alternative documents):6 OK
   
7ª PRUEBA: GT1,GT2(OFFER)->1:
.- Probamos una oferta como la que se describe en el nombre.
   Resultado esperado(numero de alternative documents):1 
   Resultado obtenido(numero de alternative documents):1 OK
   

   