package testing.wsag4j;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.rpc.ServiceException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;
import es.us.isa.ada.service.AgreementError2ExplanationMapEntry;

@RunWith(Parameterized.class)
public class Wsag4jComplianceTests {
	
	private ADAServiceV2PortType ada;
	
	private String templatePath;
	private String offerPath;
	private boolean res;
	
	private static final String templatesPath = "Documents/wsag4j/templates/";
	private static final String offersPath = "Documents/wsag4j/offers/";
	
	public Wsag4jComplianceTests(String template, String offer, boolean res){
		this.templatePath = templatesPath+template;
		this.offerPath = offersPath+offer;
		this.res = res;
	}
	
	@Before
	public void setUp(){
		try {
			ada = new ADAServiceV2Locator().getADAServiceV2Port(new URL("http://localhost:8081/ADAService"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
		
		Object[] businessMR = new Object[]{"BusinessMR.xml", "BusinessMR_offer.xml", Boolean.TRUE};
		Object[] validComputeTest = new Object[]{"compute-test.xml", "valid_compute-test-offer.xml", Boolean.TRUE};
		Object[] invalidComputeTest = new Object[]{"compute-test.xml", "invalid_compute-test-offer.xml", Boolean.FALSE};
		Object[] simpleRestriction = new Object[]{"compute-test-simple-restriction.xml", "valid_simple_restriction_offer.xml", Boolean.TRUE};
		Object[] structuralRestriction = new Object[]{"test-structural-restriction.xml", "valid-structural-restriction-offer.xml", Boolean.TRUE};
		
		res.add(businessMR);
		res.add(validComputeTest);
		res.add(invalidComputeTest);
		res.add(simpleRestriction);
		res.add(structuralRestriction);
		return res;
	}
	
	@Test
	public void Compliance(){
		Boolean isCompliant = null;
		try {
			String template = FileUtils.readFileToString(new File(templatePath));
			String offer = FileUtils.readFileToString(new File(offerPath));
			
			isCompliant = ada.isCompliant(template.getBytes(), offer.getBytes());
			System.out.println("\nVamos a comprobar "+templatePath+" contra "+offerPath+"\n     Resultado esperado "+res+" resultado obtenido "+isCompliant);
			
			if (!isCompliant){
				AgreementError2ExplanationMapEntry[] map = ada.explainNonCompliance(template.getBytes(), offer.getBytes());
				for(AgreementError2ExplanationMapEntry entry:map){
					AgreementElement[] errorElements = entry.getKey().getElements();
					AgreementElement[] explanationElements = entry.getValue().getElements();
					for(AgreementElement aeError:errorElements){
						System.out.println("Error: "+aeError.getName());
						for(AgreementElement aeExplanation:explanationElements){
							System.out.println("Explanation: "+aeExplanation.getName());
						}
					}
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(res, isCompliant);
	}
}
