package testing.compliance;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.rpc.ServiceException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;

@RunWith(Parameterized.class)
public class ComplianceTest {
	
	private ADAServiceV2PortType ada;
	
	private String temp;
	private String tempName;
	private String off;
	private String offName;
	private Boolean expectedResult;
	
	public ComplianceTest(String temp, String tempName, String off, String offName, Boolean expectedResult){
		this.temp = temp;
		this.tempName = tempName;
		this.off = off;
		this.offName = offName;
		this.expectedResult = expectedResult;
		try{
			ada = new ADAServiceV2Locator().getADAServiceV2Port(new URL("http://localhost:8081/ADAService"));
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	@Before
	public void setUp(){
		
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		//Colecci�n de par�metros para el test
		Collection<Object[]> params = new LinkedList<Object[]>();
		
		//Directorios donde est�n los documentos
		File trueTempsPath = new File("Documents\\Compliance\\True\\Templates");
		File trueOffsPath = new File("Documents\\Compliance\\True\\Offers");
		File falseTempsPath = new File("Documents\\Compliance\\False\\Templates");
		File falseOffsPath = new File("Documents\\Compliance\\False\\Offers");
		
		//Array de documentos dentro de cada directorio
		File[] trueTemps = trueTempsPath.listFiles();
		File[] trueOffs = trueOffsPath.listFiles();
		File[] falseTemps = falseTempsPath.listFiles();
		File[] falseOffs = falseOffsPath.listFiles();
		
		for(File temp:trueTemps){ //Por cada plantilla compliant
			if(!temp.getAbsolutePath().endsWith(".svn")){
				for(File off:trueOffs){ //Por cada oferta compliant
					if (!off.getAbsolutePath().endsWith(".svn")){
						try {
							String tempContent = FileUtils.readFileToString(temp);
							String offContent = FileUtils.readFileToString(off);
							Object[] comp = new Object[] { tempContent,
									temp.getName(), offContent, off.getName(),
									Boolean.TRUE };
							params.add(comp);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		for(File temp:falseTemps){ //Por cada plantilla no compliant
			if (!temp.getAbsolutePath().endsWith(".svn")) {
				for (File off : falseOffs) { //Por cada oferta no compliant
					if (!off.getAbsolutePath().endsWith(".svn")) {
						try {
							//Sacamos a una cadena el contenido de los documentos
							String tempContent = FileUtils
									.readFileToString(temp);
							String offContent = FileUtils.readFileToString(off);
							//Creamos el array de par�metros que se pasar� a cada instancia del test. Con estos par�metros se llamar� al constructor autom�ticamente
							Object[] comp = new Object[] { tempContent,
									temp.getName(), offContent, off.getName(),
									Boolean.FALSE };
							params.add(comp);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return params;
	}
	
	@Test
	public void test(){
		Boolean isCompliant = null;
		try {
			isCompliant = ada.isCompliant(temp.getBytes(), off.getBytes());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		System.out.println("Documentos: "+tempName+" - "+offName);
		System.out.println("Result: "+isCompliant+", Expected: "+expectedResult);
		assertEquals(expectedResult, isCompliant);
	}
}
