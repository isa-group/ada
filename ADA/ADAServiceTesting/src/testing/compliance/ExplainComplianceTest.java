package testing.compliance;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.rpc.ServiceException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;
import es.us.isa.ada.service.AgreementError2ExplanationMapEntry;

@RunWith(Parameterized.class)
public class ExplainComplianceTest {
	
	private ADAServiceV2PortType ada;
	
	private String temp;
	private String tempName;
	private String off;
	private String offName;
	private Boolean expectedResult;
	
	public ExplainComplianceTest(String temp, String tempName, String off, String offName, Boolean expectedResult){
		this.temp = temp;
		this.tempName = tempName;
		this.off = off;
		this.offName = offName;
		this.expectedResult = expectedResult;
		try{
			ada = new ADAServiceV2Locator().getADAServiceV2Port(new URL("http://localhost:8081/ADAService"));
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	@Before
	public void setUp(){
		
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> params = new LinkedList<Object[]>();
		
		File t = new File("Documents\\Consistency\\True\\ConsistentTemplate.wsag");
		File o = new File("Documents\\Consistency\\True\\ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag");

		String temp = null;
		String off = null;
		try {
			temp = FileUtils.readFileToString(t);
			off = FileUtils.readFileToString(o);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Object[] comp = new Object[]{temp, t.getPath(), off, o.getPath(), Boolean.TRUE};
		params.add(comp);
		return params;
	}
	
	@Test
	public void test(){
		Boolean result = null;
		try {
			AgreementError2ExplanationMapEntry[] map = ada.explainNonCompliance(temp.getBytes(), off.getBytes());
			System.out.println(map);
//			Boolean isCompliant = ada.isCompliant(temp.getBytes(), off.getBytes());
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		System.out.println("Documentos: "+tempName+" - "+offName);
		System.out.println("Result: "+result+", Expected: "+expectedResult);
		assertEquals(expectedResult, result);
	}
}
