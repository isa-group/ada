package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableTransformationToXml extends MeasurableOperation{
	
	private String documentPath;
	private String documentContent;
	private String wsag4peopleContent;
	
	public MeasurableTransformationToXml(String documentName, String documentPath, String operationName){
		super(documentName, operationName);
		this.documentPath = documentPath;
	}

	protected void initialize() {
		documentContent = readFile(documentPath);
		try{
			wsag4peopleContent = new String(ada.xmlToWSAg4People(documentContent.getBytes()));
		}catch(RemoteException e){
			e.printStackTrace();
		}
	}

	protected void finalize() {
	}

	protected void operationToBeMeasured() {
		try{
			ada.wsag4PeopleToXML(wsag4peopleContent.getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la transformación de 4people a xml");
			e.printStackTrace();
		}
	}

}
