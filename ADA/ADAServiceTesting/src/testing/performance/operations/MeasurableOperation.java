package testing.performance.operations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.rpc.ServiceException;

import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;
import etm.contrib.renderer.SimpleHtmlRenderer;
import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.monitor.EtmPoint;
import etm.core.renderer.SimpleTextRenderer;


public abstract class MeasurableOperation{
	
	protected ADAServiceV2PortType ada;
	
	/**
	 * Symbolic name to identify analysed documents in the results
	 */
	private String documentsName;
	
	/**
	 * Symbolic name to identify operation executed in the results
	 */
	protected String operationName;
	
	public MeasurableOperation(String documentsName, String operationName){
		try{
			ada = new ADAServiceV2Locator().getADAServiceV2Port(new URL("http://www.isa.us.es:8081/ADAService"));
		}catch(ServiceException e){
			System.out.println("Error del servicio");
			e.printStackTrace();
		}catch(MalformedURLException e){
			System.out.println("Error en la URL");
			e.printStackTrace();
		}
		this.documentsName = documentsName;
		this.operationName = operationName;
	}
	
	/**
	 * initialize needed resources
	 * for example load documents
	 */
	protected abstract void initialize();
	
	/**
	 * finalize loaded resources
	 * for example close a DB connection
	 */
	protected abstract void finalize();
	
	/**
	 * start to measure execution time
	 */
	protected EtmMonitor startMeasurement(){
		// initialize measurement subsystem
		BasicEtmConfigurator.configure();

		// startup measurement subsystem
		EtmMonitor etmMonitor = EtmManager.getEtmMonitor();
		etmMonitor.start();
		return etmMonitor;
	}
	
	/**
	 * stop to measure execution time
	 */
	protected void stopMeasurement(EtmMonitor etmMonitor){
		// visualize results
		try{
			File f = new File("PerformanceResult.html");
			FileWriter fw;
			fw = new FileWriter(f);
			PrintWriter pw = new PrintWriter(fw);
			etmMonitor.render(new SimpleHtmlRenderer(pw));
		}catch(IOException e){
			etmMonitor.render(new SimpleTextRenderer());
		}

		etmMonitor.stop();
	}
	
	protected abstract void operationToBeMeasured();

	/**
	 * template method
	 */
	public final void execute() {
		initialize();
		EtmMonitor etmMonitor = startMeasurement();
		for (int i = 0; i < 5; i++) {
			EtmPoint point = etmMonitor.createPoint(operationName+":"+documentsName);
			operationToBeMeasured();
			point.collect();
		}
		stopMeasurement(etmMonitor);
		finalize();
	}
	
	protected String readFile(String path){
		String documentContent = null;
		try {
		    BufferedReader in = new BufferedReader(new FileReader(path));
		    String str;
		    while ((str = in.readLine()) != null) {
		        documentContent += str;
		    }
		    in.close();
		} catch (IOException e) {
			System.out.println("Error al leer el documento");
			e.printStackTrace();
		}
		return documentContent;
	}
}
