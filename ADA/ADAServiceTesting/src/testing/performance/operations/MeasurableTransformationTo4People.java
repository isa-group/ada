package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableTransformationTo4People extends MeasurableOperation{
	
	private String documentPath;
	private String documentContent;

	public MeasurableTransformationTo4People(String documentName, String documentPath, String operationName){
		super(documentName, operationName);
		this.documentPath = documentPath;
	}

	protected void initialize() {
		documentContent = readFile(documentPath);
	}

	protected void finalize() {
	}

	protected void operationToBeMeasured() {
		try{
			ada.xmlToWSAg4People(documentContent.getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la transformación de xml a 4people");
			e.printStackTrace();
		}
	}
}
