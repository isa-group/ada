package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableExplainCompliance  extends MeasurableTwoDocumentOperation{

	public MeasurableExplainCompliance(String templateName, String templatePath, String offerName, String offerPath, String operationName) {
		super(templateName, templatePath, offerName, offerPath, operationName);
	}

	protected void operationToBeMeasured() {
		try{
			ada.explainNonCompliance(super.getTemplateContent().getBytes(), super.getOfferContent().getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la explicación de la disconformidad");
			e.printStackTrace();
		}
	}
}
