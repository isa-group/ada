package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableDeadTerms extends MeasurableOneDocumentOperation{

	public MeasurableDeadTerms(String documentName, String documentPath, String operationName) {
		super(documentName, documentPath, operationName);
	}

	protected void operationToBeMeasured() {
		try{
			ada.getDeadTerms(super.getDocumentContent().getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la comprobación de DeadTerms");
			e.printStackTrace();
		}
	}
}
