package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableGetMetric extends MeasurableOneDocumentOperation{

	public MeasurableGetMetric(String documentName, String documentPath, String operationName) {
		super(documentName, documentPath, operationName);
	}
	
	protected void initialize(){
		
	}

	protected void operationToBeMeasured() {
		try{
			ada.getMetricFile("metrics/metric2575304161886105.xml");
		}catch(RemoteException e){
			System.out.println("Error en getMetric()");
			e.printStackTrace();
		}
	}

}
