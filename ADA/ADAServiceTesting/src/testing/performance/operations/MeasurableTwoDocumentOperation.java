package testing.performance.operations;

public abstract class MeasurableTwoDocumentOperation extends MeasurableOperation{
	
	private String templatePath;
	private String templateContent;
	private String offerPath;
	private String offerContent;
	
	public MeasurableTwoDocumentOperation(String templateName, String templatePath, String offerName, String offerPath, String operationName){
		super("\nTemplate: "+templateName+"\nOffer: "+offerName, operationName);
		this.templatePath = templatePath;
		this.offerPath = offerPath;
	}

	protected void initialize() {
		templateContent = readFile(templatePath);
		offerContent = readFile(offerPath);
	}

	protected void finalize() {
	}

	protected abstract void operationToBeMeasured();
	
	protected String getTemplateContent(){
		return templateContent;
	}
	
	protected String getOfferContent(){
		return offerContent;
	}
}
