package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableCompliance extends MeasurableTwoDocumentOperation{
	
	public MeasurableCompliance(String templateName, String templatePath, String offerName, String offerPath, String operationName){
		super(templateName, templatePath, offerName, offerPath, operationName);
	}

	protected void operationToBeMeasured() {
		try{
			ada.isCompliant(super.getTemplateContent().getBytes(), super.getOfferContent().getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la comprobación de la conformidad");
			e.printStackTrace();
		}
	}

}
