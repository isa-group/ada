package testing.performance.operations;

public abstract class MeasurableOneDocumentOperation extends MeasurableOperation{

	private String documentPath;
	private String documentContent;
	
	public MeasurableOneDocumentOperation(String documentName, String documentPath, String operationName){
		super(documentName, operationName);
		this.documentPath = documentPath;
	}
	
	protected void initialize() {
		documentContent = readFile(documentPath);
	}

	protected void finalize() {
		
	}
	
	protected abstract void operationToBeMeasured();
	
	protected String getDocumentContent(){
		return documentContent;
	}
}
