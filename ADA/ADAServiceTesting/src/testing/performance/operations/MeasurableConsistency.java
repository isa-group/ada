package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableConsistency extends MeasurableOneDocumentOperation{

	public MeasurableConsistency(String documentName, String documentPath, String operationName){
		super(documentName, documentPath, operationName);
	}
	
	protected void operationToBeMeasured(){
		try{
			ada.checkDocumentConsistency(super.getDocumentContent().getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la comprobación de la consistencia");
			e.printStackTrace();
		}
	}
}
