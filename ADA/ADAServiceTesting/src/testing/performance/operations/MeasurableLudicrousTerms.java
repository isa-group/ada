package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableLudicrousTerms extends MeasurableOneDocumentOperation{

	public MeasurableLudicrousTerms(String documentName, String documentPath, String operationName) {
		super(documentName, documentPath, operationName);
	}

	protected void operationToBeMeasured() {
		try{
			ada.getLudicrousTerms(super.getDocumentContent().getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la comprobación de LudicrousTerms");
			e.printStackTrace();
		}
	}

}
