//package testing.monitorisableProperties;
//
//import java.io.File;
//import java.io.IOException;
//import java.rmi.RemoteException;
//import java.sql.SQLException;
//
//import javax.xml.rpc.ServiceException;
//
//import org.apache.commons.io.FileUtils;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import es.us.isa.ada.service.ADAManagerServiceLocator;
//import es.us.isa.ada.service.ADAManagerServicePortType;
//import es.us.isa.ada.service.String2ArrayOfStringMapEntry;
//
//public class MonitorisablePropertiesTest {
//	
//	private ADAManagerServicePortType ada;
//	
//	private String session;
//	
//	private int documentId;
//	
//	private static final String DOC_PATH = "resources/scopesDocument.wsag";
//	
//	@Before
//	public void setUp(){
//		try {
//			String docContent = FileUtils.readFileToString(new File(DOC_PATH));
//			ada = new ADAManagerServiceLocator().getADAManagerServicePort();
//			session = ada.login("Antonio", "ajurado");
//			documentId = ada.storeAgreement("PruebaGetMonitorisableProperties.wsag", docContent, session);
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@Test
//	public void test(){
//		try {
//			String2ArrayOfStringMapEntry[] s2ame = ada.getMonitorisableProperties(documentId, session);
//			for(String2ArrayOfStringMapEntry entry:s2ame){
//				String scope = entry.getKey();
//				System.out.println("Scope: "+scope);
//				String[] vars = entry.getValue();
//				for(String var:vars){
//					System.out.println("  - Var: "+var);
//				}
//			}
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@After
//	public void tearDown(){
//		try {
//			ada.deleteAgreement(documentId, session);
//			ada.logout(session);
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//}
