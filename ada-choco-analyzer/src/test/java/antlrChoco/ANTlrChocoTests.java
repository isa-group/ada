/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package antlrChoco;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import choco.Choco;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerVariable;
import es.us.isa.ada.choco.antlr.ChocoParser;
import es.us.isa.ada.choco.utils.ParsingResult;


public class ANTlrChocoTests {

	private Map<String,Variable> vars;
	private ChocoParser parser;
	
	@Before
	public void setUp(){
		parser = new ChocoParser();
		vars = new HashMap<String,Variable>();
		vars.put("A",Choco.makeIntVar("a", 0, 10));
		vars.put("B",Choco.makeIntVar("b", 0, 10));
		vars.put("C",Choco.makeIntVar("c", 0, 10));
		vars.put("D",Choco.makeIntVar("d", 0, 10));
	}
	
	@Test
	public void testComplejo(){
		String s = "A=(D*B)/C";
		parseMethod(s);
	}
	
	@Test
	public void test1(){
		String s = "A != B AND B = C";
		parseMethod(s);
	}
	
	@Test
	public void test2(){
		String s = "A > B AND B < C";
		parseMethod(s);
	}
	
	@Test
	public void test3(){
		String s = "A <= B OR B >= C";
		parseMethod(s);
	}
	
	@Test
	public void test4(){
		String s = "A > B AND B > C IMPLIES A > C";
		parseMethod(s);
	}
	
	@Test
	public void test5(){
		String s = "A > B AND B > C IFF NOT(A <= C)";
		parseMethod(s);
	}
	
	@Test
	public void test6(){
		String s = "A * B = C";
		parseMethod(s);
	}
	
	@Test
	public void test7(){
		String s = "A > -B";
		parseMethod(s);
	}
	
	@Test
	public void test8(){
		String s = "A = B AND B = C AND C = A";
		parseMethod(s);
	}
	
	@Test
	public void test9(){
		String s = "A < 10";
		parseMethod(s);
	}
	
	@Test
	public void test10(){
		String s = "A = 10 + B + C";
		parseMethod(s);
	}
	
	@Test
	public void test11(){
		String s = "D <= 10";
		parseMethod(s);
	}
	
	@Test
	public void test12(){
		String s = "NOT (A > B)";
		parseMethod(s);
	}
	
	@Test
	public void test13(){
		String s = "2 * B = A";
		parseMethod(s);
	}
	
	@Test
	public void test14(){
		String s = "B / 2 = A";
		parseMethod(s);
	}
	
	@Test
	public void test15(){
		String s = "B % 2 = A";
		parseMethod(s);
	}

	private void parseMethod(String s) {
		parser.setVariables(vars);
		ParsingResult res = parser.parseConstraint(s);
		System.out.println(res.getConstraint());
	}

}
