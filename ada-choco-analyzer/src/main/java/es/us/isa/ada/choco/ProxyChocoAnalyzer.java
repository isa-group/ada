package es.us.isa.ada.choco;

import es.us.isa.ada.ADA;
import es.us.isa.ada.choco.questions.*;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareComplianceOp;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareConsistencyOp;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareExplainNoConsistencyOp;
import es.us.isa.ada.subfacades.ProxyAnalyzer;

/**
 * User: resinas
 * Date: 10/06/13
 * Time: 17:44
 */
public class ProxyChocoAnalyzer extends ProxyAnalyzer {
    public static final String ID = "choco";

    public ProxyChocoAnalyzer() {
        super(ID, new ChocoAnalyzer());

        addOperation(ADA.CONSISTENCY, ChocoConsistencyOp.class);
        addOperation(ADA.TEMPORAL_CONSISTENCY, ChocoTimeAwareConsistencyOp.class);
        addOperation(ADA.EXPLAIN_NON_CONSISTENCY, ChocoExplainNoConsistencyOp.class);
        addOperation(ADA.TEMPORAL_EXPLAIN_NO_CONSISTENCY, ChocoTimeAwareExplainNoConsistencyOp.class);
        addOperation(ADA.COMPLIANCE, ChocoComplianceOp.class);
        addOperation(ADA.TEMPORAL_COMPLIANCE, ChocoTimeAwareComplianceOp.class);
        addOperation(ADA.EXPLAIN_NON_COMPLIANCE, ChocoQuickxplainNoComplianceOp.class);
        addOperation(ADA.LUDICROUS_TERMS, ChocoLudicrousTermsOp.class);
        addOperation(ADA.DEAD_TERMS, ChocoDeadTermsOp.class);
        addOperation(ADA.EXPLAIN_DEAD_TERMS, ChocoExplainDeadTermsOp.class);
        addOperation(ADA.EXPLAIN_LUDICROUS_TERMS, ChocoExplainLudicrousTermsOp.class);
        addOperation(ADA.AGREEMENT_FULFILMENT, ChocoAgreementFulfilmentOp.class);
        addOperation(ADA.VIOLATION_EXPLANATIONS, ChocoExplainViolationOp.class);
        addOperation(ADA.VIEWS, ChocoDecomposeIntoViewsOp.class);
        addOperation(ADA.ALTERNATIVE_DOCUMENTS, ChocoAlternativeDocumentsOp.class);
        addOperation(ADA.NUMBER_OF_ALTERNATIVES, ChocoNumberOfAlternativeDocsOp.class);

    }
}
