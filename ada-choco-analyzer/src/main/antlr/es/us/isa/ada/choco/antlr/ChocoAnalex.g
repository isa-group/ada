header{
	package es.us.isa.ada.choco.antlr;
}

class ChocoAnalex extends Lexer;

options{
	charVocabulary = '\3'..'\377' | '\u1000'..'\u1fff';
	k = 2;
	importVocab = ChocoAnasint; 	
	testLiterals=false;
}

tokens{
	
	//OPERADORES LOGICOS
	AND = "AND";
	OR = "OR";
	NOT = "NOT";
	IFF = "IFF";
	IMPLIES = "IMPLIES";

}

//protected TANTO_POR: '%';
protected SALTO: ("\r\n" | "\n") {newline();};
BLANCO: (SALTO | ' ' | '\t') {$setType(Token.SKIP);};
protected LETRA: 'a'..'z' | 'A'..'Z';
protected BARRA_BAJA: '_';
protected DIGITO: '0'..'9';
protected COMILLA: '\'';
//protected AT: '@';
PUNTO: '.';
protected ALMOADILLA: '#';

//Comentarios de linea
COMENT_LIN: ALMOADILLA (('\r')+ ~('\n') | ~('\r') )* "\r\n" {newline();$setType(Token.SKIP);} ;


IDENT options {testLiterals=true;}: (LETRA | BARRA_BAJA) (LETRA | BARRA_BAJA | DIGITO)*;
//ATT : IDENT (PUNTO IDENT)?; 
//ATT : IDENT PUNTO IDENT;
//CUIDADO CON LOS VALORES NEGATIVOS, AHORA MISMO NO ESTAN CONTEMPLADOS
//LIT_ENTERO: (DIGITO)+;
//LIT_REAL: (DIGITO)+ PUNTO (DIGITO)+;
LIT_STRING: '"' (~'"')* '"';

NUMERO : ((DIGITO)+ '.') => (DIGITO)+ '.' (DIGITO)+ {$setType(LIT_REAL);}
| ((DIGITO)+) => (DIGITO)+ {$setType(LIT_ENTERO);};


PARENTESIS_ABRIR:'(';
PARENTESIS_CERRAR: ')';

//OPERADORES ARITMETICOS	
MAS: '+';
MENOS: '-';
MULT: '*';
DIV: '/';
MOD: '%';
POW: '^';

//OPERADORES RELACIONALES
MAYOR: '>';
MENOR: '<';
MAYOR_IGUAL: ">=";
MENOR_IGUAL: "<=";
IGUAL: "=";
DISTINTO: "!=";

