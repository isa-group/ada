header{
	package es.us.isa.ada.choco.antlr;
	import java.util.*;
	import antlr.*;
}

class ChocoAnasint extends Parser;

options{
	k = 2;
	buildAST = true;	
}

tokens{
	MENOS_UNARIO;
}

{		
}

expresion: 
   expresion_nivel_1 (( IFF^ | IMPLIES^) expresion_nivel_1)*  
   ;
   
expresion_nivel_1: 
  expresion_nivel_2 (OR^ expresion_nivel_2)*  
  ;
  
expresion_nivel_2: 
  expresion_nivel_3 (AND^ expresion_nivel_3)*  
  ;
  
expresion_nivel_3: 
   (NOT^ expresion_nivel_4)
   | expresion_nivel_4 
   ;  

expresion_nivel_4: expresion_nivel_5 
	((MAYOR^ | MENOR^ | MAYOR_IGUAL^ | MENOR_IGUAL^ | IGUAL^ | DISTINTO^)
	expresion_nivel_5)*
	;
	
expresion_nivel_5: exp_mult ((MAS^ | MENOS^) exp_mult)*;

exp_mult: expresion_unaria ((MULT^ | DIV^ | MOD^ | POW^) expresion_unaria)*;

expresion_unaria! :
	MENOS! j:exp_base
	{##=#(#[MENOS_UNARIO,"Unary Minus"],#j);}
	| e:exp_base {## = #e;}
	;
	
exp_base: IDENT //feature, o atributo dentro de una invariante
		 | LIT_REAL 
		 | LIT_ENTERO 
		 | LIT_STRING 
		 | PARENTESIS_ABRIR! expresion PARENTESIS_CERRAR!
		 ; 


	
