header{
	package es.us.isa.ada.choco.antlr;
	import es.us.isa.ada.choco.utils.ParsingResult;
	import es.us.isa.ada.wsag10.domain.*;
	import es.us.isa.ada.choco.utils.Utils;
	import java.util.*;	
	import choco.Choco;
	import choco.kernel.model.constraints.Constraint;
	import choco.kernel.model.variables.Variable;
	import choco.kernel.model.variables.integer.IntegerVariable;
	import choco.kernel.model.variables.integer.IntegerConstantVariable;
	import choco.kernel.model.variables.real.*;
	import choco.kernel.model.variables.integer.IntegerExpressionVariable;
	import choco.kernel.model.variables.real.RealExpressionVariable;
}

//TODO ver como podemos meter por aqui restricciones sobre reales

class ChocoTreeParser extends TreeParser;

options{
	importVocab = ChocoAnasint;
}

{
	ConstantConverter converter = DefaultConstantConverter.getInstance();
	
	Map<String,Variable> variables;
	
	Collection<String> unknownVars;
	
	public IntegerVariable getVariable(AST ident){
		IntegerVariable res = null;
		String s = ident.getText();
		Variable v = variables.get(s);
		if (v != null && (v instanceof IntegerVariable)){
			res = (IntegerVariable)v;
		}
		else{
			unknownVars.add(s);
		}
		return res;
	}	
	
	public IntegerConstantVariable getEnumValue(AST t){
	   //String quotesTrimmed = t.getText();
	   String quotesTrimmed = t.getText().replace("\"","");
	   int aux = converter.convertToInteger(quotesTrimmed);
	   IntegerConstantVariable res = Choco.constant(aux);
	   return res;
	}
		
	
	public IntegerConstantVariable toInt(AST t){
		String s = Utils.removeQuotes(t.getText());
		int i = Integer.parseInt(s);
		IntegerConstantVariable res = Choco.constant(i);
		return res;
	}
	
	public RealConstantVariable toDouble(AST t){
        String s = t.getText();
        double i = Double.parseDouble(s);
        RealConstantVariable res = Choco.constant(i);
        return res;
    }
}

entrada[Map<String,Variable> vars] returns [ParsingResult res = null]
{Constraint aux = null;}:
{variables = vars;
unknownVars = new LinkedList<String>();} 
aux = expresion_logica
{res = new ParsingResult(aux,unknownVars);};

expresion_logica returns [Constraint res = null]
{Constraint c1 = null, c2 = null;}: 
	   #(IFF c1 = expresion_logica c2 = expresion_logica
	   	{res = Choco.ifOnlyIf(c1,c2);})
	 | #(IMPLIES c1 = expresion_logica c2 = expresion_logica
	 	{res = Choco.implies(c1,c2);})
	 | #(OR c1 = expresion_logica c2 = expresion_logica
	 	{res = Choco.or(c1,c2);})
	 | #(AND c1 = expresion_logica c2 = expresion_logica
	 	{res = Choco.and(c1,c2);})
	 | #(NOT c1 = expresion_logica {res = Choco.not(c1);})
	 | res = expresion_relacional;
	 
expresion_relacional returns [Constraint res = null]
{IntegerExpressionVariable v1 = null, v2 = null;}: 
	   #(MAYOR v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.gt(v1,v2);})
	 | #(MENOR v1 = expresion_aritmetica v2 = expresion_aritmetica
	   	{res = Choco.lt(v1,v2);})
	 | #(MAYOR_IGUAL v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.geq(v1,v2);})
	 | #(MENOR_IGUAL v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.leq(v1,v2);})
	 | #(IGUAL v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.eq(v1,v2);})
	 | #(DISTINTO v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.neq(v1,v2);});
 
expresion_aritmetica returns [IntegerExpressionVariable res = null;]
{IntegerExpressionVariable v1 = null, v2 = null;}: 
	   #(MAS v1 = expresion_aritmetica v2 = expresion_aritmetica
	   	{res = Choco.plus(v1,v2);})
	 | #(MENOS v1 = expresion_aritmetica v2 = expresion_aritmetica
	   	{res = Choco.minus(v1,v2);})
	 | #(MULT v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.mult(v1,v2);})
	 | #(DIV v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.div(v1,v2);})
	 | #(MOD v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.mod(v1,v2);})
	 | #(POW v1 = expresion_aritmetica v2 = expresion_aritmetica
	 	{res = Choco.power(v1,v2);})
	 | #(MENOS_UNARIO v1 = expresion_aritmetica)
	 | res = valor
	 | i:IDENT {res = getVariable(#i);}
	 | s:LIT_STRING {res = getEnumValue(#s);};
	 
valor returns [IntegerConstantVariable res = null;]: 
	i:LIT_ENTERO {res = toInt(#i);};