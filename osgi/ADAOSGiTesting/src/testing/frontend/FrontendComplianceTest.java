package testing.frontend;

import static org.junit.Assert.assertEquals;
import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import testing.utils.Utils;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.service.ADAServiceV2;

@RunWith(JUnit4TestRunner.class)
public class FrontendComplianceTest {

	@Inject
	private BundleContext bundleContext;
	
	/**
	 * Ruta absoluta donde est� este proyecto para tirar de los documentos a analizar
	 */
	private String urlBase;
	
	/**
	 * Documentos que usaremos para los tests
	 */
	private Collection<Object[]> docs;
	
	@Before
	public void init(){
		urlBase = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
		//Load documents
		docs = new LinkedList<Object[]>();
		
		String consistentTemplate = "ConsistentTemplate.wsag";
		String compliantOffer = "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag";
		String nonCompliantOffer = "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag";
		
		Object[] compliant = {consistentTemplate, urlBase+consistentTemplate, compliantOffer, urlBase+compliantOffer, true};
		Object[] nonCompliant = {consistentTemplate, urlBase+consistentTemplate, nonCompliantOffer, urlBase+nonCompliantOffer, false};	
		
		docs.add(compliant);
		docs.add(nonCompliant);
	}
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.1"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Test
	public void complianceTest(){
		ServiceReference sr = bundleContext.getServiceReference(ADAServiceV2.class.getCanonicalName());
		ADAServiceV2 ada = (ADAServiceV2) bundleContext.getService(sr);
		//For every document
		for(Object[] doc:docs){
			String templateName = (String)doc[0];
			String templatePath = (String)doc[1];
			String offerName = (String)doc[2];
			String offerPath = (String)doc[3];
			Boolean expectedResult = (Boolean)doc[4];

			System.out.println("\nAnalizamos " + templateName + " y " +offerName);
			//Get documents content as string
			String templateContent = Utils.readFileAsString(templatePath);
			String offerContent = Utils.readFileAsString(offerPath);
			//Analyse
			//No comprobamos la consistencia porque ya se hace en su prueba
			//correspondiente y los documentos cargados aqu� sabemos que son
			//consistentes
			Boolean areCompliant = ada.isCompliant(templateContent.getBytes(), offerContent.getBytes());
			System.out.println("�Son compliant? " + areCompliant);
			if (!areCompliant) {
				Map<AgreementError, Explanation> explanations = ada.explainNonCompliance(templateContent.getBytes(), offerContent.getBytes());
				System.out.println("Explanations: " + explanations);
			}
			System.out.println("\n=================================");
			assertEquals(expectedResult, areCompliant);
		}
	}
}
