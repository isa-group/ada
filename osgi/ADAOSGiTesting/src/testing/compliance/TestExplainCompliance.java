package testing.compliance;

import static org.junit.Assert.assertEquals;
import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import es.us.isa.ada.ADA;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.ExplainNonComplianceOperation;

@RunWith(JUnit4TestRunner.class)
public class TestExplainCompliance {

	@Inject
	private BundleContext bundleContext;
	
	/**
	 * Ruta absoluta donde est� este proyecto para tirar de los documentos a analizar
	 */
	private String urlBase;
	
	@Before
	public void init(){
		urlBase = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
	}
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.1"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Test
	public void explainComplianceTest(){
		ServiceReference sr = bundleContext.getServiceReference(ADA.class.getCanonicalName());
		ADA ada = (ADA) bundleContext.getService(sr);
		//Create operation
		ExplainNonComplianceOperation op = (ExplainNonComplianceOperation)ada.createOperation(ADA.EXPLAIN_NON_COMPLIANCE);
		//Load document
		AbstractDocument temp = ada.loadDocument(urlBase+"pruebas/consitentTemplateSoloUnGT.wsag");
		AbstractDocument of = ada.loadDocument(urlBase+"pruebas/consitentOfferSoloUnGT.wsag");
		op.addDocument(temp);
		op.addDocument(of);
		//Analyse
		ada.analyze(op);
		//Get result
		System.out.println("\n###### Empiezan los errores ######\n");
		Map<AgreementError, Explanation> mapResults = op.explainErrors();
		for(AgreementError agErr:mapResults.keySet()){
			Explanation exp = mapResults.get(agErr);
			for(AgreementElement agElem:agErr.getElements()){
				System.out.println("Error: "+agElem.getName());
			}
			for(AgreementElement agElem:exp.getElements()){
				System.out.println("Explanation: "+agElem.getName());
			}
		}
		System.out.println("\n###### Terminan los errores ######\n");
		assertEquals(Boolean.TRUE, Boolean.FALSE);
	}
}
