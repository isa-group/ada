package testing.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class Utils {

	public static String readFileAsString(String filePath){
		String result = null;
		try {
			FileInputStream file = new FileInputStream(filePath);
			byte[] b = new byte[file.available ()];
			file.read(b);
			file.close ();
			result = new String (b);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return result;
	}
}
