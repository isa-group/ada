package testing.SLARepo;

import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import es.us.isa.ada.ADA;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.operations.AlternativeDocumentsOperation;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.DeadTermsOperation;
import es.us.isa.ada.operations.DecomposeIntoViewsOperation;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;
import es.us.isa.ada.operations.LudicrousTermsOperation;
import es.us.isa.ada.wsag10.AbstractAgreementDocument;
import es.us.isa.ada.wsag10.GuaranteeTerm;
import es.us.isa.ada.wsag10.ServiceScope;
import es.us.isa.ada.wsag10.Term;

@RunWith(JUnit4TestRunner.class)
public class GetDocumentsInfo {

	@Inject
	private BundleContext bundleContext;
	
	private ADA ada;
	
	/**
	 * Ruta absoluta donde est� este proyecto para tirar de los documentos a analizar
	 */
	private String urlBase;
	
	/**
	 * Documentos que usaremos para los tests
	 */
	private Collection<Object[]> docs;
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.0"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.0"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Before
	public void init(){
		urlBase = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
		//Load documents
		docs = new LinkedList<Object[]>();
		File fPath = new File(urlBase+"front-end\\");
		File[] subfiles = fPath.listFiles();
		for(File f:subfiles){
			String path = f.getPath();
			if(!path.endsWith(".svn")){
				Object[] aux = {f.getName(), path};
				docs.add(aux);
			}
		}
	}
	
	@Test
	public void test(){
		ServiceReference sr = bundleContext.getServiceReference(ADA.class.getCanonicalName());
		ada = (ADA) bundleContext.getService(sr);
		//Para cada documento
		for(Object[] doc:docs){
			getAllInformation((String)doc[0], (String)doc[1]);
		}
	}
	
	public void getAllInformation(String docName, String docPath){
		//hay que pillar:
		// - #GT (Con getAllTerms y parseando)
		// - #Variantes (Con NumberOfAlternativeDocs)
		// - #Scopes (Con DecomposeIntoViews)
		// - #Conflicts (Con explainConsistency)
		Integer numGt = 0, numVariants = 0, numScopes = 0, numConflicts = 0;
		//Load the document
		AbstractAgreementDocument doc = (AbstractAgreementDocument)ada.loadDocument(docPath);
		//Number of GT
		Collection<Term> terms = doc.getAllTerms();
		for(Term t:terms){
			if(t instanceof GuaranteeTerm){
				numGt++;
			}
		}
		
		//Number of alternative documents
//		AlternativeDocumentsOperation altDocsOp = (AlternativeDocumentsOperation) ada.createOperation(ADA.ALTERNATIVE_DOCUMENTS);
//		altDocsOp.addDocument(doc);
//		ada.analyze(altDocsOp);
//		numVariants = altDocsOp.getNumberOfDocuments();
		
		//Number of views
		DecomposeIntoViewsOperation viewsOp = (DecomposeIntoViewsOperation) ada.createOperation(ADA.VIEWS);
		viewsOp.addDocument(doc);
		ada.analyze(viewsOp);
		Map<ServiceScope, AbstractDocument> views = viewsOp.getScopes2Views();
		numScopes = views.keySet().size();
		
		//Number of conflicts
		//First, check consistency
		ConsistencyOperation consOp = (ConsistencyOperation) ada.createOperation(ADA.CONSISTENCY);
		consOp.addDocument(doc);
		ada.analyze(consOp);
		Boolean isConsistent = consOp.isConsistent();
		//If it�s not consistent then explain conflicts
		if(!isConsistent){
			ExplainNoConsistencyOperation expConsOp = (ExplainNoConsistencyOperation) ada.createOperation(ADA.EXPLAIN_NON_CONSISTENCY);
			expConsOp.addDocument(doc);
			ada.analyze(expConsOp);
			Map<AgreementElement, Collection<AgreementElement>> conflicts = expConsOp.explainErrors();
			numConflicts = conflicts.keySet().size();
		}
		if (!docName.equalsIgnoreCase("DeadTerm(Xor).wsag") && !docName.equalsIgnoreCase("Scopes.wsag")) {
			//Check dead terms
			DeadTermsOperation deadOp = (DeadTermsOperation) ada.createOperation(ADA.DEAD_TERMS);
			deadOp.addDocument(doc);
			ada.analyze(deadOp);
			Boolean hasDeadTerms = deadOp.hasDeadTerms();
			if (hasDeadTerms) {
				numConflicts += deadOp.getDeadTerms().size();
			}
		}
		if (!docName.equalsIgnoreCase("Scopes.wsag")) {
			//Check ludicrous terms
			LudicrousTermsOperation ludOp = (LudicrousTermsOperation) ada
					.createOperation(ADA.LUDICROUS_TERMS);
			ludOp.addDocument(doc);
			ada.analyze(ludOp);
			Boolean hasLudicrousTerms = ludOp.hasLudicrousTerms();
			if (hasLudicrousTerms) {
				numConflicts += ludOp.getLudicrousTerms().size();
			}
		}
		//Print results
		System.out.println(generateHTMLForANewRow(docName, numGt.toString(), numVariants.toString(), numScopes.toString(), numConflicts.toString(), docPath));
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> params = new LinkedList<Object[]>();
		
		File f = new File("front-end");
		File[] subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			String name = subfiles[i].getName();
			if (!path.endsWith(".svn")){
				Object[] aux = {name, path};
				params.add(aux);
			}
		}
		return params;
	}
	
	private String generateHTMLForANewRow(String nombre, String nGt, String nVars, String nViews, String nConfs, String path){
		String tabulador = "  ";
		StringBuilder sb = new StringBuilder();
		sb.append(tabulador+tabulador+tabulador+"<tr>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td class=\"nombre\">"+nombre+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>WS-Agreement</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nGt+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nVars+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nViews+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td>"+nConfs+"</td>\n");
		sb.append(tabulador+tabulador+tabulador+tabulador+"<td><a href=\"http://www.isa.us.es/ada.source/docs/"+nombre+"\">Download</a></td>\n");
		sb.append(tabulador+tabulador+tabulador+"</tr>\n");
		return sb.toString();
	}
}
