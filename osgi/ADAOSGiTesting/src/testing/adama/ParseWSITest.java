package testing.adama;

import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.util.Collection;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import es.us.isa.ada.ADA;
import es.us.isa.ada.salmon.WebServiceInformation;
import es.us.isa.ada.wsag10.AbstractAgreementDocument;
import es.us.isa.ada.wsag10.ServiceDescriptionTerm;
import es.us.isa.ada.wsag10.Term;

@RunWith(JUnit4TestRunner.class)
public class ParseWSITest {

	@Inject
	private BundleContext bundleContext;
	
	private ADA ada;
	
	private static final String URL_BASE = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
	
	private static final String DOC_PATH = "MMD/WSIDocument.wsag";
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),			
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.0"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAma").version("0.1.0"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.0"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Before
	public void setUp(){
		ServiceReference sr = bundleContext.getServiceReference(ADA.class.getCanonicalName());
		ada = (ADA) bundleContext.getService(sr);
	}
	
	@Test
	public void test(){
		AbstractAgreementDocument doc = (AbstractAgreementDocument) ada.loadDocument(URL_BASE+DOC_PATH);
		Collection<Term> terms = doc.getAllTerms();
		for(Term t:terms){
			if(t instanceof ServiceDescriptionTerm){
				ServiceDescriptionTerm sdt = (ServiceDescriptionTerm) t;
				WebServiceInformation wsi = sdt.getWebServiceInformation();
				System.out.println("Name: "+wsi.getName());
				System.out.println("Description: "+wsi.getDescription());
				System.out.println("Domain: "+wsi.getDomain());
				System.out.println("WsdlURL: "+wsi.getWsdlURL());
				System.out.println("Endpoint: "+wsi.getEndpoint());
				for(Entry<String, String> operation:wsi.getOperations().entrySet()){
					System.out.println("Operation name: "+operation.getKey()+" - SoapAction: "+operation.getValue());
				}
			}
		}
	}
	
	@After
	public void tearDown(){
		
	}
}
