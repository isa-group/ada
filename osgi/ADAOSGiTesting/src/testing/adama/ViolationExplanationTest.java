package testing.adama;

import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import testing.utils.Utils;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.salmon.Measure;
import es.us.isa.ada.salmon.MonitoringManagementDocument;
import es.us.isa.ada.salmon.OperationMetric;
import es.us.isa.ada.salmon.ServiceMetric;
import es.us.isa.ada.service.ADAManagerService;
import es.us.isa.ada.service.ADAServiceWithRepo;

@RunWith(JUnit4TestRunner.class)
public class ViolationExplanationTest {

	@Inject
	private BundleContext bundleContext;
	
	private ADAManagerService adama;
	
	private ADAServiceWithRepo adarepo;
	
	private String session;
	
	private int documentId;
	
	private static final String URL_BASE = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
	
//	private static final String FIXED_AND_RANGE_VALUE_DOC_PATH = "fulfilmentTestsDocuments/agreementFixedAndRangeValue.wsag";
	
	private static final String ENUMERATED_VALUE_DOC_PATH = "fulfilmentTestsDocuments/agreementEnumeratedValue.wsag";
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics y resources que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),			
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAma").version("0.1.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.1"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Before
	public void setUp(){
		ServiceReference sr = bundleContext.getServiceReference(ADAManagerService.class.getCanonicalName());
		adama = (ADAManagerService) bundleContext.getService(sr);
		sr = bundleContext.getServiceReference(ADAServiceWithRepo.class.getCanonicalName());
		adarepo = (ADAServiceWithRepo) bundleContext.getService(sr);
	}
	
	@Test
	public void test(){
//		String docContent = Utils.readFileAsString(URL_BASE+FIXED_AND_RANGE_VALUE_DOC_PATH);
		String docContent = Utils.readFileAsString(URL_BASE+ENUMERATED_VALUE_DOC_PATH);
		session = adama.login("Antonio", "ajurado");
		try {
			documentId = adama.storeAgreement("PruebaAgFulfilment.wsag", docContent, session);
			String mmdString = adama.getMMD(documentId, session);
			MonitoringManagementDocument mmd = new MonitoringManagementDocument(mmdString);
//			System.out.println(mmd.toString());
			
			//A�adimos medidas al MMD
			Measure measureFixed = new Measure();
			Measure measureRange = new Measure();
			Measure measureEnumerated = new Measure();
			measureFixed.setValue("45");
			measureRange.setValue("20..40");
			measureEnumerated.setValue("Blue,");
			Collection<ServiceMetric> serviceMetrics = mmd.getServiceMetrics();
			for(ServiceMetric sm:serviceMetrics){
				if(sm.getMetric().equalsIgnoreCase("Color")){
					sm.addMeasure(measureEnumerated);
				}
			}
			
			Collection<OperationMetric> operationMetrics = mmd.getOperationMetrics();
			for(OperationMetric om:operationMetrics){
				if(om.getMetric().equalsIgnoreCase("DemandedTranslationTime")){
					om.addMeasure(measureFixed);
					om.addMeasure(measureRange);
				}
			}
			
			//ejecutamos ViolationExplanation
			Map<AgreementError, Explanation> explanations = adarepo.explainAgreementViolation(documentId, mmd.toString().getBytes());
			System.out.println("Explanations: \n  "+explanations);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown(){
		try {
			adama.deleteAgreement(documentId, session);
			adama.logout(session);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
