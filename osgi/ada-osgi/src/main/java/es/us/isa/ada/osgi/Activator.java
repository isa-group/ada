/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.osgi;

import es.us.isa.ada.ADA;
import es.us.isa.ada.io.IDocumentParser;
import es.us.isa.ada.io.IDocumentWriter;
import es.us.isa.ada.repository.AgreementRepository;
import es.us.isa.ada.selectors.SelectorCriteria;
import es.us.isa.ada.service.*;
import es.us.isa.ada.subfacades.ADAManager;
import es.us.isa.ada.subfacades.ProxyAnalyzer;
import es.us.isa.ada.users.SessionManager;
import es.us.isa.ada.wsag10.transforms.ITransform;
import org.osgi.framework.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Atributos utilizados en los diccionarios de los servicios 
 * de ADA:
 * 
 * <ul>
 * <li> type: si type = ADA, el elemento se reconoce como parte de ADA
 * <li> id: identificador dentro de ADA del elemento. se utiliza
 * a nivel informativo, y para clasificar ciertos elementos
 * <li> extensions: para parsers y writers, es una lista de extensiones,
 *  separadas por comas (','), que nos indica las extensiones
 *  con las que trata.
 * </ul>
 * 
 * @author Jesus
 *
 */
public class Activator implements BundleActivator {

	private Collection<ServiceReference> references;
	private BundleContext bundleContext;
//	private ServiceRegistration registration;
	private Hashtable<String,String> t;
	private Collection<ServiceRegistration> registry;
	// public static Map<String,ProxyAnalyzer> ans = new HashMap<String,
	// ProxyAnalyzer>();
	public static Collection<ProxyAnalyzer> ans = new LinkedList<ProxyAnalyzer>();
	public static Map<String, IDocumentParser> parsers = new HashMap<String, IDocumentParser>();
	public static Map<String, IDocumentWriter> writers = new HashMap<String, IDocumentWriter>();
	public static Collection<ITransform> transforms = new LinkedList<ITransform>();
//	public static SessionManager sm = null;
//	public static AgreementRepository ar = null;
	
	public static ADAManager manager;
	
	private static Boolean isOsgiLoaded = false;
	
	public void start(BundleContext bc) throws Exception {
//		System.out.println("Hello world from ADA!!");
		isOsgiLoaded = true;
		registry = new LinkedList<ServiceRegistration>();
		bundleContext = bc;
		references = new LinkedList<ServiceReference>();
		manager = new ADAManager();
		ServiceListener sl = new ADAListener();
		String filter = "(type=ADA)";
		bc.addServiceListener(sl, filter);
		ServiceReference[] srl = bc.getServiceReferences(null, filter);
		for (int i = 0; srl != null && i < srl.length; i++) {
			sl.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED,
							srl[i]));
		}

		//registramos ADA
		ADA analyser = new ADA(OSGiExtensionsLoader.getInstance());
		t = new Hashtable<String,String>();
		t.put("id", "ADA");
		ServiceRegistration registration = bc.registerService(ADA.class.getCanonicalName(), analyser, t);
		registry.add(registration);
		
		//FIXME Comentada esta parte del codigo de wsag4j porque
		//al usar el singleton, tenemos errores muy dificiles de detectar
		//como el que ocasionaba que no se cargase los SDT por culpa
		//del parser de WSAG4J. esto es debido a que a tener
		//una sola instancia del cargador de OSGi, quienes la usen
		//van a acabar teniendo de forma comun todo lo que se vaya
		//cargando, y en este caso se cargaba en 2� lugar el parser
		//de wsag4j, ke acababa sobreescribiendo al parser por defecto,
		//ocasionando que no se cargaran los SDT
		
//		//registramos la fachada de ADA para wsag4j
//		ADA4Wsag4J wsag4jada = new ADA4Wsag4J();
//		t = new Hashtable<String, String>();
//		t.put("id", "ADA4Wsag4j");
//		registration = bc.registerService(ADA4Wsag4J.class.getCanonicalName(), wsag4jada, t);
//		registry.add(registration);
		
		Properties servicesProp = loadServicesFile();
		
		//registramos el servicio
		t = new Hashtable<String, String>();
		t.put("id", "ADA Service");
		t.put("service.exported.interfaces", "*");
        t.put("service.exported.configs", "org.apache.cxf.ws");
//        t.put("org.apache.cxf.ws.address", "http://localhost:8081/ADAService");
        t.put("org.apache.cxf.ws.address", servicesProp.getProperty("ada"));
  
		ADAServiceV2 ada = new ADAServiceImpl(analyser);
		registration = bc.registerService(ADAServiceV2.class.getCanonicalName(), ada, t);
		registry.add(registration);
		
		//registramos adama
		t = new Hashtable<String, String>();
		t.put("id", "ADA Manager");
		t.put("service.exported.interfaces", "*");
        t.put("service.exported.configs", "org.apache.cxf.ws");
//        t.put("org.apache.cxf.ws.address", "http://localhost:8084/ADAma");
        t.put("org.apache.cxf.ws.address", servicesProp.getProperty("adama"));
		  
	    ADAManagerService adama = new ADAManagerServiceImpl(OSGiExtensionsLoader.getInstance());
	    registration = bc.registerService(ADAManagerService.class.getCanonicalName(), adama, t);
		registry.add(registration);
		
		//y registramos el ada service a partir de los identificadores
		//de los documentos registrados
		t = new Hashtable<String, String>();
		t.put("id", "ADA Service Repository");
		t.put("service.exported.interfaces", "*");
        t.put("service.exported.configs", "org.apache.cxf.ws");
//        t.put("org.apache.cxf.ws.address", "http://localhost:8083/ADAServiceRepo");
        t.put("org.apache.cxf.ws.address", servicesProp.getProperty("repo"));
		  
	    ADAServiceWithRepo adawithrepo = new ADAServiceWithRepoImpl(adama,ada);
	    registration = bc.registerService(ADAServiceWithRepo.class.getCanonicalName(), adawithrepo, t);
		registry.add(registration);
		
		
	}

	public void stop(BundleContext bc) throws Exception {
		
		Iterator<ServiceReference> it = references.iterator();
		while (it.hasNext()) {
			ServiceReference sr = it.next();
			bc.ungetService(sr);
		}
		Iterator<ServiceRegistration> it2 = registry.iterator();
		while (it2.hasNext()){
			ServiceRegistration sr = it2.next();
			sr.unregister();
		}
		
	}
	
	public static Boolean isInAnOsgiContainer(){
		return isOsgiLoaded;
	}

	class ADAListener implements ServiceListener {

		@Override
		public void serviceChanged(ServiceEvent arg0) {
			if (arg0.getType() == ServiceEvent.REGISTERED) {
				ServiceReference sr = arg0.getServiceReference();
				references.add(sr);
				Object obj = bundleContext.getService(sr);
				if (obj instanceof ProxyAnalyzer) {
					ProxyAnalyzer proxy = (ProxyAnalyzer) obj;
					ans.add(proxy);
				}
				else if (obj instanceof IDocumentParser){
					IDocumentParser parser = (IDocumentParser) obj;
					String aux = (String) sr.getProperty("extensions");
					Collection<String> exts = extractCSVString(aux);
					Iterator<String> it = exts.iterator();
					while (it.hasNext()){
						String s = it.next();
						parsers.put(s, parser);
					}
				}
				else if (obj instanceof IDocumentWriter){
					IDocumentWriter writer = (IDocumentWriter) obj;
					String aux = (String) sr.getProperty("extensions");
					Collection<String> exts = extractCSVString(aux);
					Iterator<String> it = exts.iterator();
					while (it.hasNext()){
						String s = it.next();
						writers.put(s, writer);
					}
				}
				else if (obj instanceof ITransform){
					ITransform trans = (ITransform) obj;
					transforms.add(trans);
				}
				else if (obj instanceof SelectorCriteria){
					//TODO ver como integrar esto en el futuro
					//debe ser de forma muy similar a las transformaciones
					//por ejemplo
					SelectorCriteria sc = (SelectorCriteria) obj;
				}
				else if (obj instanceof SessionManager){
//					sm = (SessionManager) obj;
					manager.setSessionManager((SessionManager) obj);
				}
				else if (obj instanceof AgreementRepository){
//					ar = (AgreementRepository) obj;
					manager.setAgreementRepository((AgreementRepository) obj);
				}
			} else if (arg0.getType() == ServiceEvent.MODIFIED) {
				// actualizar el servicio en la coleccion de ans
				ServiceReference sr = arg0.getServiceReference();
				Object obj = bundleContext.getService(sr);
				if (obj instanceof ProxyAnalyzer) {
					ProxyAnalyzer proxy = (ProxyAnalyzer) obj;
					ans.remove(proxy);
					ans.add(proxy);
				}
			} else if (arg0.getType() == ServiceEvent.UNREGISTERING) {
				// TODO como eliminamos del resto???
				// eliminar el servicio en la coleccion de ans
				ServiceReference sr = arg0.getServiceReference();
				references.remove(sr);
				String id = (String) sr.getProperty("id");
				Iterator<ProxyAnalyzer> it = ans.iterator();
				boolean enc = false;
				while (it.hasNext() && !enc) {
					ProxyAnalyzer an = it.next();
					if (an.getId().equals(id)) {
						it.remove();
						enc = true;
					}
				}
				bundleContext.ungetService(sr);
			}
		}

		private Collection<String> extractCSVString(String s){
			Collection<String> res = new LinkedList<String>();
			StringTokenizer st = new StringTokenizer(s,",");
			while (st.hasMoreTokens()){
				res.add(st.nextToken());
			}
			return res;
		}
		
	}
	
	private Properties loadServicesFile(){
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("ada-services.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("ADA services file not found");
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("ADA services file not found");
		}
		return p;
	}

}
