/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.osgi;

import es.us.isa.ada.loaders.ExtensionsLoader;
import es.us.isa.ada.selectors.DefaultSelectorCriteria;
import es.us.isa.ada.selectors.SelectorCriteria;
import es.us.isa.ada.subfacades.*;
import es.us.isa.ada.wsag10.parsers.DefaultWSAgParser;
import es.us.isa.ada.wsag10.transforms.WSAg4PeopleTransform;
import es.us.isa.ada.wsag10.transforms.WSAgPeople2XMLTransform;

import java.util.Collection;

public class OSGiExtensionsLoader implements ExtensionsLoader {

	private AnalysisManager am;
	private DocumentsManager dm;
	private TransformManager tm;
	private ADAManager adama;
	
	private static OSGiExtensionsLoader instance = null;
	
	public static OSGiExtensionsLoader getInstance(){
		if (instance == null){
			instance = new OSGiExtensionsLoader();
		}
		return instance;
	}

	protected OSGiExtensionsLoader() {
		loadAnalyzers();
		loadIO();
		loadTransforms();
		loadADAma();
	}

	private void loadADAma() {
		adama = Activator.manager;
	}


	private void loadTransforms() {
		//TODO cargar esto mediante OSGi
		tm = new TransformManager();
		tm.addTransform(new WSAg4PeopleTransform());
		tm.addTransform(new WSAgPeople2XMLTransform());
	}

	private void loadIO() {
		//TODO cargar mediante OSGi esto
		dm = new DocumentsManager();
		dm.addParser("wsag", new DefaultWSAgParser());
	}

	private void loadAnalyzers() {
		Collection<ProxyAnalyzer> ans = Activator.ans;
		SelectorCriteria sc = new DefaultSelectorCriteria();
		am = new AnalysisManager(ans,sc);
	}


	@Override
	public AnalysisManager getAnalysisManager() {
		return am;
	}

	@Override
	public DocumentsManager getDocumentsManager() {
		return dm;
	}

	@Override
	public TransformManager getTransformManager() {
		return tm;
	}

	@Override
	public ADAManager getADAManager() {
		return adama;
	}


}
