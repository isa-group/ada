/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.choco.osgi;

import es.us.isa.ada.ADA;
import es.us.isa.ada.choco.ChocoAnalyzer;
import es.us.isa.ada.choco.questions.*;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareComplianceOp;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareConsistencyOp;
import es.us.isa.ada.choco.questions.timeAware.ChocoTimeAwareExplainNoConsistencyOp;
import es.us.isa.ada.subfacades.ProxyAnalyzer;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

public class Activator implements BundleActivator {

	private Collection<ServiceRegistration> registrations;
	
	//TODO no estoy muy seguro de que esto vaya a funcionar...
	public void start(BundleContext bc) throws Exception {
		
		//registro del analizador
		registrations = new LinkedList<ServiceRegistration>();
		ServiceRegistration sr;
		Hashtable<String,String> table = new Hashtable<String,String>();
		ChocoAnalyzer choco = new ChocoAnalyzer();
		String id = "choco";
		table.put("type", "ADA");
		table.put("id", id);
		
		ProxyAnalyzer proxy = new ProxyAnalyzer(id, choco);
		proxy.addOperation(ADA.CONSISTENCY, ChocoConsistencyOp.class);
		proxy.addOperation(ADA.TEMPORAL_CONSISTENCY, ChocoTimeAwareConsistencyOp.class);
		proxy.addOperation(ADA.EXPLAIN_NON_CONSISTENCY, ChocoExplainNoConsistencyOp.class);
		proxy.addOperation(ADA.TEMPORAL_EXPLAIN_NO_CONSISTENCY, ChocoTimeAwareExplainNoConsistencyOp.class);
		proxy.addOperation(ADA.COMPLIANCE, ChocoComplianceOp.class);
		proxy.addOperation(ADA.TEMPORAL_COMPLIANCE, ChocoTimeAwareComplianceOp.class);
		proxy.addOperation(ADA.EXPLAIN_NON_COMPLIANCE, ChocoQuickxplainNoComplianceOp.class);
		proxy.addOperation(ADA.LUDICROUS_TERMS,ChocoLudicrousTermsOp.class);
		proxy.addOperation(ADA.DEAD_TERMS,ChocoDeadTermsOp.class);
		proxy.addOperation(ADA.EXPLAIN_DEAD_TERMS,ChocoExplainDeadTermsOp.class);
		proxy.addOperation(ADA.EXPLAIN_LUDICROUS_TERMS,ChocoExplainLudicrousTermsOp.class);
		proxy.addOperation(ADA.AGREEMENT_FULFILMENT, ChocoAgreementFulfilmentOp.class);
		proxy.addOperation(ADA.VIOLATION_EXPLANATIONS, ChocoExplainViolationOp.class);
		proxy.addOperation(ADA.VIEWS, ChocoDecomposeIntoViewsOp.class);
		proxy.addOperation(ADA.ALTERNATIVE_DOCUMENTS, ChocoAlternativeDocumentsOp.class);
		proxy.addOperation(ADA.NUMBER_OF_ALTERNATIVES, ChocoNumberOfAlternativeDocsOp.class);
		sr = bc.registerService(ProxyAnalyzer.class.getCanonicalName(), proxy, table);
		registrations.add(sr);

	}
	

	public void stop(BundleContext bc) throws Exception {
		Iterator<ServiceRegistration> it = registrations.iterator();
		while (it.hasNext()){
			ServiceRegistration sr = it.next();
			sr.unregister();
		}
	}

}
