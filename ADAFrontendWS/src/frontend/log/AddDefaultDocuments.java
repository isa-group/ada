package frontend.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddDefaultDocuments extends DBConnection{
	
	private String[] names;
	
	private String[] contents;
	
	public AddDefaultDocuments(String[] names, String[] contents){
		super();
		this.names = names;
		this.contents = contents;
	}

	protected void dbQuery(Connection c) throws SQLException {
		
		PreparedStatement psInsertDocs = c.prepareStatement("INSERT INTO documentos VALUES (null, ?, ?, ?)");
		
		if(names.length == contents.length) {
			for (int i = 0; i < names.length; i++) {
				psInsertDocs.setString(1, names[i]);
				psInsertDocs.setString(2, contents[i]);
				psInsertDocs.setInt(3, 1);

				//Execute insert
				psInsertDocs.executeUpdate();
			}
		}else{
			System.out.println("ERROR: El numero de nombres es distinto al numero de contenidos");
		}
	}
}
