package frontend.log;

import javafx.async.JavaTaskBase;
import javafx.async.RunnableFuture;

/**
 * @author AJurado
 */
public class AddReportTaskBase extends JavaTaskBase{
    
    public-init var text:String;
    
    public-init var username:String;
    
    public-init var userOrganization:String;
    
    public-init var userEmail:String;
    
    public-init var sessionId:String;

	public-init var createTask:function(text:String, username:String, userOrganization:String, userEmail:String, sessionId:String):RunnableFuture;

	override function create():RunnableFuture{
	    createTask(text, username, userOrganization, userEmail, sessionId);
	}
}
