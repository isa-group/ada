package frontend.log;

import javafx.async.JavaTaskBase;
import javafx.async.RunnableFuture;

/**
 * @author AJurado
 */
public class AddDefaultDocsTaskBase extends JavaTaskBase{
    
    public-init var docsNames:nativearray of String;
    
    public-init var docsContents:nativearray of String;
    
    public-init var createTask:function(docsNames:nativearray of String, docsContents:nativearray of String):RunnableFuture; 
    
    override function create():RunnableFuture{
        createTask(docsNames, docsContents);
    }
}