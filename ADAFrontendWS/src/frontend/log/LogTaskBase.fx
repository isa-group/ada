package frontend.log;

import javafx.async.JavaTaskBase;
import javafx.async.RunnableFuture;

/**
 * @author AJurado
 */
public class LogTaskBase extends JavaTaskBase{
    
    /**
     * Operation name
     */
    public-init var operation : String;
    
    /**
     * Java array that contains document name, document content and default boolean
     */
    public-init var doc1 : nativearray of Object;
    
    /**
     * Java array that contains document name, document content and default boolean
     */
    public-init var doc2 : nativearray of Object;
    
    /**
     * Text showed in the console
     */
    public-init var consoleLog : String;
    
    /**
     * This session identifier
     */
    public-init var sessionId : String;
    
    /**
     * Result returned by ADA service
     */
    public-init var resultADA : String;
    
    /**
     * Info about the browser, operating system...
     */
    public-init var browserInfo : String;
    
    public-init var createTask:function(operation:String, doc1:nativearray of Object, doc2:nativearray of Object, consoleLog:String, resultADA:String, sessionId:String, browserInfo:String):RunnableFuture;
    
    override function create():RunnableFuture{
        createTask(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo);
    }
}