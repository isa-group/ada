/*
 * Copyright 2006-2008 Kees de Kooter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package frontend.detail.editorkits;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;
import javax.swing.text.PlainView;
import javax.swing.text.Segment;
import javax.swing.text.Utilities;

public class WSAg4PeopleView extends PlainView {

	private static HashMap<Pattern, Color> defaultPatterns;
    private static String TAG_COMMENT = "(\".*\")";

    static {
        // NOTE: the order is important!
    	defaultPatterns = new LinkedHashMap<Pattern, Color>();
        
        defaultPatterns.put(Pattern.compile("(Template )"), new Color(0, 120, 0));
        defaultPatterns.put(Pattern.compile("(AgreementOffer )"), new Color(0, 120, 0));

        defaultPatterns.put(Pattern.compile("(-)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(--)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Initiator:)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( initiator:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Responder:)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( responder:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(ServiceProvider:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(ExpirationTime:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(TemplateID:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(TemplateName:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Terms)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( Service )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("(Service Properties)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Service Description Terms:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Service Description Term)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Service References:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Service Properties)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("( Guarantee )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( Term )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("( Service Reference)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("( Property )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(measured by)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( by )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(related to)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( to )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Scopes:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Qualifying Condition:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(KPI:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(SLO:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Relative importance between guarantees:)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( importance )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( between )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( guarantees:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Guarantees:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Penalty:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Reward:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Utility if selected:)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( if )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( selected:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Custom Business Value:)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("(Value:)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( Value of)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Value Of)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( of )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( Of )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Creation )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Constraints:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Items:)"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("( is )"), java.awt.Color.BLUE);
        defaultPatterns.put(Pattern.compile("(Assessed in)"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( assessed )"), java.awt.Color.BLUE);
//        defaultPatterns.put(Pattern.compile("( in )"), java.awt.Color.BLUE);

        defaultPatterns.put(Pattern.compile("(All:)"), new Color(127, 0, 127));
        defaultPatterns.put(Pattern.compile("(One Or More between:)"), new Color(127, 0, 127));
        defaultPatterns.put(Pattern.compile("(Exactly One between:)"), new Color(127, 0, 127));
//        defaultPatterns.put(Pattern.compile("( Or )"), new Color(127, 0, 127));
//        defaultPatterns.put(Pattern.compile("( More )"), new Color(127, 0, 127));
//        defaultPatterns.put(Pattern.compile("(between:)"), new Color(127, 0, 127));
//        defaultPatterns.put(Pattern.compile("( Exactly )"), new Color(127, 0, 127));
      
        defaultPatterns.put(Pattern.compile("(//.*)"), java.awt.Color.GRAY);
    }

    public WSAg4PeopleView(Element element) {

        super(element);

        // Set tabsize to 4 (instead of the default 8)
        getDocument().putProperty(PlainDocument.tabSizeAttribute, 4);
    }

    @Override
    protected int drawUnselectedText(Graphics graphics, int x, int y, int p0,
            int p1) throws BadLocationException {

        Document doc = getDocument();
        String text = doc.getText(p0, p1 - p0);

        Segment segment = getLineBuffer();

        SortedMap<Integer, Integer> startMap = new TreeMap<Integer, Integer>();
        SortedMap<Integer, Color> colorMap = new TreeMap<Integer, Color>();

        // Match all regexes on this snippet, store positions
        for (Map.Entry<Pattern, Color> entry : defaultPatterns.entrySet()) {

            Matcher matcher = entry.getKey().matcher(text);

            while (matcher.find()) {
                startMap.put(matcher.start(1), matcher.end());
                colorMap.put(matcher.start(1), entry.getValue());
            }
        }

        // Todo: check the map for overlapping parts

        int i = 0;

        // Colour the parts
        for (Map.Entry<Integer, Integer> entry : startMap.entrySet()) {
            int start = entry.getKey();
            int end = entry.getValue();

            if (i < start) {
                graphics.setColor(Color.black);
                doc.getText(p0 + i, start - i, segment);
                x = Utilities.drawTabbedText(segment, x, y, graphics, this, i);
            }

            graphics.setColor(colorMap.get(start));
            i = end;
            doc.getText(p0 + start, i - start, segment);
            x = Utilities.drawTabbedText(segment, x, y, graphics, this, start);
        }

        // Paint possible remaining text black
        if (i < text.length()) {
            graphics.setColor(Color.black);
            doc.getText(p0 + i, text.length() - i, segment);
            x = Utilities.drawTabbedText(segment, x, y, graphics, this, i);
        }

        return x;
    }

}
