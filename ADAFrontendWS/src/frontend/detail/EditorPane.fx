package frontend.detail;
import frontend.detail.editorkits.WSAgEditorKit;
import frontend.detail.editorkits.WSAg4PeopleEditorKit;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import javafx.ext.swing.SwingComponent;
import java.awt.event.KeyAdapter;

/**
 * @author AJurado
 */
public class EditorPane extends SwingComponent{
	var editorPane: JEditorPane;
	var scrollPane: JScrollPane;
	var updateComponentFlag: Boolean = false;

	/* Important: contentType variable must be before than text var because
	setContentType deletes editorPane text */
    public var contentType: String = "text/wsag" on replace{
        editorPane.setContentType(contentType);
    }
    
    public var text: String on replace{
    	if(not updateComponentFlag){
        	editorPane.setText(text);
        	if(editorPane.getPreferredSize().getHeight() > height){
        	    //ScrollBars are visible
        	    editorPaneHeight = editorPane.getPreferredSize().getHeight();
        	}else{
        	    editorPaneHeight = height;
        	}
		}
	};
    
    public var editable: Boolean = true on replace{
    	editorPane.setEditable(editable);
	};
	
	public var isChanged: Boolean = false;
	
	public var highlighter:ADAHighlighter = new ADAHighlighter(editorPane);
    
    public var onKeyUp: function(keyEvent :java.awt.event.KeyEvent);

    function updateComponentField(){
    	updateComponentFlag = true;
    	text = editorPane.getText();
    	updateComponentFlag = false;
	}
	
	public var editorPaneHeight:Number;
	
	public function getCaretPosition():Integer{
	    return editorPane.getCaretPosition();
	}
	
	public function setCaretPosition(position:Integer, viewPos:java.awt.Point){
	    editorPane.setCaretPosition(position);
	    FX.deferAction(function (){
	        scrollPane.getViewport().setViewPosition(viewPos);
	    });
	}
	
	public function getViewPos():java.awt.Point{
	    return scrollPane.getViewport().getViewPosition();
	}
	
    override function createJComponent(): JComponent {
    	editorPane = new JEditorPane();
    	editorPane.setEditorKitForContentType("text/wsag", new WSAgEditorKit());
    	editorPane.setEditorKitForContentType("text/wsag4people", new WSAg4PeopleEditorKit());
    	editorPane.setEditable(editable);
    
    	var keyListener = KeyAdapter{
        	override function keyReleased(e: java.awt.event.KeyEvent) {
            	updateComponentField();
                if(onKeyUp != null){onKeyUp(e ); }
			}
		};

        editorPane.addKeyListener( keyListener );
		scrollPane = new JScrollPane(editorPane);
		scrollPane.setPreferredSize(new Dimension(500, 500));
        return scrollPane;    
	}
}