package frontend.detail;
import frontend.Main;
import frontend.ADADocument;
import frontend.ADAService;
import frontend.DefaultBackground;
import frontend.Tooltip;
import frontend.PopupWindow;
import frontend.popupWindows.ConfirmWindow;
import frontend.popupWindows.MessageWindow;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.layout.Stack;
import javafx.scene.layout.VBox;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.geometry.HPos;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.input.MouseEvent;
import javafx.scene.Cursor;
import javafx.scene.effect.Effect;
import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.Lighting;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.light.DistantLight;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;

/**
 * @author AJurado
 */
 
public class Tab extends CustomNode {
    
    /**
     * Document
     */
    public var doc: ADADocument on replace{
        tabName = doc.docName;
        tt.text = tabName;
        wsagViewContent = doc.docContent;
        doc.clearErrorsAndWarnings(parentTabbedMenu.editor);
        if(id != "metricXML" and tabName != "newTemplate.wsag" and tabName != "newOffer.wsag"){
            //println("Antes de pasar a 4people: {doc.docContent}");
            try{
                wsag4pViewContent = ADAService.xmlToWSAg4People(doc.docContent);
            }catch(e){
                wsag4pViewContent = "WSAg4People view couldn�t be generated. It may be due to a connection problem or a bad syntax";
            }
        	//println("Despues de pasar a 4people: {wsag4pViewContent}");
        }
        id = doc.docName;
        //wsagViewCursorPosition = 0;
        //wsag4pViewCursorPosition = 0;
    }
    
    /**
     * Text to show on this Tab
     */
    public var tabName: String;
    
    /**
     * Xml content of the document which it represents this tab
     */
	public var wsagViewContent: String;
	
	/**
	 * Position of the cursor in this view
	 */
	public var wsagViewCursorPosition:Integer = 0;// on replace{
	//    println("posicion del cursor de la pesta�a {tabName} en la vista wsag es {wsagViewCursorPosition}");
	//}
	
	/**
	 * Wsag4People content of the document which it represents this tab 
	 */
	public var wsag4pViewContent: String;
	
	/**
	 * Position of the cursor in this view
	 */
	public var wsag4pViewCursorPosition:Integer = 0;// on replace{
	//    println("posicion del cursor de la pesta�a {tabName} en la vista 4p es {wsag4pViewCursorPosition}");
	//}
	
	public var wsagViewPos:java.awt.Point = new java.awt.Point(0,0);
	
	public var wsag4pViewPos:java.awt.Point = new java.awt.Point(0,0);
	 
	/**
	 * Active view on this tab. Accepted values [wsag, wsag4p]
	 */
	public var activeView: String = "wsag";
	  
	/**
	 * TabbedMenu to which this Tab belongs
	 */
	public var parentTabbedMenu: TabbedMenu;
	
	/**
     * Index of this Tab in parentTabbedMenu
     */
	public var index: Integer;
	
	/**
	 * Represents the state of this Tab. When true, tabContent is showed on editor
	 */
	protected var isActiveTab: Boolean on replace{
	    if(isActiveTab){
	        bg.effect = effectActiveTab;
	    }else{
	        bg.effect = effectInactiveTab;
	    }
	}
	
	/**
	 * True when tab content is saved on disk. False when changes are not saved.
	 */
	public var isSaved: Boolean = true on replace oldValue{
	    if(tabName != ""){
	        var temporalString = tabName.substring(0, 2);
    	    if(isSaved){
    	        if(temporalString == "* "){
    	            tabName = tabName.substring(2, tabName.length());
    	        }
    	    }else{
    	        if(temporalString != "* "){
    	            tabName = "* {tabName}"
    	        }
    	    }
	    }
	}
	
	/**
	 * Effect shown when this tab is active
	 */
	var effectActiveTab: Effect = Lighting{
	    light: DistantLight{
	        azimuth: 230
	        elevation: 60
	    }
	    specularConstant: 0.7
	    surfaceScale: 1.5
	}
	
	/**
	 * Effect shown when this tab is inactive
	 */
	var effectInactiveTab: Effect = ColorAdjust{
	    brightness: -0.25
	}
	
	/**
	 * Tooltip to show full document name
	 */
	protected var tt:Tooltip = Tooltip{
		text: tabName
	}
	
	/**
	 * InnerShadow color on close tab button is binded to it. This creates a pressed
	 * button effect.
	 */
	var shadowColor:Color = Color.TRANSPARENT;
	
	/**
	 * Tab background
	 */
	var bg: DefaultBackground;
	
	/**
	 * It makes this Tab the active tab.
	 */
	public function selectTab(select:Boolean):Boolean{
	    if(select){
	        if(parentTabbedMenu.continueIfUnsavedChanges()){
	            if(id != "metricXML"){
    	    		parentTabbedMenu.editor.disable = false;
    	    	}else{
    	    	    this.requestFocus();
    	    	    parentTabbedMenu.editor.disable = true;
    	    	}
    	    	parentTabbedMenu.indexActiveTab = index;
    	    	isActiveTab = true;
    	    	selectView(activeView);
    	    	if(parentTabbedMenu.isVisibleTree){
    	    	    parentTabbedMenu.loadTree(wsagViewContent);
    	    	}
	        }
	    }else{
            isActiveTab = false;
            //Save caret position of active view before this tab is deselected
            println("Deseleccionamos la pesta�a {tabName} y...");
            saveCaretPosition();
	    }
	    return isActiveTab;
	}
	
	/**
	 * It shows selected view of this tab on editor
	 * @param view can be "wsag" "wsag4p" "tree"
	 */
	public function selectView(view:String){
	    if(id != "metricXML"){
	        activeView = view;
    	    if(view == "wsag"){
    	        parentTabbedMenu.editor.contentType = "text/wsag";
        	    parentTabbedMenu.editor.text = wsagViewContent;
        	    doc.setErrorsAndWarnings(parentTabbedMenu.editor);
        	}else if(view == "wsag4p"){
        	    parentTabbedMenu.editor.contentType = "text/wsag4people";
        	    parentTabbedMenu.editor.text = wsag4pViewContent;
        	    doc.setErrorsAndWarnings(parentTabbedMenu.editor);
        	}else{
        	    println("Error: m�todo selectView dentro de la clase Tab\nPar�metro incorrecto");
        	}
        	//Load caret position of new active view
	    	loadCaretPosition();
	    }else{
	        parentTabbedMenu.editor.contentType = "text/wsag";
	        parentTabbedMenu.editor.text = wsagViewContent;
	        doc.clearErrorsAndWarnings(parentTabbedMenu.editor);
	    }
	}
	
	/**
	 * Close this Tab.
	 */
	public function closeTab(){
	    if(isSaved){
	        if(id == "doubleViewTab"){
	            parentTabbedMenu.hideDoubleEditor();
	        }
	        parentTabbedMenu.closeTab(index);
	    }else{
	        var confWindow = ConfirmWindow{
    	        id: "confirm"
    	        message: "There are unsaved changes on this document.\nDo you want to close it without saving?"
    	    	yesAction: function(){
    	    	    isSaved = true;
    	    	    parentTabbedMenu.closeTab(index);
    	    	}
    	    	noAction: function(){
    	    	    //Window will be closed automatically when user clicks some button
    	    	}
    	    }
    	    PopupWindow.addPopupToScene(confWindow, scene);
	    }
	}
	
	function loadCaretPosition(){
	    if(activeView == "wsag"){
	        //println("Cargamos posicion WSAg de la pesta�a {tabName} = {wsagViewCursorPosition}");
	        parentTabbedMenu.editor.setCaretPosition(wsagViewCursorPosition, wsagViewPos);
	    }
	    if(activeView == "wsag4p"){
	        //println("Cargamos posicion WSAg4People de la pesta�a {tabName} = {wsag4pViewCursorPosition}");
	        parentTabbedMenu.editor.setCaretPosition(wsag4pViewCursorPosition, wsag4pViewPos);
	    }
	}
	
	public function saveCaretPosition(){
	    if(activeView == "wsag"){
	        //println("Guardamos posicion WSAg de la pesta�a {tabName} = {wsagViewCursorPosition}");
	        wsagViewCursorPosition = parentTabbedMenu.editor.getCaretPosition();
	        wsagViewPos = parentTabbedMenu.getViewPos();
	    }
	    if(activeView == "wsag4p"){
	        //println("Guardamos posicion WSAg4People de la pesta�a {tabName} = {wsag4pViewCursorPosition}");
	        wsag4pViewCursorPosition = parentTabbedMenu.editor.getCaretPosition();
	        wsag4pViewPos = parentTabbedMenu.getViewPos();
	    }
	}

    public override function create(): Node {
        isActiveTab = true;
        //tt.text = tabName;
		
        var c: Circle;
        var text: Label;
        return Group{
        	content: [
        		Stack{
        		    nodeHPos: HPos.LEFT
        		    content: [
						bg = DefaultBackground{
					    	width: bind text.width+20, height: 25
					    	effect: effectActiveTab
		    			    onMouseEntered: function( e: MouseEvent ): Void {
		    			        tt.show(e.sceneX, e.sceneY);
		    			    }
		    			    onMouseExited: function( e: MouseEvent ): Void {
		    			        tt.hide();
		    			    }
		    			    onMouseClicked: function( e: MouseEvent ): Void {
		    			        if(not isActiveTab){
		    			        	selectTab(true);
		    			        }
		    			    }            			    
		    			}
		    			text = Label{
							text: bind tabName
							textOverrun: OverrunStyle.ELLIPSES;
		            	    layoutInfo: LayoutInfo{
		            	        width: 200
		            	    }
						}
        		    ]
        		}
        		c = Circle {
    			    centerX: bind bg.x+bg.width-10, centerY: bind bg.y+10
					cache: true
    			    radius: 5
    			    fill: RadialGradient{
						centerX: c.centerX-2, centerY: c.centerY-2
    			        focusX: c.centerX, focusY:c.centerY
    			        radius: 5
    			        proportional: true
    			        stops: [
    			        	Stop { offset: 0.0, color: Color.WHITE }
    			            Stop { offset: 0.4, color: Color.RED }
    			            Stop { offset: 1.0, color: Color.RED }
    			        ]
					}
    			    effect: InnerShadow{
    			    	offsetX: 1, offsetY: 1
    			        width: 1, height: 1
    			        radius: 1
    			        color: bind shadowColor
    			    }
    			    onMouseEntered: function( e: MouseEvent ): Void {
    			    	c.cursor = Cursor.HAND;
    			    }
    			    onMousePressed: function( e: MouseEvent ): Void {
    					shadowColor = Color.BLACK;
    			    }
    			    onMouseReleased: function( e: MouseEvent ): Void {
    			    	shadowColor = Color.TRANSPARENT;
    			    }
    			    onMouseClicked: function( e: MouseEvent ): Void {
    			        tt.hide();
    			        doc.resetMaps();
    			        closeTab();
    			    }
        		}
        	]
        }
    }
}