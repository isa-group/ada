package frontend.detail;
import frontend.DefaultBackground;
import frontend.PopupWindow;
import frontend.TreeLoader;
import frontend.popupWindows.ConfirmWindow;
import frontend.popupWindows.MessageWindow;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.Cursor;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.layout.ClipView;
import javafx.scene.effect.InnerShadow;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Polygon;
import javafx.scene.paint.Color;
import javafx.geometry.VPos;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;
import javafx.animation.Interpolator;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.KeyEvent;
import javax.swing.tree.*;
import javax.swing.JTree;

/**
 * @author AJurado
 */
 
public class TabbedMenu extends CustomNode {
    
    def defaultEditorText:String = "To use the Agreement Document Analyser (ADA) you may:\n\n    -  Create a new Template or an Agreement offer, by clicking on respective �create� buttons.\n\n    -  Show to be edited a loaded document by clicking on its name from the left side panel.\n\n    -  Open an existing document stored locally in your machine, by clicking on respective �open� button.\n\n    -  Analyse documents loaded in the left side panel, by clicking the �analyse� button.";
    
    /**
     * Tabs on this tabbed menu
     */ 
    public var tabs: Tab[];
    
    /**
     * The width of this tabbed menu
     */
    public var width: Number;
    
    /**
     * The height of this tabbed menu
     */
    public var height: Number;
    
    /**
     * Index of active tab
     */
    public var indexActiveTab: Integer on replace oldIndex{
        tabs[oldIndex].selectTab(false);
    }
    
    /**
     * The editor pane to show tab content
     */
    public var editor: EditorPane = EditorPane{
    	width: bind width+treePane.translateX, height: bind height
    	text: defaultEditorText
    	disable: true
    	onKeyUp: function( e: java.awt.event.KeyEvent ): Void {
    	    var tab: Tab = tabs[indexActiveTab];
    	    if(tab.activeView == "wsag"){
    	        if(tab.wsagViewContent == editor.text){
    	            tab.isSaved = true;
    	        }else{
    	            tab.isSaved = false;
    	        }
    	    }else if(tab.activeView == "wsag4p"){
    	        if(tab.wsag4pViewContent == editor.text){
    	            tab.isSaved = true;
    	        }else{
    	        	tab.isSaved = false;
    	        }
    	    }
    	}
    }
    
    /**
     * Editor for template document on explainNonCompliance result
     */
    public var templateEditor:EditorPane = EditorPane{
    	width: bind width/2, height: bind height
    	onKeyUp: function( e: java.awt.event.KeyEvent ): Void {
    	    var tab: DoubleViewTab = tabs[indexActiveTab] as DoubleViewTab;
    	    if(tab.activeView == "wsag"){
    	        if(tab.templateWSAgContent == templateEditor.text and tab.offerWSAgContent == offerEditor.text){
    	            tab.isSaved = true;
    	        }else{
    	            tab.isSaved = false;
    	        }
    	    }else if(tab.activeView == "wsag4p"){
    	        if(tab.templateWSAg4pContent == templateEditor.text and tab.offerWSAg4pContent == offerEditor.text){
    	            tab.isSaved = true;
    	        }else{
    	            tab.isSaved = false;
    	        }
    	    }
    	}
    }
    /**
     * Editor for offer document on explainNonCompliance result
     */
    public var offerEditor:EditorPane = EditorPane{
        width: bind width/2, height: bind height
    	onKeyUp: function( e: java.awt.event.KeyEvent ): Void {
    	    var tab: DoubleViewTab = tabs[indexActiveTab] as DoubleViewTab;
    	    if(tab.activeView == "wsag"){
    	        if(tab.templateWSAgContent == templateEditor.text and tab.offerWSAgContent == offerEditor.text){
    	            tab.isSaved = true;
    	        }else{
    	            tab.isSaved = false;
    	        }
    	    }else if(tab.activeView == "wsag4p"){
    	        if(tab.templateWSAg4pContent == templateEditor.text and tab.offerWSAg4pContent == offerEditor.text){
    	            tab.isSaved = true;
    	        }else{
    	            tab.isSaved = false;
    	        }
    	    }
    	}
    }
    
    /**
     * Double editor view contains template and offer editors
     */
    var doubleEditorView:HBox = HBox{
        layoutY: 30
        layoutInfo: LayoutInfo{
            managed: false
            
        }
        content: [
        	templateEditor,
        	offerEditor
        ]
    }
    
    /**
     * True if double editor is shown
     */
    var isShownDoubleEditor:Boolean;
    
    //#################################################################
    //#####################   PRUEBA   ################################
    //#################################################################
    
    var verticalScrollBarFromEditor = (editor.getJComponent() as javax.swing.JScrollPane).getVerticalScrollBar();
    
    /**
     * A vertical bar beside the editor where errors will be shown
     */
    public var errorBar: Group = Group{
        layoutX: 0, layoutY: 0
        layoutInfo: LayoutInfo{
            managed: false
        }
        content: [
        	
        ]
    }
    
    //#################################################################
    //#####################   FIN PRUEBA   ############################
    //#################################################################
    
    /**
     * Clip value
     */
    var clipx: Number = 0.0;
    
    var returnVBox:VBox;
    
    public function addTab(tab:Tab){
        var found:Boolean = false;
        for(t in tabs){
            if(t.id == tab.id){
                if(t.index != indexActiveTab){
                    //Tab we want to add is already open but not active
                    t.selectTab(true);
                }else{
                    //Tab we want to add is already open and active
                    //Do nothing
                }
                found = true;
                break;
            }else if(t.id == "doubleViewTab"){
                if((t as DoubleViewTab).template.docName == tab.doc.docName or (t as DoubleViewTab).offer.docName == tab.doc.docName){
                    t.selectTab(true);
                    found = true;
                    break;
                }
            }
        }
        if(not found){
            var selected = tab.selectTab(true);
            if(selected){
                insert tab into tabs;
                if(sizeof tabs == 1){
                    if(tab.id != "metricXML"){
                        editor.disable = false;
                    }
    			}else{
    			    if(tab.id != "metricXML"){
    			        editor.disable = false;
    			    }else{
    			        editor.disable = true;
    			    }
    			}
            }
        }
    }
    
    public function closeTab(index:Integer){
        delete tabs[index];
        if(sizeof tabs > 0){
            for(tab in tabs[index..sizeof tabs-1]){
                tab.index -= 1;
            }
            if(indexActiveTab > index){
                indexActiveTab -= 1;
            }else if(indexActiveTab == index){
                if(index == sizeof tabs){
                    indexActiveTab -= 1;
                }
                tabs[sizeof tabs-1].selectTab(true);
            }
        }else{
            editor.contentType = "text/plain";
            editor.text = defaultEditorText;
            editor.disable = true;
        }
        //If size of total opened tabs is smaller than available size, all tabs must be viewed
        if(sizeof tabs * 220 <= width-20){
            clipx = 0;
        }
    }
    
    /**
     * Return true if there aren�t unsaved changes or there are unsaved changes but the user has choosed continue without save
     * Return false if there are unsaved changes and the user has choosed "cancel" 
     */
    public function continueIfUnsavedChanges():Boolean{
        var continueWithChanges:Boolean;
   		if((sizeof tabs > 0) and (not tabs[indexActiveTab].isSaved)){
            var puw = MessageWindow{
                id: "error"
                message: "There are unsaved changes on active tab.\nPlease, save it before to perform this action"
            }
            PopupWindow.addPopupToScene(puw, scene);
            //este false deber�a depender de la elecci�n del usuario a la pregunta "desea continuar? aceptar - cancelar"
            continueWithChanges = false;
        }else{
            continueWithChanges = true;
        }
        return continueWithChanges;
    }
    
    /**
     * Pane where a document is showed as a tree
     */
    public var treePane:TreePane = TreePane{
        layoutX: bind width, layoutY: 28
        width: 250, height: bind height
        layoutInfo: LayoutInfo{
            managed: false
        }
    }
    
    /**
     * Timeline that shows treePane
     */
    var t:Timeline = Timeline {
	    keyFrames: [
	    	KeyFrame{
	    	    time: 0s
	    	    canSkip: true
	    	    values: [treePane.translateX=>0]
	    	}
	        KeyFrame{
	            time: 0.1s
	            canSkip: true
	            values: [treePane.translateX=>-50 tween Interpolator.EASEOUT]
	        }
	        KeyFrame{
	            time: 0.5s
	            canSkip: true
	            values: [treePane.translateX=>-250 tween Interpolator.EASEIN]
	        }
	    ]
	}
    
    public var isVisibleTree:Boolean;
    
    public function loadTree(fileContent:String):Boolean{
        var successfulLoad:Boolean;
        try{
            var auxTree = TreeLoader.cargaJTree(fileContent);
    	    var root:TreeNode = auxTree.getModel().getRoot() as TreeNode;
    	    var rootString = root.toString();
    	    if(rootString.equalsIgnoreCase("wsag:Template") or rootString.equalsIgnoreCase("wsag:AgreementOffer")){
    	        treePane.newJTree = auxTree;
    	        successfulLoad = true;
    	    }else{
    	        var messageW = MessageWindow{
                    id: "error"
                    message: "Error loading tree view. It may be due to a bad syntax in XML."
                }
                PopupWindow.addPopupToScene(messageW, scene);
                successfulLoad = false;
    	    }
        }catch(e){
            var messageW = MessageWindow{
                id: "error"
                message: "Error loading tree view."
            }
            PopupWindow.addPopupToScene(messageW, scene);
        }
        return successfulLoad;
    }
    
    public function showTree(){
        if(loadTree(tabs[indexActiveTab].wsagViewContent)){
            isVisibleTree = true;
            t.rate = 1;
            t.play();
        }
    }
    
    public function hideTree(){
        if(isVisibleTree){
            isVisibleTree = false;
            t.rate = -1;
            t.play();
        }
    }
    
    public function showDoubleEditor(){
        //Deshabilitar y ocultar el editor original
        //Mostrar los dos nuevos editores
        if(not isShownDoubleEditor){
            editor.disable = true;
            editor.visible = false;
            insert doubleEditorView into returnVBox.content;
            isShownDoubleEditor = true;
        }
    }
    
    public function hideDoubleEditor(){
        //Ocultar los dos nuevos editores
        //habilitar y mostrar el editor original
        if(isShownDoubleEditor){
            editor.disable = false;
            editor.visible = true;
            delete doubleEditorView from returnVBox.content;
            isShownDoubleEditor = false;
        }
    }
    
    public function setCaretPosition(pos:Integer, view:java.awt.Point){
        editor.setCaretPosition(pos, view);
    }
    
    public function getCaretPosition():Integer{
        return editor.getCaretPosition();
    }
    
    public function getViewPos():java.awt.Point{
        return editor.getViewPos();
    }

    public override function create(): Node{
        var menu: Group;
        var leftArrow: Group;
        var rightArrow: Group;
        returnVBox = VBox {
            spacing: -2
            content: [
            	menu = Group{
            	    content: [
            	    	leftArrow = Group{
            	    	    translateY: 5
            	    	    content:[
	            	    	    DefaultBackground{
	            	    	        width: 10, height: 25
	            	    	        onMouseEntered: function( e: MouseEvent ): Void {
	            	    	            cursor = Cursor.HAND;
	            	    	        }
	            	    	        onMouseExited: function( e: MouseEvent ): Void {
	            	    	            cursor = Cursor.DEFAULT;
	            	    	        }
	            	    	        onMouseClicked: function( e: MouseEvent ): Void {
	            	    	            if(clipx >= 30){
	            	    	                clipx -= 30;
	            	    	            }
	            	    	        }
	            	    	    }
	            	    	    Polygon{
	            	    	        layoutX: 2.5, layoutY: 7.5
	            	    	        points: [
            	    	        		0.0, 5.0,
            	    	        		5.0, 0.0,
            	    	        		5.0, 10.0
            	    	            ]
	            	    	    }
            	    	    ]
            	    	}
            	    	ClipView{
            	    	    clipX: bind clipx
            	    	    translateX: 10
            	    	    width: bind width-20, height: 30
            	    		node: HBox{
            	    		    spacing: -5
	    	            	    height: 25 //tama�o de una pesta�a, deber�a ser sacada de Skin
	    	            	    content: bind tabs
            	    		}
            	    		pannable: false
    	            	}
    	            	rightArrow = Group{
    	            	    translateX: bind width-10, translateY: 5
    	            	    content:[
    	            	    	DefaultBackground{
	            	    	        width: 10, height: 25
	            	    	        onMouseEntered: function( e: MouseEvent ): Void {
	            	    	            cursor = Cursor.HAND;
	            	    	        }
	            	    	        onMouseExited: function( e: MouseEvent ): Void {
	            	    	            cursor = Cursor.DEFAULT;
	            	    	        }
	            	    	        onMouseClicked: function( e: MouseEvent ): Void {
	            	    	            if((sizeof tabs * 220)-clipx > width-20){
	            	    	                clipx += 30;
	            	    	            }
	            	    	        }
	            	    	    }
	            	    	    Polygon{
	            	    	        layoutX: 2.5, layoutY: 7.5
	            	    	        points: [
            	    	            	0.0, 0.0,
            	    	            	0.0, 10.0,
            	    	            	5.0, 5.0
            	    	            ]
	            	    	    }
    	            	    ]
    	            	}
            	    ]
            	}
            	editor,
            	treePane
            ]
        };
        return returnVBox;
    }
}