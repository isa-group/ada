package frontend.detail;
import frontend.ADADocument;
import frontend.ADAService;
import frontend.Tooltip;

/**
 * @author AJurado
 */
public class DoubleViewTab extends Tab{
    
    override var id = "doubleViewTab" on replace{
        id = "doubleViewTab";
    }
    
    override var tabName = "ExplainNonCompliance result" on replace{
        if(tabName != "* ExplainNonCompliance result" and tabName != "ExplainNonCompliance result"){
            tabName = "ExplainNonCompliance result";
        }
    }
    
    public var template:ADADocument on replace{
        templateWSAgContent = template.docContent;
        try{
        	templateWSAg4pContent = ADAService.xmlToWSAg4People(template.docContent);
        }catch(e){
            templateWSAg4pContent = "WSAg4People view couldn�t be generated. It may be due to a connection problem or a bad syntax";
        }
        template.clearErrorsAndWarnings(parentTabbedMenu.templateEditor);
        tt.text = "Template: {template.docName}\nOffer: {offer.docName}";
    }
    
    public var offer:ADADocument on replace{
		offerWSAgContent = offer.docContent;
		try{
			offerWSAg4pContent = ADAService.xmlToWSAg4People(offer.docContent);
		}catch(e){
		    offerWSAg4pContent = "WSAg4People view couldn�t be generated. It may be due to a connection problem or a bad syntax";
		}
		offer.clearErrorsAndWarnings(parentTabbedMenu.offerEditor);
		tt.text = "Template: {template.docName}\nOffer: {offer.docName}";
    }
    
    public var templateWSAgContent:String;
    
    public var templateWSAg4pContent:String;
    
    public var offerWSAgContent:String;
    
    public var offerWSAg4pContent:String;
    
    override function selectTab(select:Boolean):Boolean{
        if(select){
	        if(parentTabbedMenu.continueIfUnsavedChanges()){
                parentTabbedMenu.hideTree();
                parentTabbedMenu.showDoubleEditor();
		    	parentTabbedMenu.indexActiveTab = index;
		    	isActiveTab = true;
		    	selectView(activeView);
	        }
	    }else{
	        parentTabbedMenu.hideDoubleEditor();
	        isActiveTab = false;
	    }
	    return isActiveTab;
    }
    
    override function closeTab(){
        template.resetMaps();
        offer.resetMaps();
        super.closeTab();
    }
    
    override function selectView(view:String){
        activeView = view;
	    if(view == "wsag"){
	        parentTabbedMenu.templateEditor.contentType = "text/wsag";
    	    parentTabbedMenu.templateEditor.text = templateWSAgContent;
    	    parentTabbedMenu.offerEditor.contentType = "text/wsag";
    	    parentTabbedMenu.offerEditor.text = offerWSAgContent;
    	    template.setErrorsAndWarnings(parentTabbedMenu.templateEditor);
    	    offer.setErrorsAndWarnings(parentTabbedMenu.offerEditor);
    	}else if(view == "wsag4p"){
    	    parentTabbedMenu.templateEditor.contentType = "text/wsag4people";
    	    parentTabbedMenu.templateEditor.text = templateWSAg4pContent;
    	    parentTabbedMenu.offerEditor.contentType = "text/wsag4people";
    	    parentTabbedMenu.offerEditor.text = offerWSAg4pContent;
    	    template.setErrorsAndWarnings(parentTabbedMenu.templateEditor);
    	    offer.setErrorsAndWarnings(parentTabbedMenu.offerEditor);
    	}else{
    	    println("Error: m�todo selectView dentro de la clase Tab\nPar�metro incorrecto");
    	}
    }
    
}