package frontend.detail;

import java.awt.Color;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;

/*
 * Se han a�adido indices a los metodos, para poder pintar del mismo
 * color pares error-causa.
 * Hemos creado una array de 10 colores para ello, de forma que hasta el 11� par
 * no se repetira color :)
 */
public class ADAHighlighter {
	
	// Some instances of the private subclass of the default highlight painter
//	Highlighter.HighlightPainter errorHighlightPainter = new ADAHighlightPainter(Color.RED);
//	Highlighter.HighlightPainter warningHighlightPainter = new ADAHighlightPainter(Color.ORANGE);
	private Color[] colours;
	
	//Text component
	JTextComponent textComp;
	
	public ADAHighlighter(JTextComponent textComp){
		this.textComp = textComp;
		Color[] aux = {Color.RED,Color.ORANGE,Color.CYAN,Color.GREEN,Color.PINK,Color.YELLOW,Color.DARK_GRAY,Color.BLUE,Color.MAGENTA,Color.LIGHT_GRAY};
		colours = aux;
	}
	
	public void addSetHighlight(Collection c, int index){
		Iterator it = c.iterator();
		while (it.hasNext()){
			String s = it.next().toString();
			addErrorHighlight(s, index);
		}
	}
	
	// Creates highlights around all occurrences of pattern in textComp
	public void addErrorHighlight(String pattern, int index) {
		//vamos a colorear de distintos colores
		/*
		 * Colores:
		 * CYAN x
		 * DARK_GRAY x
		 * LIGHT_GRAY x
		 * BLUE x
		 * RED x 
		 * ORANGE x
		 * GREEN x  
		 * MAGENTA x
		 * PINK x 
		 * YELLOW x
		 */
		int auxPos = index % colours.length;
		Highlighter.HighlightPainter errorHighlightPainter = new ADAHighlightPainter(colours[auxPos]);
	    try {
	        Highlighter hilite = textComp.getHighlighter();
	        Document doc = textComp.getDocument();
	        String text = doc.getText(0, doc.getLength());
	        boolean isWSAg = text.startsWith("<");
	        int pos = 0;
	        
	        int indexOfDash = pattern.indexOf("-");
	        //If it�s an OfferItem error, for example "SDT1-Size"
	        if(indexOfDash >= 0){
	        	String sdtName = pattern.substring(0, indexOfDash);
	        	String offerItemName = pattern.substring(indexOfDash+1, pattern.length());
	        	while((pos = text.indexOf(sdtName, pos)) >= 0){
	        		pos = text.indexOf(offerItemName, pos);
	        		int start = pos;
	        		int end = pos+offerItemName.length();
	        		if(isWSAg){
	        			//Searching term start position
	        			while(!text.startsWith("<OfferItem", start)){
	        				start--;
	        			}
	        			//Searching term type end position
	        			while(!text.startsWith("</OfferItem>", end)){
	        				end++;
	        			}
	        			end += "</OfferItem>".length();
	        			hilite.addHighlight(start, end+1, errorHighlightPainter);
	        		}else{
	        			//Start position is set by default
	        			while(!text.startsWith(" ", end)){
	        				end++;
	        			}
	        			hilite.addHighlight(start, end, errorHighlightPainter);
	        		}
	        	}
	        }else{
	        	// Search for pattern
	        	pos = text.indexOf(pattern, pos);
		        while (pos >= 0) {
		            // Create highlighter using private painter and apply around pattern
		        	int start = pos;
		        	int end = pos+pattern.length();
		        	if(isWSAg){
		        		//Comprobamos que no est� dentro de un comentario
			        	if(!isInsideAWSAgComment(text, pos)){
			        		//Searching term start position
			        		while(!text.startsWith("<wsag:", start)){
				        		start--;
				        	}
			        		//Setting term type start and end position
			        		int termStart = start+6;
			        		int termEnd = start+6;
			        		//Searching term type end position
			        		while(!text.startsWith(" ", termEnd) && !text.startsWith(">", termEnd)){
			        			termEnd++;
			        		}
			        		String term = text.substring(termStart, termEnd);
			        		//Searching term end position
				        	while(!text.startsWith("</wsag:"+term+">", end)){
				        		end++;
				        	}
				        	end += new String ("</wsag:"+term+">").length();
				        	hilite.addHighlight(start, end+1, errorHighlightPainter);
			        	}
		        	}else{
		        		if(!isInsideA4PeopleComment(text, pos)){
		        			while(!text.startsWith("\n", start)){
			        			start--;
			        		}
			        		while(!text.startsWith(";", end)){
			        			end++;
			        		}
			        		hilite.addHighlight(start+1, end+1, errorHighlightPainter);
		        		}
		        	}
		            pos = end;
		            pos = text.indexOf(pattern, pos);
		        }
	        }
	    } catch (BadLocationException e) {
	    	e.printStackTrace();
	    }
	}
	
	// Creates highlights around all occurrences of pattern in textComp
//	public void addWarningHighlight(String pattern, int index) {
//		
//		int auxPos = index % colours.length;
//		Highlighter.HighlightPainter warningHighlightPainter = new ADAHighlightPainter(colours[auxPos]);
//		
//	    try {
//	        Highlighter hilite = textComp.getHighlighter();
//	        Document doc = textComp.getDocument();
//	        String text = doc.getText(0, doc.getLength());
//	        boolean isWSAg = text.startsWith("<");
//	        int pos = 0;
//
//	        // Search for pattern
//	        while ((pos = text.indexOf(pattern, pos)) >= 0) {
//	            // Create highlighter using private painter and apply around pattern
//	        	int start = pos;
//	        	int end = pos+pattern.length();
//	        	if(isWSAg){
//	        		//Searching term start position
//	        		while(!text.startsWith("<wsag:", start)){
//		        		start--;
//		        	}
//	        		//Setting term type start and end position
//	        		int termStart = start+6;
//	        		int termEnd = start+6;
//	        		//Searching term type end position
//	        		while(!text.startsWith(" ", termEnd) && !text.startsWith(">", termEnd)){
//	        			termEnd++;
//	        		}
//	        		String term = text.substring(termStart, termEnd);
//	        		//Searching term end position
//		        	while(!text.startsWith("</wsag:"+term+">", end)){
//		        		end++;
//		        	}
//		        	end += new String ("</wsag:"+term+">").length();
//		        	hilite.addHighlight(start, end+1, warningHighlightPainter);
//	        	}else{
//	        		while(!text.startsWith("\n", start)){
//	        			start--;
//	        		}
//	        		while(!text.startsWith(";", end)){
//	        			end++;
//	        		}
//	        		hilite.addHighlight(start+1, end+1, warningHighlightPainter);
//	        	}
//	            pos += end;
//	        }
//	    } catch (BadLocationException e) {
//	    	e.printStackTrace();
//	    }
//	}

	// Removes only our private highlights
	public void clearHighlights() {
	    Highlighter hilite = textComp.getHighlighter();
	    Highlighter.Highlight[] hilites = hilite.getHighlights();

	    for (int i=0; i<hilites.length; i++) {
	        if (hilites[i].getPainter() instanceof ADAHighlightPainter) {
	            hilite.removeHighlight(hilites[i]);
	        }
	    }
	}
	
	//Check if an integer position is inside an XML comment
	private Boolean isInsideAWSAgComment(String text, int pos){
		Boolean res = null;
		
		String leftSideOfPattern = text.substring(0, pos);
    	String rightSideOfPattern = text.substring(pos);
    	
    	int closeCommentPosAtLeft = leftSideOfPattern.lastIndexOf("-->");
    	int openCommentPosAtLeft = leftSideOfPattern.lastIndexOf("<!--");
    	if(openCommentPosAtLeft == -1 || closeCommentPosAtLeft > openCommentPosAtLeft){
    		//La �ltima etiqueta antes del patr�n es la de cerrar, no estamos dentro de un comentario
    		res = false;
    		//System.out.println("En la posici�n "+start+" no estamos en un comentario");
    	}else{
    		//La �ltima etiqueta antes del patr�n es la de abrir, podemos estar dentro de un comentario
    		//si despu�s del patr�n hay una etiqueta de cerrar
    		if(rightSideOfPattern.indexOf("-->") != -1){
    			//Estamos dentro de un comentario, no hacer nada!
    			//System.out.println("En la posici�n "+start+" estamos en un comentario!");
    			res = true;
    		}else{
    			res = false;
    		}
    	}
    	return res;
	}
	
	private Boolean isInsideA4PeopleComment(String text, int pos){
		Boolean res = null;
		
		String leftSideOfPattern = text.substring(0, pos+2);
		
		int closeCommentPosAtLeft = leftSideOfPattern.lastIndexOf("\n");
		int openCommentPosAtLeft = leftSideOfPattern.lastIndexOf("//");
		
		if(closeCommentPosAtLeft > openCommentPosAtLeft){
			//No hay comentario
			res = false;
		}else{
			//Hay comentario
			res = true;
		}		
		return res;
	}

	// A private subclass of the default highlight painter
	class ADAHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {
	    public ADAHighlightPainter(Color color) {
	        super(color);
	    }
	}
	
}
