package frontend;
import frontend.operations.Operation;
import frontend.DefaultBackground;
import javafx.scene.Scene;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Stack;
import javafx.scene.layout.LayoutInfo;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.paint.Color;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.Cursor;
import javafx.scene.text.Text;
import javafx.scene.control.ListView;
import javafx.scene.control.Button;

 /**
  * @author AJurado
  */

 /**
  * Static function
  */
public function addPopupToScene(puw:Node, s:Scene){
    if(s.lookup(puw.id) == null){
        for(node in s.content){
            node.disable = true;
        }
        insert puw into s.content;
    }else{
        println("Ya hay una ventana emergente con el mismo id: <{puw.id}>\nSi el id est� vac�o es que te has olvidado de asignarle un id a la nueva ventana emergentey coincide con el id de la ventana principal que es la cadena vac�a\nA�ade un id a tu ventana y deber�a solucionarse");
    }
}

public function closeWindow(id:String, scene:Scene){
    var s = scene;
	delete scene.lookup(id) from scene.content;
	if(s.content[sizeof s.content-1].disabled){
	    for(node in s.content){
            node.disable = false;
        };
	}
}

 /**
  * Defines the background and a close button for a popup window
  */
public class PopupWindow extends CustomNode {
    
    public var width:Number;
    
    public var height:Number;
    
    var background:DefaultBackground;
    
    public function closeWindow(){
        var s = scene;
    	delete scene.lookup(id) from scene.content;
    	if(s.content[sizeof s.content-1].disabled){
    	    for(node in s.content){
                node.disable = false;
            };
    	}
    }
    
    var window = Group{
        var closeButton:Stack;
        cache: true
        effect: DropShadow{
            radius: 10
            offsetX: 2, offsetY: 2
            color: Color.DIMGREY
        }
    	content: [
    		background = DefaultBackground{
		        arcWidth: 10, arcHeight: 10
		        width: bind width, height: bind height
		    }
		    closeButton = Stack{
		        translateX: bind background.boundsInLocal.width-30, translateY: 10
		        content: [
		        	Circle{
    		            radius: 10
    		            fill: Color.GREY
    		            effect: DropShadow{
    		                radius: 5
    		                width: 10, height: 10
    		                offsetX: 1, offsetY: 1
    		            }
    		            onMouseEntered: function( e: MouseEvent ): Void{
    		                cursor = Cursor.HAND
    		            }
    		            onMouseExited: function( e: MouseEvent ): Void{
    		                cursor = Cursor.DEFAULT
    		            }
    		            onMouseClicked: function( e: MouseEvent ): Void{
    		                closeWindow(id, scene);
    		            }
    		        }
    		        Line{
        	    	    startX: 0, startY: 0
        	    	    endX: 6, endY: 6
        	    	    stroke: Color.WHITE
        	    	}
        	    	Line{
        	    	    startX: 6, startY: 0
        	    	    endX: 0, endY: 6
        	    	    stroke: Color.WHITE
        	    	}
		        ]
		    }
    	]
    }

    public override function create(): Node{
        return Group{
            layoutX: bind scene.width/2-(window.boundsInLocal.width/2), layoutY: bind scene.height/2-(window.boundsInLocal.height/2)
            content: [
            	window
            ]
        };
    }
}