package frontend.buttons;
import frontend.Tooltip;
import javafx.scene.Node;
import javafx.scene.CustomNode;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.effect.Reflection;
import javafx.scene.Cursor;
import javafx.geometry.HPos;
import javafx.scene.layout.Stack;
import javafx.util.Sequences;

/**
 * @author AJurado
 */

public class ADAButton extends CustomNode {
    
	public var imageURL: String on replace{
	    imageBt = Image{
	        url: imageURL
	    }
	}
	
	public var action: function();
    
    public var imageBt: Image;
    
    public var op = 0.9;
    
    public var ttText:String on replace{
        tt.text = ttText;
    }
    
    var tt: Tooltip = Tooltip{
        text: ttText
    }
    
    var rect:Rectangle = Rectangle{
	    width: 50, height: 50
	    fill: Color.TRANSPARENT
		onMouseEntered: function( e: MouseEvent ): Void {
		    op = 1.0;
		    rect.cursor = Cursor.HAND;
		    tt.show(e.sceneX, e.sceneY);
		}
		onMouseExited: function( e: MouseEvent ): Void {
		    op = 0.9;
		    rect.cursor = Cursor.DEFAULT;
		    tt.hide();
		}
		onMouseClicked: function( e: MouseEvent ): Void {
		    tt.hide();
		    action();
		}
	}
	
	var stack:Stack = Stack{
	    content: [
	    	rect,
	    	ImageView {
				image: imageBt
				opacity: bind op
				effect: Reflection{
				    fraction: 0.4
				    topOpacity: 0.75
				    topOffset: 1
				}
			}
	    ]
	}

    public override function create(): Node {
        return stack;
    }
}