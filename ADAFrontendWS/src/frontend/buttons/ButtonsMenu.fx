package frontend.buttons;

import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Stack;
import javafx.scene.layout.LayoutInfo;
import javafx.geometry.HPos;
import frontend.DefaultBackground;

/**
 * @author AJurado
 */

public class ButtonsMenu extends CustomNode{
    
    /**
     * Buttons
     */
    public var buttons: ADAButton[];
    
    /**
     * Views buttons
     */
    public var views: ADAButton[];
    
    /**
     * This menu width
     */
    public var width:Number;
    
    /**
     * Background menu
     */
    var background: DefaultBackground;
    
    public override function create(): Node{
        return Group{
            content: [
            	Stack{
            	    nodeHPos: HPos.LEFT
            	    content: [
            			background = DefaultBackground{
            			    width: bind width, height: 70
            			}
            			HBox{
            			    translateY: 10
							spacing: 10
            			    content: buttons
            			}
            			HBox{
            			    layoutInfo: LayoutInfo{
            			        width: bind width
            			    }
            			    hpos: HPos.RIGHT
            			    translateX: -10, translateY: 10
            			    spacing: 10
            			    content: views
            			}
					]      	    
            	}
            ]
        }
    }
}