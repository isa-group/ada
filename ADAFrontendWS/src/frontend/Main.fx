package frontend;
import frontend.buttons.*;
import frontend.operations.*;
import frontend.master.AccordionMenu;
import frontend.master.AccordionItem;
import frontend.detail.*;
import frontend.detail.editorkits.WSAgView;
import frontend.popupWindows.*;
import frontend.log.*;
import uuidGeneration.*;
import es.us.isa.ada.exceptions.BadSyntaxException;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.lang.StringBuilder;
import javax.swing.JTree;
import javafx.stage.Stage;
import javafx.stage.AppletStageExtension;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.scene.Cursor;
import javafx.scene.text.Text;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.paint.Paint;
import javafx.scene.Group;
import javafx.util.Sequences;
import java.util.Collection;
import java.util.UUID;


/**
 * @author AJurado
 */

var stage: Stage;
public var scene: Scene;

var stageW:Number = 800;
var stageH:Number = 600;

var codebase:String = "http://www.isa.us.es/ada.source/";

var sessionId:UUID;

public function getSessionId():String{
    return sessionId.toString();
}

//Variables que reciben valor desde el navegador mediante javascript
public var browserInfo:String;

public var docRecievedName:String;
public var docRecievedPath:String;

public var recievedOperation:String;

var openedTemplates:ADADocument[] = [];
var openedOffers:ADADocument[] = [];

var fileM = FileManager{};

var buttonsMenu: ButtonsMenu = ButtonsMenu{
    width: bind stageW
    buttons: [
    	ADAButton{
    	    imageURL: "{__DIR__}buttons/icons/newTemp.png"
    	    action: function(){
    	        if(editZone.continueIfUnsavedChanges()){
        	        var url = new URL("{codebase}docs/blankTemplateWSAg.wsag");
        	        var url4People = new URL("{codebase}docs/blankTemplateWSAg4People.wsag");
        	    	insert fileM.openBlankDocument(url.openStream(), url4People.openStream(), "template") into openedTemplates;
    	        }
    	    }
    	    ttText: "Create New Template"
    	}
    	ADAButton{
    	    imageURL: "{__DIR__}buttons/icons/newOffer.png"
    	    action: function(){
    	        if(editZone.continueIfUnsavedChanges()){
    	        	var url = new URL("{codebase}docs/blankAgreementOfferWSAg.wsag");
        	        var url4People = new URL("{codebase}docs/blankAgreementOfferWSAg4People.wsag");
        	        insert fileM.openBlankDocument(url.openStream(), url4People.openStream(), "offer") into openedOffers;    	            
    	        }
    	    }
    	    ttText: "Create New Offer"
    	}
    	ADAButton{
    		imageURL: "{__DIR__}buttons/icons/openTemp.png"
    	    action: function(){
    			if(editZone.continueIfUnsavedChanges()){
    	            insert fileM.openFile(null, "template", false) into openedTemplates;
    	        }
    		}
    		ttText: "Open Template"
    	}
    	ADAButton{
    	    imageURL: "{__DIR__}buttons/icons/openOffer.png"
    	    action: function(){
    	        if(editZone.continueIfUnsavedChanges()){
    	            insert fileM.openFile(null, "offer", false) into openedOffers;
    	        }
    	    }
    	    ttText: "Open Offer"
    	}
    	ADAButton{
			imageURL: "{__DIR__}buttons/icons/save.png"
			action: function(){
			    if(sizeof editZone.tabs == 0){
			        var puw = MessageWindow{
			            id: "error"
			            message: "Error: Before saving a document you must open it in the editor panel and modify it."
			        }
			        PopupWindow.addPopupToScene(puw, scene);
			    }else if(editZone.tabs[editZone.indexActiveTab].isSaved){
			        var puw = MessageWindow{
			            id: "error"
			            message: "Error: There are no changes to save on this document"
			        }
			        PopupWindow.addPopupToScene(puw, scene);
			    }else{
			        if(editZone.tabs[editZone.indexActiveTab].id == "doubleViewTab"){
			            fileM.saveFilesFromDoubleViewTab();
			        }else{
			            fileM.saveFile();
			        }
			    }
			}
			ttText: "Save"
		}
		ADAButton{
		    imageURL: "{__DIR__}buttons/icons/analyze.png"
		    action: function(){
		        if(not operationsList.isVisible){
		            insert operationsList into scene.content;
		            operationsList.isVisible = true;
		        }else{
		            delete operationsList from scene.content;
		            operationsList.isVisible = false;
		        }
		    }
		    ttText: "Analyse"
		}
		ADAButton{
		    imageURL: "{__DIR__}buttons/icons/getMetric.png"
		    action: function(){
				fileM.getMetric();
		    }
		    ttText: "Get metrics for agreement variables"
		}
		ADAButton{
		    imageURL: "{__DIR__}buttons/icons/setMetric.png"
		    action: function(){
		        fileM.addMetric();
		    }
		    ttText: "Upload a custom metric file"
		}
    ]
    views: [
    	ADAButton{
    	    imageURL: "{__DIR__}buttons/icons/wsag.png"
    	    action: function(){
    	        if(editZone.continueIfUnsavedChanges()){
    	            //Save caret position of active view before it�s modified
        	        var tab = editZone.tabs[editZone.indexActiveTab];
        	        tab.saveCaretPosition();
        	        tab.selectView("wsag");
    	        }
    	    }
    	    ttText: "Show XML view"
    	}
    	ADAButton{
    	    imageURL: "{__DIR__}buttons/icons/wsag4P.png"
    	    action: function(){
    	        if(editZone.continueIfUnsavedChanges()){
    	            //Save caret position of active view before it�s modified
        	        var tab = editZone.tabs[editZone.indexActiveTab];
        	        tab.saveCaretPosition();
        	        tab.selectView("wsag4p");
    	        }
    	    }
    	    ttText: "Show WSAG4People view"
    	}
    	ADAButton{
    	    imageURL: "{__DIR__}buttons/icons/tree.png"
    	    action: function(){
    	        if(editZone.continueIfUnsavedChanges()){
    	            if(editZone.tabs[editZone.indexActiveTab].id != "doubleViewTab"){
    	                if(editZone.isVisibleTree){
        	                editZone.hideTree();
        	            }else{
        	                if(sizeof editZone.tabs > 0){
        	                    editZone.showTree();
        	                }
        	            }
    	            }
    	        }
    	    }
    	    ttText: "Show Tree view"
    	}
    ]
}

var leftMenu: AccordionMenu = AccordionMenu{}

public var editZone: TabbedMenu = TabbedMenu{
    width: bind stageW-leftMenu.width-15, height: bind stageH-100
}

var console:Console = Console{
    id: "console" //Important!! This id makes posible to close console
    x: bind leftMenu.width, y: bind stageH //x = tama�o de leftMenu, y = tama�o de la scene
    width: bind editZone.width-2, minHeight: 100
    layoutInfo: LayoutInfo{
        managed: false
    }
    sceneAux: scene
}

var operations:Operation[] = [
	Operation{
	    opName: "Check Conflicts (single-document)"
	    nDocs: 1
	    nTemps: 0
	    nOffers: 0
	    console: console
	    listenerCallback: function(result, docs){
	        if(result != null){
	            var resultWindow:ResultWindow;
	            var isConsistent = result[0] as Boolean;
    	        var hasWarnings = result[1] as Boolean;
    	        if(isConsistent){
    	            if(hasWarnings){
        	            resultWindow = ResultWindow{
        	                id: "consistentWithWarnings"
        	            }
        	        }else{
        	            resultWindow = ResultWindow{
           					id: "consistentWithNoWarnings"
           				}
        	        }
    	        }else{
    	            resultWindow = ResultWindow{
    	                id: "inconsistent"
    	            }
    	        }
    	        PopupWindow.addPopupToScene(resultWindow, scene);
    	        
    	        //Adding log
    	        var resultADA:String = "{isConsistent}";
    	        LogTaskBase{
    	            operation: operations[0].opName
    	            doc1: [docs[0].docName, docs[0].docContent, docs[0].docDefault] as nativearray of Object //nativearray creates a java array instead a javafx sequence
    	            doc2: [docs[1].docName, docs[1].docContent, docs[1].docDefault] as nativearray of Object
    	            consoleLog: console.getConsoleText()
    	            resultADA: resultADA
    	            sessionId: sessionId.toString()
    	            browserInfo: browserInfo
    	            createTask: function(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo){
    	                return new AddLogQuery(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo);
    	            }
    	        }.start();
	        }else{
	            var messageWindow:MessageWindow = MessageWindow{
					id: "error"
					message: "There was an error: It may be due to a syntax error, please check the document syntax."
	            }
	            PopupWindow.addPopupToScene(messageWindow, scene);
	        }
	    }
	    execute: function(listener, docs){
	        var ww = WaitWindow{
                id: "wait"
                message: "Analysing, please wait"
            }
            PopupWindow.addPopupToScene(ww, scene);
	        
	        //Everytime execute function is called we will create a new JavaTaskBase and start it
	        ADATaskBase{
	            listener: listener
	            docs: docs
	            createTask: function(listener, docs){
	                return new CheckConsistencyTask(listener, docs[0].docContent);
	            }
	            onDone: function(){
	                PopupWindow.closeWindow("wait", scene);
	            }
	        }.start();
	    }
	    /*execute: function(docs:ADADocument[]){
	        docs[0].resetMaps();
	        console.showConsole();
	        //Este m�todo execute ser� borrado
	        //Se cambiar� por un m�todo create que crear� una RunnableOperation
			try{
				var resultWindow:ResultWindow;
				//println("Checking consistency..........");
				console.addLine("Checking consistency..........");
       	       	var isConsistent = ADAService.isConsistent(docs[0].docContent);
              	if(isConsistent){
              	    //println("    It�s consistent");
              	    console.setResult("CONSISTENT!", Color.GREEN);
              	    //println("    Checking warnings.........");
              	    console.addLine("Checking warnings..........");
					var hasWarnings = ADAService.hasWarnings(docs[0].docContent);
					if(hasWarnings){
					    //println("        It has warnings");
					    console.setResult("WARNINGS", Color.RED);
   						resultWindow = ResultWindow{
   							id: "consistentWithWarnings"
   						}
   					}else{
   					    //println("        It has no warnings");
   					    console.setResult("NO WARNINGS", Color.GREEN);
   						resultWindow = ResultWindow{
   							id: "consistentWithNoWarnings"
   						}
   					}
				}else{
				    //println("    It�s not consistent");
				    console.setResult("INCONSISTENT!", Color.RED);
					resultWindow = ResultWindow{
						id: "inconsistent"
					}
				}
				PopupWindow.addPopupToScene(resultWindow, scene);
			}catch(e:java.net.ConnectException){
				println("serverExceptionOnCheckConsistency");
				var messageW = MessageWindow{
					id: "error"
					message: "Unable to connect to server. Please try again later."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}catch(e:org.apache.axis.AxisFault){
				println("AxisFaultException");
				var messageW = MessageWindow{
					id: "error"
					message: "Something goes wrong with this document."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}
	    }*/
	}
	Operation{
	    opName: "Explain Conflicts (single-document)"
	    nDocs: 1
	    nTemps: 0
	    nOffers: 0
	    console: console
	    listenerCallback: function(result, docs){
	        if(result != null){
	            var isConsistent = result[0] as Boolean;
    	        var hasWarnings = result[1] as Boolean;
    	        var errors = result[2] as java.util.Collection;
    	       // var warnings = result[3] as java.util.List;
    	        docs[0].resetMaps();
    	        for(e in errors){
    	            docs[0].addWarningOrError(e as Collection);
    	        }
    	      //  for(w in warnings){
    	      //      docs[0].addWarning(w as String);
    	      //  }
    	        if(isConsistent){
    	            if(hasWarnings){
    			        var puw = MessageWindow{
    				        id: "info"
    				        //message: "Inconsistencies are highlighted in RED and warnings are highlighted in ORANGE."
    				        message: "Conflicts are highlighted."
    				    }
    				    PopupWindow.addPopupToScene(puw, scene);
    	            }else{
    					var resultWindow = ResultWindow{
    						id: "consistentWithNoWarnings"
    					}
    					PopupWindow.addPopupToScene(resultWindow, scene);
    	            }
    	        }else{
    			    var puw = MessageWindow{
    			        id: "info"
    			       // message: "Inconsistencies are highlighted in RED and warnings are highlighted in ORANGE."
    			       message: "Conflicts are highlighted."
    			    }
    			    PopupWindow.addPopupToScene(puw, scene);
    	        }
    	        
    	        //Adding log
				var resultADA:String = docs[0].getErrorsAsString();
				LogTaskBase{
				    operation: operations[1].opName
				    doc1: [docs[0].docName, docs[0].docContent, docs[0].docDefault] as nativearray of Object //nativearray creates a java array instead a javafx sequence
				    doc2: [docs[1].docName, docs[1].docContent, docs[1].docDefault] as nativearray of Object
				    consoleLog: console.getConsoleText()
				    resultADA: resultADA
				    sessionId: sessionId.toString()
				    browserInfo: browserInfo
				    createTask: function(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo){
				        return new AddLogQuery(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo);
				    }
				}.start();
	        }else{
	            var messageWindow:MessageWindow = MessageWindow{
					id: "error"
					message: "There was an error: It may be due to a syntax error, please check the document syntax."
	            }
	            PopupWindow.addPopupToScene(messageWindow, scene);
	        }
	    }
	    execute: function(listener, docs){
	        var ww = WaitWindow{
                id: "wait"
                message: "Analysing, please wait"
            }
            PopupWindow.addPopupToScene(ww, scene);
            
	        ADATaskBase{
	            listener: listener
	            docs: docs
	            createTask: function(listener, docs){
	                return new ExplainInconsistencies(listener, docs[0].docContent);
	            }
	            onDone: function(){    
	                //Analysed document is opened on a new tab
	                for(tab in editZone.tabs){
				        if(tab.id == docs[0].docName){
				            tab.closeTab();
				            break;
				        }
				    }
			        leftMenu.searchAccordionItemByName(docs[0].docName).openItem();
	            	
	            	PopupWindow.closeWindow("wait", scene);
	            }
	        }.start()
	    }
	    /*execute: function(docs:ADADocument[]){
	        docs[0].resetMaps();
	        console.showConsole();
	        try{
	            var resultWindow:ResultWindow;
	            console.addLine("Checking consistency..........");
    			var isConsistent = ADAService.isConsistent(docs[0].docContent);
    		    if(isConsistent){
    		        console.setResult("CONSISTENT!", Color.GREEN);
    		        console.addLine("Checking warnings..........");
    			    var hasWarnings = ADAService.hasWarnings(docs[0].docContent);
    		        if(hasWarnings){
    		            console.setResult("WARNINGS", Color.RED);
    			        //Llamar a explain warnings
    			        console.addLine("Explaining Warnings..........");
    			        var resultExplanation = ADAService.explainWarnings(docs[0].docContent);//Arrays.asList()??????????
    		            var nWarnings:Integer = 0;
    		            if(sizeof resultExplanation > 0){
    			            for(war in resultExplanation){
        		                docs[0].addWarning(war);
        		                nWarnings++;
        			        }
        			        if(nWarnings == 1){
        			            console.setResult("{nWarnings} Warning", Color.RED);
        			        }else{
        			            console.setResult("{nWarnings} Warnings", Color.RED);
        			        }
        			        var puw = MessageWindow{
            			        id: "info"
            			        message: "Inconsistencies are highlighted in RED and warnings are highlighted in ORANGE."
            			    }
            			    PopupWindow.addPopupToScene(puw, scene);
            			    for(tab in editZone.tabs){
            			        if(tab.id == docs[0].docName){
            			            tab.closeTab();
            			            break;
            			        }
            			    }
        			        leftMenu.searchAccordionItemByName(docs[0].docName).openItem();
    			        }
    			    }else{
						console.setResult("NO WARNINGS", Color.GREEN);
						resultWindow = ResultWindow{
							id: "consistentWithNoWarnings"
						}
						PopupWindow.addPopupToScene(resultWindow, scene);
    			    }
    			}else{
    			    //Llamar a explain inconsistencies y explain warnings
    			    console.setResult("INCONSISTENT!", Color.RED);
    			    console.addLine("Explaining inconsistencies..........");
    			    var errorExplanation = ADAService.explainInconsistencies(docs[0].docContent);
    		        var nErrors:Integer = 0;
    		        for(err in errorExplanation){
    		            docs[0].addError(err);
    		            nErrors++;
    			    }
    			    if(nErrors == 1){
    			        console.setResult("{nErrors} Error", Color.RED);
    			    }else{
    			        console.setResult("{nErrors} Errors", Color.RED);
    			    }
    			    console.addLine("Explaining warnings..........");
    			    var warningExplanation = ADAService.explainWarnings(docs[0].docContent);
    		        var nWarnings:Integer = 0;
    		        for(war in warningExplanation){
    		            docs[0].addWarning(war);
    		            nWarnings++;
    			    }
    			    if(nWarnings == 1){
    			        console.setResult("{nWarnings} Warning", Color.RED);
    			    }else{
    			        console.setResult("{nWarnings} Warnings", Color.RED);
    			    }
    			    var puw = MessageWindow{
    			        id: "info"
    			        message: "Inconsistencies are highlighted in RED and warnings are highlighted in ORANGE."
    			    }
    			    PopupWindow.addPopupToScene(puw, scene);
    			    for(tab in editZone.tabs){
    			        if(tab.id == docs[0].docName){
    			            tab.closeTab();
    			            break;
    			        }
    			    }
    			    leftMenu.searchAccordionItemByName(docs[0].docName).openItem();
    		    }
	        }catch(e:java.net.ConnectException){
				println("serverExceptionOnExplainInconsistencies");
				var messageW = MessageWindow{
					id: "error"
					message: "Unable to connect to server. Please try again later."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}catch(e:org.apache.axis.AxisFault){
				println("AxisFaultException");
				var messageW = MessageWindow{
					id: "error"
					message: "Something goes wrong with this document."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}
		}*/
	}
	Operation{
	    opName: "Check Compliance Conflicts (template-offer)"
	    nDocs: 2
	    nTemps: 1
	    nOffers: 1
	    console: console
	    listenerCallback: function(result, docs){
	        if(result != null){
	            var resultWindow:ResultWindow;
	            var isTempConsistent = result[0] as Boolean;
	            var isOfferConsistent = result[1] as Boolean;
	            var isCompliant = result[2] as Boolean;
	            if(not isTempConsistent or not isOfferConsistent){
	                resultWindow = ResultWindow{
    					id: "inconsistentOnComplianceOp"
       	            }
	            }else{
	                if(isCompliant){
	                    resultWindow = ResultWindow{
      	                    id: "compliant"
      	                }
	                }else{
	                    resultWindow = ResultWindow{
	                        id: "nonCompliant"
	                    }
	                }
	            }
	            PopupWindow.addPopupToScene(resultWindow, scene);
	            
	            //Adding log
    	        var resultADA:String = "{isCompliant}";
    	        LogTaskBase{
    	            operation: operations[2].opName
    	            doc1: [docs[0].docName, docs[0].docContent, docs[0].docDefault] as nativearray of Object //nativearray creates a java array instead a javafx sequence
    	            doc2: [docs[1].docName, docs[1].docContent, docs[1].docDefault] as nativearray of Object
    	            consoleLog: console.getConsoleText()
    	            resultADA: resultADA
    	            sessionId: sessionId.toString()
    	            browserInfo: browserInfo
    	            createTask: function(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo){
    	                return new AddLogQuery(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo);
    	            }
    	        }.start();
	        }else{
	            var messageWindow:MessageWindow = MessageWindow{
					id: "error"
					message: "There was an error: It may be due to a syntax error, please check the document syntax."
	            }
	            PopupWindow.addPopupToScene(messageWindow, scene);
	        }
	    }
	    execute: function(listener, docs){
	        var ww = WaitWindow{
                id: "wait"
                message: "Analysing, please wait"
            }
            PopupWindow.addPopupToScene(ww, scene);
	        
	        ADATaskBase{
	            listener: listener
	            docs: docs
	            createTask: function(listener, docs){
	                return new CheckComplianceTask(listener, docs[0].docContent, docs[1].docContent);
	            }
	            onDone: function(){
	                PopupWindow.closeWindow("wait", scene);
	            }
	        }.start();
	    }
	    /*execute: function(docs:ADADocument[]){
	        docs[0].resetMaps();
	        docs[1].resetMaps();
	        console.showConsole();
	        try{
	            var resultWindow:ResultWindow;
    	        var tempCode = docs[0].docContent;
    	        var offerCode = docs[1].docContent;
    	        console.addLine("Checking template consistency..........");
    	        var isTempConsistent = ADAService.isConsistent(tempCode);
    	        if(isTempConsistent){
    	            console.setResult("CONSISTENT!", Color.GREEN);
    	        }else{
    	            console.setResult("INCONSISTENT!", Color.RED);
    	        }
    	        console.addLine("Checking offer consistency..........");
    	        var isOfferConsistent = ADAService.isConsistent(offerCode);
    	        if(isOfferConsistent){
    	            console.setResult("CONSISTENT!", Color.GREEN);
    	        }else{
    	            console.setResult("INCONSISTENT!", Color.RED);
    	        }
                if(not isTempConsistent or not isOfferConsistent){
    				resultWindow = ResultWindow{
    					id: "inconsistentOnComplianceOp"
       	            }
       	        }else{
       	            console.addLine("Checking compliance..........");
       	            var isCompliant = ADAService.isCompliant(tempCode, offerCode);
    		        if(isCompliant){
    		            console.setResult("COMPLIANT!", Color.GREEN);
      	                resultWindow = ResultWindow{
      	                    id: "compliant"
      	                }
      	            }else{
      	                console.setResult("NON COMPLIANT!", Color.RED);
      	                resultWindow = ResultWindow{
      	                    id: "nonCompliant"
      	                }
      	            }
       	        }
       	        PopupWindow.addPopupToScene(resultWindow, scene);
	        }catch(e:java.net.ConnectException){
				println("serverExceptionOnCheckCompliance");
				var messageW = MessageWindow{
					id: "error"
					message: "Unable to connect to server. Please try again later."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}catch(e:org.apache.axis.AxisFault){
				println("AxisFaultException");
				e.printStackTrace();
				var messageW = MessageWindow{
					id: "error"
					message: "Something goes wrong with this document."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}
	    }*/
	}
	Operation{
	    opName: "Explain Non-Compliance Conflicts (template-offer)"
	    nDocs: 2
	    nTemps: 1
	    nOffers: 1
	    console: console
	    listenerCallback: function(result, docs){
	        if(result != null){
	            var isTempConsistent = result[0] as Boolean;
	            var isOfferConsistent = result[1] as Boolean;
	            var isCompliant = result[2] as Boolean;
	            var tempErrors = result[3] as java.util.Collection;
	            var offerErrors = result[4] as java.util.Collection;
	            docs[0].resetMaps();
	            docs[1].resetMaps();
	            if(not isTempConsistent or not isOfferConsistent){
    				var inconsistentWindow = ResultWindow{
    					id: "inconsistentOnComplianceOp"
    				}
    				PopupWindow.addPopupToScene(inconsistentWindow, scene);
    			}else{
    			    if(isCompliant){
    			        var inconsistentWindow = ResultWindow{
    	                   id: "compliant"
    	                }
    	                PopupWindow.addPopupToScene(inconsistentWindow, scene);
    			    }else{
    			        for(tError in tempErrors){
            	            docs[0].addWarningOrError(tError as Collection);
            	        }
            	        for(oErrors in offerErrors){
            	            docs[1].addWarningOrError(oErrors as Collection);
            	        }
    			        var puw = MessageWindow{
    				        id: "info"
    				        message: "The origin for the non-compliance between documents is highlighted."
    				    }
    				    PopupWindow.addPopupToScene(puw, scene);
    			    }
    			}
    			
    			//Adding log
    	        var resultADA:String = "{docs[0].getErrorsAsString()}\n{docs[1].getErrorsAsString()}";
    	        LogTaskBase{
    	            operation: operations[3].opName
    	            doc1: [docs[0].docName, docs[0].docContent, docs[0].docDefault] as nativearray of Object //nativearray creates a java array instead a javafx sequence
    	            doc2: [docs[1].docName, docs[1].docContent, docs[1].docDefault] as nativearray of Object
    	            consoleLog: console.getConsoleText()
    	            resultADA: resultADA
    	            sessionId: sessionId.toString()
    	            browserInfo: browserInfo
    	            createTask: function(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo){
    	                return new AddLogQuery(operation, doc1, doc2, consoleLog, resultADA, sessionId, browserInfo);
    	            }
    	        }.start();
	        }else{
	            var messageWindow:MessageWindow = MessageWindow{
					id: "error"
					message: "There was an error: It may be due to a syntax error, please check the document syntax."
	            }
	            PopupWindow.addPopupToScene(messageWindow, scene);
	        }
	    }
	    execute: function(listener, docs){
	        var ww = WaitWindow{
                id: "wait"
                message: "Analysing, please wait"
            }
            PopupWindow.addPopupToScene(ww, scene);
	        
	        ADATaskBase{
	            listener: listener
	            docs: docs
	            createTask: function(listener, docs){
	                return new ExplainNonComplianceTask(listener, docs[0].docContent, docs[1].docContent);
	            }
	            onDone: function(){
	                //Analysed documents are opened on a new tab
	                for(tab in editZone.tabs){
    			        if(tab.id == docs[0].docName){
    			            tab.closeTab();
    			        }
    			        if(tab.id == docs[1].docName){
    			            tab.closeTab();
    			        }
    			        if(tab.id == "doubleViewTab"){
    			            tab.closeTab();
    			        }
    			    }
				    //leftMenu.searchAccordionItemByName(docs[0].docName).openItem();
				    //leftMenu.searchAccordionItemByName(docs[1].docName).openItem();
	                /*
	                 * - Hay que crear una pesta�a nueva, con id = "doubleViewTab"
	                 * - Esa pesta�a debe tener dos documentos, template y offer que ser�n
	                 * cargados en el editor correspondiente.
	                 * - Si la pesta�a con id = "doubleViewTab" ya est� abierta,
	                 * simplemente cambiar los documentos que haya por los nuevos
	                 * var newTab: Tab = Tab{
			    	        doc: returnDoc
			    	        parentTabbedMenu: editZone
			    	        index: sizeof editZone.tabs
			    		}
	                 */
	                 var newTab:DoubleViewTab = DoubleViewTab{
	                     template: docs[0]
	                     offer: docs[1]
	                     parentTabbedMenu: editZone
	                     index: sizeof editZone.tabs
	                 }
	                 editZone.addTab(newTab);
	                 
	                 PopupWindow.closeWindow("wait", scene);
	            }
	        }.start();
	    }
	    /*execute: function(docs:ADADocument[]){
	        docs[0].resetMaps();
	        docs[1].resetMaps();
	        console.showConsole();
	        try{
	            var tempCode = docs[0].docContent;
    			var offerCode = docs[1].docContent;
    			console.addLine("Checking template consistency..........");
    			var isTempConsistent = ADAService.isConsistent(tempCode);
    			if(isTempConsistent){
    			    console.setResult("CONSISTENT!", Color.GREEN);
    			}else{
    			    console.setResult("INCONSISTENT!", Color.RED);
    			}
    			console.addLine("Checking offer consistency..........");
    			var isOfferConsistent = ADAService.isConsistent(offerCode);
    			if(isOfferConsistent){
    			    console.setResult("CONSISTENT!", Color.GREEN);
    			}else{
    			    console.setResult("INCONSISTENT!", Color.RED);
    			}
    		    if(not isTempConsistent or not isOfferConsistent){
    				var inconsistentWindow = ResultWindow{
    					id: "inconsistentOnComplianceOp"
    				}
    				PopupWindow.addPopupToScene(inconsistentWindow, scene);
    			}else{
    			    console.addLine("Checking compliant..........");
    			    var compliance = ADAService.isCompliant(tempCode, offerCode);
    		        if(compliance){
    		            console.setResult("COMPLIANT!", Color.GREEN);
    			        var inconsistentWindow = ResultWindow{
    	                   id: "compliant"
    	                }
    	                PopupWindow.addPopupToScene(inconsistentWindow, scene);
    			    }else{
    			        console.setResult("NON COMPLIANT!", Color.RED);
    			        console.addLine("Explaining non compliance..........");
    			        var mapEntry = ADAService.explainNonCompliance(docs[0].docContent, docs[1].docContent);
    		            var nTempErrors:Integer = 0;
    		            var nOfferErrors:Integer = 0;
    		            for(entry in mapEntry){
    					    docs[1].addError(entry.getKey().getName());
    					    nOfferErrors++;
    					    var values = entry.getValue();
    					    for(value in values){
    					        docs[0].addError(value.getName());
    					        nTempErrors++;
    					    }
    					}
    					console.addLine("          Template errors..........");
    					if(nTempErrors == 1){
    					    console.setResult("{nTempErrors} Error", Color.RED);
    					}else{
    					    console.setResult("{nTempErrors} Errors", Color.RED);
    					}
    					console.addLine("          Offer errors..........");
    					if(nOfferErrors == 1){
    					    console.setResult("{nOfferErrors} Error", Color.RED);
    					}else{
    					    console.setResult("{nOfferErrors} Errors", Color.RED);
    					}
    					var puw = MessageWindow{
    				        id: "info"
    				        message: "The origin for the non-compliance between documents is highlighted in RED in each document."
    				    }
    				    PopupWindow.addPopupToScene(puw, scene);
        			    for(tab in editZone.tabs){
        			        if(tab.id == docs[0].docName){
        			            tab.closeTab();
        			        }
        			        if(tab.id == docs[1].docName){
        			            tab.closeTab();
        			        }
        			    }
    				    leftMenu.searchAccordionItemByName(docs[0].docName).openItem();
    				    leftMenu.searchAccordionItemByName(docs[1].docName).openItem();
    			    }
    		    }
	        }catch(e:java.net.ConnectException){
				println("serverExceptionOnExplainNonCompliance");
				var messageW = MessageWindow{
					id: "error"
					message: "Unable to connect to server. Please try again later."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}catch(e:org.apache.axis.AxisFault){
				println("AxisFaultException");
				var messageW = MessageWindow{
					id: "error"
					message: "Something goes wrong with this document."
				}
				PopupWindow.addPopupToScene(messageW, scene);
			}
	    }*/
	}
];

var operationsList: OperationsList = OperationsList{
    id: "opList"
    x: 200, y: 70
    operations: operations
    openedTemps: bind openedTemplates
    openedOffers: bind openedOffers
    tabbedMenu: editZone
};

var reportButton: ReportButton = ReportButton{
    xPos: bind stageW-325, yPos: 0
    scaleX: 0.6, scaleY: 0.6
}

function run(__ARGS__:String[]){

    /*
     * #######################################################################
     * ## El metric para las pruebas es: metrics/metric2575304161886105.xml ##
     * #######################################################################
     */
     
     /**
      * Set sessionId
      */
     var uuidListener:UUIDListener = UUIDListener{
         override function returnUUID(uuid:UUID){
             sessionId = uuid;
         }
     }
     
     var uuidTask:UUIDTaskBase = UUIDTaskBase{
         listener: uuidListener
     }
     
     uuidTask.start();
     
     /*
      * Setting scene
      */
     scene = Scene{
         fill: null
         content: [
         	VBox{
         	    content: [
         	    	buttonsMenu,
         	    	HBox{
         	    	    spacing: 5
         	    	    content: [
         	    	    	leftMenu,
         	    	    	editZone
         	    	    ]
         	    	}
         	    ]
         	}
         	console,
         	reportButton
         ]
     }

	/*
	 * Open default documents
	 */
	try{
	    //############################## PRUEBA ###########################
	    //Para ver si se pueden cargar los documentos de forma reflexiva -> No funciona
	    /*println("Empezamos la prueba en {codebase}");
	    var filesPath:File = new File("{codebase}/docs/");
	    println("Files path: {filesPath.getAbsolutePath()}");
	    var files = filesPath.listFiles();
	    for(f in files){
	        var path = f.getAbsolutePath();
	        println(path);
	    }*/
	    
	    //#################################################################
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig3_Conflict-free_Template.wsag"), "TWEB_Fig3_Conflict-free_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig6_InconsistentTerms_Template.wsag"), "TWEB_Fig6_InconsistentTerms_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig7_DeadTerms_Template.wsag"), "TWEB_Fig7_DeadTerms_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig8_LudicrousTerms_Template.wsag"), "TWEB_Fig8_LudicrousTerms_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig9_PartialDeadTerm_Template.wsag"), "TWEB_Fig9_PartialDeadTerm_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig10_PartialInconsistentTerms_Template.wsag"), "TWEB_Fig10_PartialInconsistentTerms_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig18_WSAG4J_Template.wsag"), "TWEB_Fig18_WSAG4J_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig20_WSAG4J_Offer.wsag"), "TWEB_Fig20_WSAG4J_Offer.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig24_RegionalGovernmental_Template.wsag"), "TWEB_Fig24_RegionalGovernmental_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/TWEB_Fig25_SALMon_Template.wsag"), "TWEB_Fig25_SALMon_Template.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/MasterTemplate.wsag"), "MasterTemplate.wsag", "template") into openedTemplates;
	    insert fileM.openDefaultDocument(new URL("{codebase}docs/ConsistentTemplate.wsag"), "ConsistentTemplate.wsag", "template") into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/Template(term_errors_by_SDT_and_CC_with_themselves).wsag"), "Template(term_errors_by_SDT_and_CC_with_themselves).wsag", "template") into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag"), "Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag", "template") into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/Template(term_errors_by_several_GTs_and_CCs).wsag"), "Template(term_errors_by_several_GTs_and_CCs).wsag", "template") into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/Template(warning_by_SDT_CC).wsag"), "Template(warning_by_SDT_CC).wsag", "template") into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/DeadTerm(GT1-GT1).wsag"), "DeadTerm(GT1-GT1).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/DeadTerm(Xor).wsag"), "DeadTerm(Xor).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/InconsistentGTs(GT1-GT1).wsag"), "InconsistentGTs(GT1-GT1).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/InconsistentGTs(GT1-GT2).wsag"), "InconsistentGTs(GT1-GT2).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/InconsistentGTs(GT1-GT2andGT3-GT3).wsag"), "InconsistentGTs(GT1-GT2andGT3-GT3).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/LudicrousTerm(GT1-GT1).wsag"), "LudicrousTerm(GT1-GT1).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/LudicrousTerm(GT1-SDT).wsag"), "LudicrousTerm(GT1-SDT).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/Warning(QC-CC).wsag"), "Warning(QC-CC).wsag", "template")into openedTemplates;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/Warning(QC-SDT).wsag"), "Warning(QC-SDT).wsag", "template")into openedTemplates;
    	
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag"), "AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag", "offer") into openedOffers;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/AgreementOffer(warning_by_SDT).wsag"), "AgreementOffer(warning_by_SDT).wsag", "offer") into openedOffers;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag"), "ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag", "offer") into openedOffers;
    	insert fileM.openDefaultDocument(new URL("{codebase}docs/ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag"), "ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag", "offer") into openedOffers;
	}catch(e){
		println("serverExceptionOnLoadDefaultDocuments");
		var messageW = MessageWindow{
			id: "error"
			message: "Default documents can�t be loaded. Please try again later or load your own documents."
		}
		PopupWindow.addPopupToScene(messageW, scene);
		e.printStackTrace();
	}
	
	//###################################
	//Solo para meter los documentos por defecto en la BD
	/*var names:String[];
	var contents:String[];
	for(doc in openedTemplates){
	    insert doc.docName into names;
	    insert doc.docContent into contents;
	}
	for(doc in openedOffers){
	    insert doc.docName into names;
	    insert doc.docContent into contents;
	}
	AddDefaultDocsTaskBase{
	    docsNames: names as nativearray of String
	    docsContents: contents as nativearray of String
	    createTask: function(docsNames, docsContents){
	        return new AddDefaultDocuments(docsNames, docsContents);
	    }
	}.start();*/
	//##################################

	stage = Stage {
        title : "ADA Front-End"
        width: bind stageW with inverse
        height: bind stageH with inverse
        style: StageStyle.TRANSPARENT
        resizable: true
        scene: scene
    }
}

class FileManager{
	var fileChooser: javax.swing.JFileChooser;

    function getFileChooser(): javax.swing.JFileChooser{
        if(fileChooser == null){
            fileChooser = javax.swing.JFileChooser {};
        }
        return fileChooser;
    }
    
    function openDefaultDocument(url:URL, docName:String, type:String):ADADocument{
        var returnDoc: ADADocument;
        
        var reader = new BufferedReader(new InputStreamReader(url.openStream()));
        var builder = new StringBuilder();
        var line = reader.readLine();
	    while (line != null) {
	        builder.append(line);
	        builder.append('\n');
	        line = reader.readLine();
	    }
	    reader.close();
	    returnDoc = ADADocument{
	    	docName: docName
	    	docPath: ""
	    	docContent: builder.toString()
	    	docDefault: true
	    }
	    
	    var newItem: AccordionItem = AccordionItem{
		    doc: returnDoc
		    type: type
		    openItem: function(){
		        if(editZone.continueIfUnsavedChanges()){
		            editZone.addTab(Tab{
			            doc: newItem.doc
			            parentTabbedMenu: editZone
			        	index: sizeof editZone.tabs
			        });
		        }
		    }
		    closeItem: function(){
		        var found = false;
	            for(tab in editZone.tabs){
		            if(tab.doc.docName == newItem.doc.docName){
		                found = true;
		                tab.closeTab();
		                newItem.doc.resetMaps();
	    		        if(Sequences.indexOf(editZone.tabs, tab) == -1){
	    		            if(newItem.type == "template"){
	        		            delete newItem.doc from openedTemplates;
	        		            delete newItem from leftMenu.templates;
	        		        }else if(newItem.type == "offer"){
	        		            delete newItem.doc from openedOffers;
	        		            delete newItem from leftMenu.offers;
	        		        }
	    		        }
		                break;
		            }
		        }
		        if(not found){
		            if(newItem.type == "template"){
			            delete newItem.doc from openedTemplates;
			            delete newItem from leftMenu.templates;
			        }else if(newItem.type == "offer"){
			            delete newItem.doc from openedOffers;
			            delete newItem from leftMenu.offers;
			        }
		        }
		    }
		    width: leftMenu.width
		}
	    
	    if(type == "template"){
	        leftMenu.addTemplate(newItem);
	    }else if(type == "offer"){
	        leftMenu.addOffer(newItem);
	    }
        
        return returnDoc;
    }
    
    function openBlankDocument(is:InputStream, is4People:InputStream, type:String):ADADocument{
        var returnDoc: ADADocument;
        //Reading from is InputStream
	    var reader = new BufferedReader(new InputStreamReader(is));
	    var builder = new StringBuilder();
	    var line = reader.readLine();
	    while (line != null) {
	        builder.append(line);
	        builder.append('\n');
	        line = reader.readLine();
	    }
	    reader.close();
	    returnDoc = ADADocument{
	    	docName: if(type == "template")"newTemplate.wsag" else "newOffer.wsag"
	    	docPath: ""
	    	docContent: builder.toString()
	    	docDefault: true
	    }
	    //Reading from is4People InputStream
	    reader = new BufferedReader(new InputStreamReader(is4People));
	    builder = new StringBuilder();
	    line = reader.readLine();
	    while(line != null){
	        builder.append(line);
	        builder.append('\n');
	        line = reader.readLine();
	    }
	    reader.close();
	    
	    var newTab: Tab = Tab{
	        doc: returnDoc
	        parentTabbedMenu: editZone
	        index: sizeof editZone.tabs
	        wsag4pViewContent: builder.toString();
		}
		var newItem: AccordionItem = AccordionItem{
		    doc: returnDoc
		    type: type
		    openItem: function(){
		        if(editZone.continueIfUnsavedChanges()){
		            editZone.addTab(Tab{
			            doc: newItem.doc
			            parentTabbedMenu: editZone
			        	index: sizeof editZone.tabs
			        });
		        }
		    }
		    closeItem: function(){
		        var found = false;
	            for(tab in editZone.tabs){
		            if(tab.doc.docName == newItem.doc.docName){
		                found = true;
		                tab.closeTab();
		                newItem.doc.resetMaps();
	    		        if(Sequences.indexOf(editZone.tabs, tab) == -1){
	    		            if(newItem.type == "template"){
	        		            delete newItem.doc from openedTemplates;
	        		            delete newItem from leftMenu.templates;
	        		        }else if(newItem.type == "offer"){
	        		            delete newItem.doc from openedOffers;
	        		            delete newItem from leftMenu.offers;
	        		        }
	    		        }
		                break;
		            }
		        }
		        if(not found){
		            if(newItem.type == "template"){
			            delete newItem.doc from openedTemplates;
			            delete newItem from leftMenu.templates;
			        }else if(newItem.type == "offer"){
			            delete newItem.doc from openedOffers;
			            delete newItem from leftMenu.offers;
			        }
		        }
		    }
		    width: leftMenu.width
		}
	    
	    if(type == "template"){
	        leftMenu.addTemplate(newItem);
	    }else if(type == "offer"){
	        leftMenu.addOffer(newItem);
	    }
	    editZone.addTab(newTab);

        return returnDoc;
    }
    
    function openFile(f:File, type:String, defaultFile:Boolean):ADADocument{
        var returnDoc: ADADocument;
        var file:File;
        
        if(f == null){
            var fc = getFileChooser();
            var retVal = fc.showOpenDialog(null);
            if(retVal == javax.swing.JFileChooser.APPROVE_OPTION){
                file = fc.getSelectedFile();
            }
        }else{
            file = f;
        }
        if(file == null){
            //user has pressed "cancel" on opened dialog so do nothing
        }else if(isCorrectTypeOnOpen(file, type)){
            var reader = new BufferedReader(new FileReader(file));
            var builder = new StringBuilder();
            var line = reader.readLine();
            while (line != null) {
                builder.append(line);
                builder.append('\n');
                line = reader.readLine();
            }
            reader.close();
            returnDoc = ADADocument{
            	docName: file.getName()
            	docPath: file.getCanonicalPath()
            	docContent: builder.toString()
            	docDefault: false
            }
            try{
                var newTab: Tab = Tab{
        	        doc: returnDoc
        	        parentTabbedMenu: editZone
        	        index: sizeof editZone.tabs
        		}
	    		var newItem: AccordionItem = AccordionItem{
	    		    doc: returnDoc
	    		    type: type
	    		    openItem: function(){
	    		        if(editZone.continueIfUnsavedChanges()){
	    		            editZone.addTab(Tab{
	        		            doc: newItem.doc
	        		            parentTabbedMenu: editZone
	        		        	index: sizeof editZone.tabs
	        		        });
	    		        }
	    		    }
	    		    closeItem: function(){
	    		        var found = false;
			            for(tab in editZone.tabs){
	    		            if(tab.doc.docName == newItem.doc.docName){
	    		                found = true;
	    		                tab.closeTab();
	    		                newItem.doc.resetMaps();
	            		        if(Sequences.indexOf(editZone.tabs, tab) == -1){
	            		            if(newItem.type == "template"){
	                		            delete newItem.doc from openedTemplates;
	                		            delete newItem from leftMenu.templates;
	                		        }else if(newItem.type == "offer"){
	                		            delete newItem.doc from openedOffers;
	                		            delete newItem from leftMenu.offers;
	                		        }
	            		        }
	    		                break;
	    		            }
	    		        }
	    		        if(not found){
	    		            if(newItem.type == "template"){
		    		            delete newItem.doc from openedTemplates;
		    		            delete newItem from leftMenu.templates;
		    		        }else if(newItem.type == "offer"){
		    		            delete newItem.doc from openedOffers;
		    		            delete newItem from leftMenu.offers;
		    		        }
	    		        }
	    		    }
	    		    width: leftMenu.width
	    		}
		        if(type == "template"){
	    	        leftMenu.addTemplate(newItem);
	    	    }else if(type == "offer"){
	    	        leftMenu.addOffer(newItem);
	    	    }else{
	    	    }
	    	    editZone.addTab(newTab);
            }catch(e:BadSyntaxException){
                var messageWindow:MessageWindow = MessageWindow{
					id: "error"
					message: "SyntaxError: Cannot convert from WSAg To WSAg4People. It may be due to a syntax error, please check the document syntax."
	            }
	            PopupWindow.addPopupToScene(messageWindow, scene);
            }
        }else{
            var puw = MessageWindow{
                id: "error"
                message: "Error: Selected document is not an agreement {type}"
            }
            PopupWindow.addPopupToScene(puw, scene);
        }

        return returnDoc;
    }
    
    function saveFile(){
        var tab:Tab = editZone.tabs[editZone.indexActiveTab];
        var type:String;
        var found = false;
        var file:File;
        var oldDoc:ADADocument;
        var savedDoc:ADADocument=ADADocument{}
        for(t in openedTemplates){
            if(t.docName == tab.doc.docName){
                //println("Documento a guardar encontrado en las plantillas");
                type = "template";
                oldDoc = t;
                found = true;
                break;
            }
        }
        if(not found){
            for(o in openedOffers){
                if(o.docName == tab.doc.docName){
                    //println("Documento a guardar encontrado en las ofertas");
                    type = "offer";
                    oldDoc = o;
                    break;
                }
            }
        }
        //################ PRUEBA ###############
        var contentToSave:String;
        if(tab.activeView == "wsag4p"){
            try{
                contentToSave = ADAService.wsag4PeopleToXML(editZone.editor.text);
                //println("Vamos a guardar:\n{contentToSave}");
            }catch(e){
                //println("wsag4peopleToXmlException");
                var err = MessageWindow{
                    id: "error"
                    message: "Unable to convert from WSAg4People to WSAg. Please check the document syntax."
                }
                PopupWindow.addPopupToScene(err, scene);
                e.printStackTrace();
            }
        }else if(tab.activeView == "wsag"){
            contentToSave = editZone.editor.text;
        }
        //############## FIN PRUEBA ##############
        if(tab.doc.docDefault){
			var fc = getFileChooser();
            var retVal = fc.showSaveDialog(null);
            if(retVal == javax.swing.JFileChooser.APPROVE_OPTION){
                file = fc.getSelectedFile();
			}
        }else{
            file = new File(tab.doc.docPath);
        }
        
        if(file != null){ //if (file == null) then do nothing because user has clicked "cancel" on fileChooser
	        if(isCorrectTypeOnSave(/*editZone.editor.text*/contentToSave, type)){
	            var previousTabName = tab.doc.docName;
	            try{
	                //cambiar el nombre a la tab y al item
    	            savedDoc.docName = file.getName();
    	            savedDoc.docPath = file.getCanonicalPath();
    	            savedDoc.docContent = contentToSave;//editZone.editor.text;
    	            savedDoc.docDefault = false;
    	            
    	            tab.doc = savedDoc; //Se genera dentro de Tab.fx el contenido de las vistas
    	            tab.tabName = savedDoc.docName;
    	            //println("tabName: {tab.tabName}");
    	            //guardar la vista wsag
    	            //println("Pasa de tab.doc = savedDoc");
    	            var fileWriter = new FileWriter(file);
    	            var printWriter = new PrintWriter(fileWriter);
    	            printWriter.print(savedDoc.docContent);
    	            printWriter.close();
    	            tab.isSaved = true;//println("tab.isSaved: {tab.isSaved}");
    	            tab.doc.resetMaps();
    	            //println("Llega hasta despu�s de resetMaps");
    	            
    	            if(type == "template"){
		                openedTemplates[Sequences.indexOf(openedTemplates, oldDoc)] = savedDoc;
		                for(t in leftMenu.templates){
		                    if(t.doc.docName == previousTabName){
		                        t.doc = savedDoc;
		                        break;
		                    }
		                }
		            }
		            if(type == "offer"){
		                openedOffers[Sequences.indexOf(openedOffers, oldDoc)] = savedDoc;
		                for(o in leftMenu.offers){
		                    if(o.doc.docName == previousTabName){
		                        o.doc = savedDoc;
		                        break;
		                    }
		                }
		            }
    	            
    	            var messageW = MessageWindow{
    	                id: "saved"
    	                message: "Document saved successfully"
    	            }
    	            PopupWindow.addPopupToScene(messageW, scene);
	            }catch(e:BadSyntaxException){
	                tab.tabName = "* {previousTabName}";
	                var messageWindow:MessageWindow = MessageWindow{
    					id: "error"
    					message: "SyntaxError: Cannot convert from WSAg To WSAg4People. It may be due to a syntax error, please check the document syntax."
    	            }
    	            PopupWindow.addPopupToScene(messageWindow, scene);
	            }
	        }else{
	            var messageW = MessageWindow{
	                id: "error"
	                message: "Unable to save changes. Please check the document syntax.\nA template must starts with '<wsag:Template' and an offer with '<wsag:AgreementOffer'"
	            }
	            PopupWindow.addPopupToScene(messageW, scene);
	            //println("El tipo al guardar no es correcto");
	        }
        }
    }
    
    function saveFilesFromDoubleViewTab(){
        var tab:DoubleViewTab = editZone.tabs[editZone.indexActiveTab] as DoubleViewTab;
        var found = false;
        var fileTemp:File;
        var fileOffer:File;
        var oldTemp:ADADocument = tab.template;
        var oldOffer:ADADocument = tab.offer;
        var savedTemp:ADADocument = ADADocument{}
        var savedOffer:ADADocument = ADADocument{}
        //################ PRUEBA ###############
        var templateContentToSave:String;
        var offerContentToSave:String;
        if(tab.activeView == "wsag4p"){
            try{
                templateContentToSave = ADAService.wsag4PeopleToXML(editZone.templateEditor.text);
                offerContentToSave = ADAService.wsag4PeopleToXML(editZone.offerEditor.text);
            }catch(e){
                println("wsag4peopleToXmlException");
                var err = MessageWindow{
                    id: "error"
                    message: "Unable to convert from WSAg4People to WSAg."
                }
                PopupWindow.addPopupToScene(err, scene);
                //e.printStackTrace();
            }
        }else if(tab.activeView == "wsag"){
            templateContentToSave = editZone.templateEditor.text;
            offerContentToSave = editZone.offerEditor.text;
        }
        //############## FIN PRUEBA ##############
        if(tab.template.docDefault){
			var fc = getFileChooser();
			fc.setDialogTitle("Save template document");
            var retVal = fc.showSaveDialog(null);
            if(retVal == javax.swing.JFileChooser.APPROVE_OPTION){
                fileTemp = fc.getSelectedFile();
			}
        }else{
            fileTemp = new File(tab.template.docPath);
        }
        if(tab.offer.docDefault){
			var fc = getFileChooser();
			fc.setDialogTitle("Save offer document");
            var retVal = fc.showSaveDialog(null);
            if(retVal == javax.swing.JFileChooser.APPROVE_OPTION){
                fileOffer = fc.getSelectedFile();
			}
        }else{
            fileOffer = new File(tab.offer.docPath);
        }
        
        if(fileTemp != null and fileOffer != null){ //if (file == null) then do nothing because user has clicked "cancel" on fileChooser
	        if(isCorrectTypeOnSave(/*editZone.editor.text*/templateContentToSave, "template") and isCorrectTypeOnSave(offerContentToSave, "offer")){
	            //cambiar el nombre a la tab y al item
	            savedTemp.docName = fileTemp.getName();
	            savedTemp.docPath = fileTemp.getCanonicalPath();
	            savedTemp.docContent = templateContentToSave;//editZone.editor.text;
	            savedTemp.docDefault = false;
	            savedOffer.docName = fileOffer.getName();
	            savedOffer.docPath = fileOffer.getCanonicalPath();
	            savedOffer.docContent = offerContentToSave;
	            savedOffer.docDefault = false;
                //Update documents from left menu
                openedTemplates[Sequences.indexOf(openedTemplates, oldTemp)] = savedTemp;
                for(t in leftMenu.templates){
                    if(t.doc.docName == tab.template.docName){
                        t.doc = savedTemp;
                        break;
                    }
                }

                openedOffers[Sequences.indexOf(openedOffers, oldOffer)] = savedOffer;
                for(o in leftMenu.offers){
                    if(o.doc.docName == tab.offer.docName){
                        o.doc = savedOffer;
                        break;
                    }
                }

	            tab.template = savedTemp; //Se genera dentro de Tab.fx el contenido de las vistas
	            tab.offer = savedOffer;
	            //guardar la vista wsag
	            var tempFileWriter = new FileWriter(fileTemp);
	            var tempPrintWriter = new PrintWriter(tempFileWriter);
	            tempPrintWriter.print(savedTemp.docContent);
	            var offerFileWriter = new FileWriter(fileOffer);
	            var offerPrintWriter = new PrintWriter(offerFileWriter);
	            offerPrintWriter.print(savedOffer.docContent);
	            tempPrintWriter.close();
	            offerPrintWriter.close();
	            tab.isSaved = true;
	            tab.template.resetMaps();
	            tab.offer.resetMaps();
	            
	            var messageW = MessageWindow{
	                id: "saved"
	                message: "Documents saved successfully"
	            }
	            PopupWindow.addPopupToScene(messageW, scene);
	        }else{
	            var messageW = MessageWindow{
	                id: "error"
	                message: "Unable to save changes. Please check the document syntax.\nA template must starts with '<wsag:Template' and an offer with '<wsag:AgreementOffer'"
	            }
	            PopupWindow.addPopupToScene(messageW, scene);
	            //println("El tipo al guardar no es correcto");
	        }
        }
    }
    
    /**
     * Function that checks if param file is a WSAg4People document or a WSAg document
     * @param file
     * @return true if file is a WSAg4People or false if file is a WSAg
     */
    function isWSAg4People(file:File):Boolean{
		var result:Boolean;
        var reader = new BufferedReader(new FileReader(file));
        var line = reader.readLine();
        if(trimString(line, 1) == "<"){
            result = false;
        }else{
            result = true;
        }
        return result;
    }
    
    /**
     * @param content String that will be checked
     * @param type Document type, ("template" or "offer")
     */
    function isCorrectTypeOnSave(content:String, type:String):Boolean{
        var isCorrectType = false;
        //Check first line
        if(type == "template"){
            isCorrectType = (trimString(content, 14) == "<wsag:Template");
        }else if(type == "offer"){
            isCorrectType = (trimString(content, 20) == "<wsag:AgreementOffer");
        }
        
        //Check second line if necessary
        if(not isCorrectType){
            if(type == "template"){
                isCorrectType = (trimString(content, content.indexOf("\n")+1, 14) == "<wsag:Template");
            }else if(type == "offer"){
                isCorrectType = (trimString(content, content.indexOf("\n")+1, 20) == "<wsag:AgreementOffer");
            }
        }
        return isCorrectType;
    }
    
    /**
     * @param file Document file that will be checked
     * @param type Document type, ("template" or "offer")
     */
    function isCorrectTypeOnOpen(file:File, type:String):Boolean{
        var isCorrectType = false;
        var reader = new BufferedReader(new FileReader(file));
        var line = reader.readLine();
        //Check first line
        if(type == "template"){
            isCorrectType = (trimString(line, 14) == "<wsag:Template");
        }else if(type == "offer"){
            isCorrectType = (trimString(line, 20) == "<wsag:AgreementOffer");
        }
        //Check second line if necessary
        if(not isCorrectType){
            line = reader.readLine();
            if(type == "template"){
                isCorrectType = (trimString(line, 14) == "<wsag:Template");
            }else if(type == "offer"){
                isCorrectType = (trimString(line, 20) == "<wsag:AgreementOffer");
            }
        }
		reader.close();
        return isCorrectType;
    }
    
    /**
     * Returns a substring of string from initialIndex to initialIndex+length
     */
    function trimString(string:String, initialIndex:Integer, length:Integer):String{
        if(string == null) return "";
    
        if(string.length() > initialIndex + length) {
            return "{string.substring(initialIndex, initialIndex+length).trim()}";
        } else {
            return string;
        }
    }
    
    /**
     * Returns a substring of string from specified length
     */
    function trimString(string:String, length:Integer):String{
        if(string == null) return "";
    
        if(string.length() > length) {
            return "{string.substring(0, length).trim()}";
        } else {
            return string;
        }
    }
    
    //Por ahora no permitiremos subir archivos de m�trica
/*    function addMetric(filePath:String):String{
        var file = new File(filePath);
        var reader = new BufferedReader(new FileReader(file));
        var builder = new StringBuilder();
        var line = reader.readLine();
        while (line != null) {
            builder.append(line);
            builder.append('\n');
            line = reader.readLine();
        }
        reader.close();
        var metric = builder.toString();
        return ADAService.addMetric(metric);
    }*/
    
    function getMetric(){
        try{
            var metricContent = ADAService.getMetric("metrics/metric2575304161886105.xml");
            var tab = Tab{
                id: "metricXML"
                doc: ADADocument{
                    docName: "metricXML"
        	    	docPath: ""
        	    	docContent: metricContent
        	    	docDefault: true
                }
                parentTabbedMenu: editZone
                index: sizeof editZone.tabs
            }
            var message = MessageWindow{
                id: "info"
                message: "This ad-hoc metric file depicts the supported metrics for agreement documents variables. Currently, the allowed metric is fixed and cannot be changed. We will improve it, as soon as possible."
            }
            PopupWindow.addPopupToScene(message, scene);
            editZone.addTab(tab);
        }catch(e:java.net.ConnectException){
			println("serverExceptionGetMetric");
			var messageW = MessageWindow{
				id: "error"
				message: "Unable to connect to server. Please try again later."
			}
			PopupWindow.addPopupToScene(messageW, scene);
		}catch(e:org.apache.axis.AxisFault){
			println("AxisFaultException");
			e.printStackTrace();
			var messageW = MessageWindow{
				id: "error"
				message: "Connection problem, please try again later."
			}
			PopupWindow.addPopupToScene(messageW, scene);
		}
    }
    
    function addMetric(){
        //Abrir ventana para que el usuario seleccione el documento desde su PC
        var file = null;
        var fc = getFileChooser();
        var cancelPressed = false;
        fc.addChoosableFileFilter(new XmlFilter());
        var retVal = fc.showDialog(null, "Upload");
        if(retVal == javax.swing.JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
		}
        if(file != null){
            //El fichero existe
            var name = file.getName();
            if(not name.endsWith(".xml")){
                name = "{name}.xml";
            }
            try{
                //Pasar el documento a String y luego a byte[]
                var reader = new BufferedReader(new FileReader(file));
                var builder = new StringBuilder();
                var line = reader.readLine();
                while (line != null) {
                    builder.append(line);
                    builder.append('\n');
                    line = reader.readLine();
                }
                reader.close();
                
                var metricPath = ADAService.addMetric(builder.toString(), name);
                if(metricPath.endsWith("already exists")){//Error, el documento ya existe
                    var messageW = MessageWindow{
                        id: "error"
                        message: "{name} already exists. Please, select a diferent name and try again."
                    }
                    PopupWindow.addPopupToScene(messageW, scene);
                }else{
                    var messageW = MessageWindow{
                        id: "info"
                        message: "Your custom metric file has been successfully uploaded. Remember to edit your documents with the following: {metricPath}:-put the metric name here-"
                    }
                    PopupWindow.addPopupToScene(messageW, scene);
                }
            }catch(e:java.rmi.RemoteException){
                var messageW = MessageWindow{
                    id: "error"
                    message: "Connection problem, please try again later."
                }
                PopupWindow.addPopupToScene(messageW, scene);
            }catch(e:java.io.FileNotFoundException){
                var messageW = MessageWindow{
                    id: "error"
                    message: "Selected file doesn�t exist. Please be sure to select an existing file in your local machine"
                }
                PopupWindow.addPopupToScene(messageW, scene);
            }
        }
    }
}
class XmlFilter extends javax.swing.filechooser.FileFilter{
    
    override function accept(file:File):Boolean{
        if(file.isDirectory()){
            return true;
        }
        var fileName = file.getName();
        return fileName.endsWith(".xml");
    }
    
    override function getDescription():String{
        return "*.xml";
    }
}