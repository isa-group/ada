package frontend;

import javax.swing.JTree;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class TreeLoader {

	/** Metodo que carga un JTree con los datos de un XML cargado en un JEditorPane */
	public static JTree cargaJTree(String xmlContent){
		JTree resultado = new JTree();
		InputStream xmlIS = new ByteArrayInputStream(xmlContent.getBytes());

	    org.w3c.dom.Document agreement;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			agreement = builder.parse(xmlIS);
			
		    Element raiz = agreement.getDocumentElement();
		    raiz.getChildNodes().getLength();
	
		    DefaultMutableTreeNode nodoHijo = new DefaultMutableTreeNode(raiz.getNodeName());
			
			if(raiz.hasChildNodes()){
				org.w3c.dom.Node nodo = raiz.getFirstChild();
				while(nodo != null){
					loadChild(nodoHijo,nodo);
					nodo = nodo.getNextSibling();
				}
			}
			resultado = null;
			resultado = new JTree(nodoHijo);
		} catch (Exception e) {
			System.err.println("Error while creating the tree view for the agreement document");
		}
		return resultado;
	}
	
	private static void loadChild(DefaultMutableTreeNode nodoPadre, org.w3c.dom.Node nodo){
		boolean almacenar = true;
		
		DefaultMutableTreeNode nodoHijo;
		if(nodo.getNodeType() == Node.TEXT_NODE){
			nodoHijo = new DefaultMutableTreeNode(nodo.getTextContent());
			if (nodo.getTextContent().trim().equals("")) almacenar = false;			
		}else
			nodoHijo = new DefaultMutableTreeNode(nodo.getNodeName());
		
		NamedNodeMap atributos = nodo.getAttributes();
		if(atributos!=null){
			for(int i = 0; i < atributos.getLength();i++){
				org.w3c.dom.Node na = (org.w3c.dom.Node)atributos.item(i);		
				String s = na.getNodeName()+": "+na.getNodeValue();
				DefaultMutableTreeNode atri = new DefaultMutableTreeNode(s);
				nodoHijo.add(atri);
			}
		}
		
		if(nodo.hasChildNodes()){
			org.w3c.dom.Node nodoAux = nodo.getFirstChild();
			while(nodoAux != null){
				loadChild(nodoHijo,nodoAux);
				nodoAux = nodoAux.getNextSibling();
			}
		}
		if(almacenar)
			nodoPadre.add(nodoHijo);
	}
}
