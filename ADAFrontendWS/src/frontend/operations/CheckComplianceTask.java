package frontend.operations;

import es.us.isa.ada.exceptions.BadSyntaxException;



public class CheckComplianceTask extends ADATask{
	
	private String template;
	
	private String offer;

	public CheckComplianceTask(FXListener listener, String template, String offer) {
		super(listener);
		this.template = template;
		this.offer = offer;
	}

	@Override
	public void run() throws Exception {
		Object[] result = new Object[3];
		try{
			boolean isTempConsistent;
			boolean isOfferConsistent;
			boolean isCompliant = false;
			
			//Checking Template Conflicts
			addConsoleLine("Checking template conflicts"+consoleLineExtendChars);
			
			//Checking Template Inconsistent Terms
			addConsoleLine(consoleLineFirstChars+"Checking template inconsistent terms"+consoleLineExtendChars);
			isTempConsistent = servicePort.checkDocumentConsistency(template.getBytes());
	        if(isTempConsistent){
	            setResultConsoleLine("Inconsistent terms-free", correctMessageColor);
	        }else{
	            setResultConsoleLine("Inconsistent terms", wrongMessageColor);
	        }
	        
	        //Checking Offer Conflicts
	        addConsoleLine("Checking offer conflicts"+consoleLineExtendChars);
	        
	        //Checking Offer Inconsistent Terms
	        addConsoleLine(consoleLineFirstChars+"Checking offer inconsistent terms"+consoleLineExtendChars);
	        isOfferConsistent = servicePort.checkDocumentConsistency(offer.getBytes());
	        if(isOfferConsistent){
	            setResultConsoleLine("Inconsistent terms-free", correctMessageColor);
	        }else{
	            setResultConsoleLine("Inconsistent terms", wrongMessageColor);
	        }
	        
	        if(isTempConsistent && isOfferConsistent){
	        	//Checking Compliance Conflicts
	        	addConsoleLine("Checking compliance conflicts (This may take several minutes)"+consoleLineExtendChars);
	        	isCompliant = servicePort.isCompliant(template.getBytes(), offer.getBytes());
		        if(isCompliant){
		            setResultConsoleLine("Conflicts-free", correctMessageColor);
  	            }else{
  	                setResultConsoleLine("Conflicts", wrongMessageColor);
  	            }
	        }
	        result[0] = isTempConsistent;
	        result[1] = isOfferConsistent;
	        result[2] = isCompliant;
		}catch(BadSyntaxException e){
			addConsoleLine("There was an error: It may be due to a syntax error, please check the document syntax.");
			result = null;
		}
		returnResult(result);
	}
}
