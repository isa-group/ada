package frontend.operations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.exceptions.BadSyntaxException;
import es.us.isa.ada.service.AgreementElement2ArrayOfAgreementElementMapEntry;
import es.us.isa.ada.service.Term2ArrayOfAgreementElementMapEntry;
import es.us.isa.ada.wsag10.Term;

public class ExplainInconsistencies extends ADATask{
	
	private String doc;

	public ExplainInconsistencies(FXListener listener, String doc) {
		super(listener);
		this.doc = doc;
	}

	@Override
	public void run() throws Exception {
//		Object[] result = new Object[4];
		Object[] result = new Object[3];
		//1. isConsistent
		//2. hasWarnings
		//3. inconsistencias si las hay, warnings si no hay inconsistencias
		
		//usar estas mismas listas para ir guardando
		//los elementos que van apareciendo, y no colorearlos
		//de nuevo
		List<String> warningsNames = new ArrayList<String>();
		List<String> errorsNames = new ArrayList<String>();
		Integer conflictsShowedInConsole = 1;
		Integer deadTermsShowedInConsole = 1;
		Integer ludicrousTermsShowedInConsole = 1;
		
		Collection<Collection<String>> explaining = new LinkedList<Collection<String>>();
		try{
			boolean isConsistent;
			boolean hasWarnings = false;
			
			//Checking Conflicts
			addConsoleLine("Checking conflicts"+consoleLineExtendChars);
			
			//Checking Inconsistent Terms
			addConsoleLine(consoleLineFirstChars+"Checking inconsistent terms"+consoleLineExtendChars);
			isConsistent = servicePort.checkDocumentConsistency(doc.getBytes());
			if(isConsistent){
				setResultConsoleLine("Inconsistent terms-free", correctMessageColor);
				
				//Checking Dead Terms
				addConsoleLine(consoleLineFirstChars+"Checking dead terms"+consoleLineExtendChars);
				Term[] deadTerms = servicePort.getDeadTerms(doc.getBytes());

				if(deadTerms.length > 0){
					setResultConsoleLine("Dead terms", wrongMessageColor);
					
					//Explaining Dead Terms
					addConsoleLine(consoleLineFirstChars+"Explaining dead terms"+consoleLineExtendChars);
					Term2ArrayOfAgreementElementMapEntry[] deadTermsExplanation = servicePort.explainDeadTerms(doc.getBytes(), deadTerms);
					for(Term2ArrayOfAgreementElementMapEntry e: deadTermsExplanation){
						Collection<String> set = new LinkedList<String>();
						AgreementElement[] termValues = e.getValue();
						String warn = e.getKey().getName();
						addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"Dead term "+deadTermsShowedInConsole+": "+warn);
						deadTermsShowedInConsole++;
						if(!warningsNames.contains(warn)){
							//metemos el warning
							set.add(warn);
							warningsNames.add(warn);
						}
						for (AgreementElement cause:termValues){
							//causas del warning
							addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"  - Cause: "+cause.getName());
							if (!warningsNames.contains(cause)){
								//y metemos sus causas
								set.add(cause.getName());
								warningsNames.add(cause.getName());
							}
						}
						if (!set.isEmpty()){
							explaining.add(set);
						}
					}
//					Si mostramos los conflictos en la consola esto no hace falta
//					if(deadTerms.length == 1){
//						setResultConsoleLine("1 dead term", wrongMessageColor);
//					}else{
//						setResultConsoleLine(deadTerms.length+" dead terms", wrongMessageColor);
//					}
				}else{
					setResultConsoleLine("Dead terms-free", correctMessageColor);
				}
				
				//Checking Ludicrous Terms
				addConsoleLine(consoleLineFirstChars+"Checking ludicrous terms"+consoleLineExtendChars);
				Term[] ludicrousTerms = servicePort.getLudicrousTerms(doc.getBytes());
				
				if(ludicrousTerms.length > 0){
					setResultConsoleLine("Ludicrous terms", wrongMessageColor);
					
					//Explaining Ludicrous Terms
					addConsoleLine(consoleLineFirstChars+"Explaining ludicrous terms"+consoleLineExtendChars);
					Term2ArrayOfAgreementElementMapEntry[] ludicrousTermsExplanation = servicePort.explainLudicrousTerms(doc.getBytes(), ludicrousTerms);
					for(Term2ArrayOfAgreementElementMapEntry e: ludicrousTermsExplanation){
						Collection<String> set = new LinkedList<String>();
						AgreementElement[] termValues = e.getValue();
						
						String warn = e.getKey().getName();
						addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"Ludicrous term "+ludicrousTermsShowedInConsole+": "+warn);
						ludicrousTermsShowedInConsole++;
						if(!warningsNames.contains(warn)){
							//metemos el warning
							set.add(warn);
							warningsNames.add(warn);
						}
						
						for (AgreementElement cause:termValues){
							addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"  - Cause: "+cause.getName());
							//causas del warning
							if (!warningsNames.contains(cause)){
								//y metemos sus causas
								set.add(cause.getName());
								warningsNames.add(cause.getName());
							}
						}
						
						if (!set.isEmpty()){
							explaining.add(set);
						}
					}
//					if(ludicrousTerms.length == 1){
//						setResultConsoleLine("1 ludicrous term", wrongMessageColor);
//					}else{
//						setResultConsoleLine(ludicrousTerms.length+" ludicrous terms", wrongMessageColor);
//					}
				}else{
					setResultConsoleLine("Ludicrous terms-free", correctMessageColor);
				}
				hasWarnings = !(deadTerms.length == 0 && ludicrousTerms.length == 0);
			}else{
				setResultConsoleLine("Inconsistent terms", wrongMessageColor);
				
				//Explaining inconsistent terms
				addConsoleLine(consoleLineFirstChars+"Explaining inconsistent terms (This may take several minutes)"+consoleLineExtendChars);
				AgreementElement2ArrayOfAgreementElementMapEntry[] errorsExplanation = servicePort.explainInconsistencies(doc.getBytes());
				for(AgreementElement2ArrayOfAgreementElementMapEntry e: errorsExplanation){

					Collection<String> set = new LinkedList<String>();
					
					String keyName = e.getKey().getName();
					addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"Conflict "+conflictsShowedInConsole+": "+keyName);
					conflictsShowedInConsole++;
					if (!errorsNames.contains(keyName)){
						set.add(keyName);
						errorsNames.add(keyName);
					}
					
					AgreementElement[] termValues = e.getValue();
					for(AgreementElement ae: termValues){
						addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"  - Cause: "+ae.getName());
						if(!errorsNames.contains(ae.getName())){
							set.add(ae.getName());
							errorsNames.add(ae.getName());
						}
					}
					
					if (!set.isEmpty()){
						explaining.add(set);
					}
				}
//				if(explaining.size() == 1){
//					setResultConsoleLine("1 inconsistent term", wrongMessageColor);
//				}else{
//					setResultConsoleLine(explaining.size()+" inconsistent terms", wrongMessageColor);
//				}
				//Si hay inconsistencias NO ANALIZAMOS LOS WARNINGS
				
				
//				addConsoleLine("Checking warnings..........");
//				hasWarnings = !(deadTerms.length == 0 && ludicrousTerms.length == 0);
//				if(hasWarnings){
//					setResultConsoleLine("WARNINGS", wrongMessageColor);
//					addConsoleLine("Explaining warnings..........");
//					Term2ArrayOfAgreementElementMapEntry[] deadTermsExplanation = servicePort.explainDeadTerms(doc.getBytes(), deadTerms);
//					for(Term2ArrayOfAgreementElementMapEntry e: deadTermsExplanation){
//						AgreementElement[] termValues = e.getValue();
//						//#######################################
//						//System.out.println("DeadTerm key name: "+e.getKey().getName());
//						//#######################################
//						String warn = e.getKey().getName();
//						if(!warningsNames.contains(warn)){
//							warningsNames.add(warn);
//						}
////						for(AgreementElement ae: termValues){
////							//#######################################
////							//System.out.println("  - DeadTerm value name:"+ae.getName());
////							//#######################################
////							if(!warningsNames.contains(ae.getName())){
////								warningsNames.add(ae.getName());
////							}
////						}
//					}
//					Term2ArrayOfAgreementElementMapEntry[] ludicrousTermsExplanation = servicePort.explainLudicrousTerms(doc.getBytes(), ludicrousTerms);
//					for(Term2ArrayOfAgreementElementMapEntry e: ludicrousTermsExplanation){
//						AgreementElement[] termValues = e.getValue();
//						//#######################################
//						//System.out.println("LudicrousTerm key name: "+e.getKey().getName());
//						//#######################################
//						String warn = e.getKey().getName();
//						if(!warningsNames.contains(warn)){
//							warningsNames.add(warn);
//						}
////						for(AgreementElement ae: termValues){
////							//#######################################
////							//System.out.println("  - LudicrousTerm value name: "+ae.getName());
////							//#######################################
////							if(!warningsNames.contains(ae.getName())){
////								warningsNames.add(ae.getName());
////							}
////						}
//					}
//					if(warningsNames.size() == 1){
//						setResultConsoleLine("1 Warning", wrongMessageColor);
//					}else{
//						setResultConsoleLine(warningsNames.size()+" Warnings", wrongMessageColor);
//					}
//				}else{
//					setResultConsoleLine("NO WARNINGS", correctMessageColor);
//				}
			}
			result[0] = isConsistent;
			result[1] = hasWarnings;
			result[2] = explaining;
//			result[2] = errorsNames;
//			result[3] = warningsNames;
		}catch(BadSyntaxException e){
			addConsoleLine("There was an error: It may be due to a syntax error, please check the document syntax.");
			result = null;
		}
		returnResult(result);
	}
}
