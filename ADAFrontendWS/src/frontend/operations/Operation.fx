package frontend.operations;
import frontend.Main;
import frontend.ADADocument;
import frontend.Console;
import javafx.async.JavaTaskBase;
import javafx.async.RunnableFuture;
import javafx.scene.Cursor;

/**
 * @author AJurado
 */
 
public class Operation extends FXListener{
    
    /**
     * This operation name
     */
    public var opName:String;
    
    /**
     * Number of docs. If ndocs == 1, this document can be a template or an offer
     */
    public var nDocs:Integer;
    
    /**
     * Number of templates
     */
    public var nTemps:Integer;
    
    /**
     * Number of offers
     */
    public var nOffers:Integer;
    
    /**
     * Documents that will be analysed by this operation
     */
    public var docs:ADADocument[];
    
    /**
     * Console
     */
    public var console:Console;

    /**
     * This function must be defined when creating this Operation
     */
    public-init var listenerCallback:function(data:Object[], docs:ADADocument[]);
    
    /**
     * Callback function from FXListener
     */
    override function callback(data):Void{
        listenerCallback(data, docs);
    }
    
    /**
     * To add new ConsoleLine from FXListener
     */
    override function addLineToConsole(msg):Void{
        console.addLine(msg);
    }
    
    /**
     * To set the result to the last added ConsoleLine from FXListener
     */
    override function setResultToConsoleLine(msg, color):Void{
        console.setResult(msg, color);
    }
    
    /**
     * Will create a new JavaBaseTask and start it
     */
    public-init var execute:function(listener:FXListener, docs:ADADocument[]);

    /**
     * This method code must be defined when instantiating an operation
     * @param docs is an ADADocument Sequence where templates must be added before offers
     */
    //public var execute:function(docs:ADADocument[]);
}