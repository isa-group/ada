package frontend.operations;

import java.rmi.RemoteException;

import es.us.isa.ada.exceptions.BadSyntaxException;

public class CheckConsistencyTask extends ADATask{
	
	private String doc;
	
	public CheckConsistencyTask(FXListener listener, String doc){
		super(listener);
		this.doc = doc;
	}

	@Override
	public void run() throws Exception {
		Object[] result = new Object[2];
		try{
			boolean isConsistent;
			boolean hasWarnings = false;
			
			//Checking Conflicts
			addConsoleLine("Checking conflicts"+consoleLineExtendChars);
			
			//Checking Inconsistent Terms
			addConsoleLine(consoleLineFirstChars+"Checking inconsistent terms"+consoleLineExtendChars);
			isConsistent = servicePort.checkDocumentConsistency(doc.getBytes());
			if(isConsistent){
				setResultConsoleLine("Inconsistent terms-free", correctMessageColor);
//				
				//Checking Dead Terms
				addConsoleLine(consoleLineFirstChars+"Checking dead terms"+consoleLineExtendChars);
				int deadTerms = servicePort.getDeadTerms(doc.getBytes()).length;
				if(deadTerms==0){
					setResultConsoleLine("Dead terms-free", correctMessageColor);
				}else{
					setResultConsoleLine("Dead terms", wrongMessageColor);
				}
				
				//Checking Ludicrous Terms
				addConsoleLine(consoleLineFirstChars+"Checking ludicrous terms"+consoleLineExtendChars);
				int ludicrousTerms = servicePort.getLudicrousTerms(doc.getBytes()).length;
				if(ludicrousTerms==0){
					setResultConsoleLine("Ludicrous terms-free", correctMessageColor);
				}else{
					setResultConsoleLine("Ludicrous terms", wrongMessageColor);
				}
//				hasWarnings = !(deadTerms == 0 && ludicrousTerms == 0);
//				if(hasWarnings){
//					setResultConsoleLine("WARNINGS", wrongMessageColor);
//				}else{
//					setResultConsoleLine("NO WARNINGS", correctMessageColor);
//				}
				hasWarnings = !(deadTerms == 0 && ludicrousTerms == 0); 
			}else{
				setResultConsoleLine("Inconsistent terms", wrongMessageColor);
			}
			result[0] = isConsistent;
			result[1] = hasWarnings;
		}catch(BadSyntaxException e){
			addConsoleLine("There was an error: It may be due to a syntax error, please check the document syntax.");
			result = null;
		}
		returnResult(result);
	}
}
