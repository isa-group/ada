package frontend;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.layout.Stack;
import javafx.scene.Scene;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;
import javafx.animation.Interpolator;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.FontPosture;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.effect.DropShadow;

/**
 * @author AJurado
 */
 
public class Tooltip extends CustomNode{
    
    /**
     * String text
     */
    public var text:String on replace{
        tTip.content = text;
    }
        
    /*
     * Text showed
     */
    var tTip = Text {
		font: Font.font("verdana", FontWeight.HEAVY, FontPosture.REGULAR, 10)
		content: text
		textAlignment: TextAlignment.CENTER
    }
    
    /**
     * Tooltip background
     */
    var bg = Rectangle{
	    width: bind tTip.boundsInLocal.width+10, height: bind tTip.boundsInLocal.height+5
	    fill: Color.LIGHTYELLOW
	    effect: DropShadow{
	        radius: 10
	        width: 10, height: 10
	        offsetX: 1, offsetY: 1
	    }
	    cache: true
    }
    
    /**
     * X position where tooltip will be showed
     */
    var posX:Number;
    
    /**
     * Y position where tooltip will be showed
     */
    var posY:Number;
    
    /**
     * Opacity
     */
    var ttOpacity = 0.0;
    
    /**
     * True if this tooltip has been inserted into the scene
     */
    var isInScene:Boolean = false;
    
    /**
     * Time to show this tooltip 
     */
    var waitTime = Timeline{
        keyFrames: [
        	KeyFrame{
        	    time: 1s
        	    canSkip: true
        	    action: function(){
					insert this into Main.scene.content;
        	        isInScene = true;
        	        showFade.playFromStart();
        	    }
        	}
        ]
    }
    
    /**
     * Animation to show this tooltip
     */
    var showFade = Timeline{
        keyFrames: [
            KeyFrame{
                time: 1s
                canSkip: true
                values: [ttOpacity=>1 tween Interpolator.EASEBOTH]
            }
        ]
    }
    
    /**
     * Initialize tooltip animation
     */
    public function show(x:Number, y:Number){
        //Comprobar si el tama�o mas la posici�n es mayor que el tama�o de la scene
        //En ese caso se desplaza para que aparezca mas a la izquierda
        if(tTip.boundsInLocal.width + x > Main.scene.width){
            posX = Main.scene.width-tTip.boundsInLocal.width-20;
        }else{
            posX = x;
        }
        posY = y;
        ttOpacity = 0.0;
        waitTime.playFromStart();
    }
    
    /**
     * Stops animation and hide tooltip
     */
    public function hide(){
        if(isInScene){
			delete this from Main.scene.content;
        }
        waitTime.stop();
        showFade.stop();
        ttOpacity = 0.0;
    }
    
    /**
     * This tooltip. Used to be inserted into the scene 
     */
    var tooltip:Node = Stack{
        layoutInfo: LayoutInfo{
            managed: false
        }
        layoutX: bind posX, layoutY: bind posY
        opacity: bind ttOpacity
        content: [
        	bg,
        	tTip
        ]
    };

    public override function create(): Node {
        return tooltip;
    }
}