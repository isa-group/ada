package frontend.popupWindows;
import frontend.PopupWindow;
import frontend.DefaultBackground;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.FontPosture;
import javafx.scene.input.MouseEvent;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Lighting;
import javafx.scene.effect.light.DistantLight;
import javafx.scene.control.ProgressIndicator;


/**
 * @author AJurado
 */
public class WaitWindow extends CustomNode {
    
    public var message:String;
    
    override var id = "wait";
    
    public-init var indicator:Boolean = true;
    
    var indicatorCorrector:Number = if(indicator) 0 else 20;
    
    var text:Text = Text{
        content: message
        font: Font.font("Verdana", FontWeight.BOLD, 22)
        fill: Color.DIMGREY
        layoutX: 37-indicatorCorrector, layoutY: 39
    }
    
    var background:DefaultBackground = DefaultBackground{
        arcWidth: 15, arcHeight: 15
        width: text.boundsInLocal.width+45, height: text.boundsInLocal.height+35
        stroke: Color.GREY;
        strokeWidth: 2
        effect: DropShadow{
            offsetX: 3, offsetY: 3
            color: Color.GREY
        }
    }
    
    var progress:ProgressIndicator = ProgressIndicator{
        layoutX: 5, layoutY: 20
        progress: -1
        style: "accent: gray"
    }

    public override function create(): Node {
        return Group {
            layoutX: bind scene.width/2-background.width/2, layoutY: bind scene.height/2-background.height/2-50
            cache: true
            content: [
            	background,
            	if(indicator)progress else null,
            	text
            ]
        };
    }
}
