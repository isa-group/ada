package frontend.popupWindows;
import frontend.PopupWindow;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.LayoutInfo;

/**
 * @author AJurado
 */

public class MessageWindow extends CustomNode {
    
    /**
     * String message to show on window
     */
    public var message:String;
    
    override var id on replace{
        if(id == "error"){
            icon = Image{
            	url: "{__DIR__}icons/error.png"
            }
        }else if(id == "info"){
            icon = Image{
            	url: "{__DIR__}icons/info.png"
            }
        }else{
            icon = Image{
            	url: "{__DIR__}icons/error.png"
            }
        }
    }
    
    /**
     * Image showed on imageView
     */
    var icon:Image;
    
    /**
     * ImageView to show icon
     */
    var imageView = ImageView{
        image: icon
        cache: true
    }
    
    /**
     * Text that shows the message
     */
    var text:Text = Text{
        layoutX: 75, layoutY: 25
        wrappingWidth: 400
        font: Font{
            size: 13
        }
        content: message
    }
    
    /**
     * Content that groups all nodes showed in this window
     */
    var content:Node;
    
    /**
     * Shows a background and a close button
     */
    var window:PopupWindow;
    
    public override function create(): Node {
        return Group {
            content: [
            	window = PopupWindow{
            	    id: id //Important, this id makes posible to close the PopupWindow
            	    width: bind content.boundsInLocal.width+50, height: bind content.boundsInLocal.height+50
            	}
            	content = Group{
            	    layoutX: bind window.boundsInLocal.minX+25, layoutY: bind window.boundsInLocal.minY+40
            	    content: [
            	    	imageView,
            	    	text
            	    ]
            	}
            ]
        };
    }
}
