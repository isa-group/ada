package frontend;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.AppletStageExtension;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.CustomNode;
import javafx.scene.text.Text;
import javafx.scene.effect.Effect;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import java.lang.Exception;


/**
 * @author AJurado
 */
Stage {
    title : "MyApp"
    scene: Scene {
        width: 200
        height: 200
        content: [
        	Button{
        	    action: function(){
        	        try{
            	        ADAService.xmlToWSAg4People("");
        	        }catch(e:Exception){
        	            e.printStackTrace();
        	        }
        	    }
        	}
        ]
    }
}