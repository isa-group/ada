package frontend;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.paint.Color;

/**
 * @author AJurado
 */
public class DefaultBackground extends CustomNode {
    
    var bg: Rectangle;
    
    /**
     * X position, needed outside
     */
    public var x: Number;
    
    /**
     * Y position, needed outside
     */
    public var y: Number;
    
    public var arcWidth:Integer;
    
    public var arcHeight:Integer;
    
    public var width: Number;
    public var height: Number;
    
    public var stroke:Color;
    
    public var strokeWidth:Number;

    public override function create(): Node {
        return Group {
            cache: true
            content: [
            	bg = Rectangle{
            	    width: bind width, height: bind height
            	    arcWidth: arcWidth, arcHeight: arcHeight
            	    stroke: stroke
            	    strokeWidth: strokeWidth
            	    fill: LinearGradient{
            	        startX: 0, startY: 0
            	        endX: 0, endY: 1.0
						proportional: true
            	        stops: [ 	
            	        	Stop{ offset: 0.0, color: Color.LAVENDER},
            	        	Stop{ offset: 1.0, color: Color.DARKGREY}
            	        ]
            	    }
            	}
            ]
        };
    }
}