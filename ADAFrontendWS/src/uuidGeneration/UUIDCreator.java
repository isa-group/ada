package uuidGeneration;

import java.util.UUID;

import com.sun.javafx.functions.Function0;
import javafx.async.RunnableFuture;

public class UUIDCreator implements RunnableFuture{
	
	/**
	 * Listener to communicate with javafx code
	 */
	private UUIDListener listener;
	
	/**
	 * Generated uuid
	 */
	private UUID uuid;
	
	public UUIDCreator(UUIDListener listener){
		this.listener = listener;
	}
	
	/**
	 * returns generated UUID
	 */
	protected void returnUUID(){
		javafx.lang.FX.deferAction(new Function0<Void>() {
//			@Override
			public Void invoke() {
				listener.returnUUID(uuid);
				return null;
			}
		});
	}

	public void run() throws Exception {
		//UUID creation
		this.uuid = UUID.randomUUID();
		
		returnUUID();
	}
}
