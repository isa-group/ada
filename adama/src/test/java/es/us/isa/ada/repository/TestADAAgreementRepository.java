/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.repository;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import es.us.isa.ada.users.ADASessionManager;
import es.us.isa.ada.users.SessionManager;
import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

public class TestADAAgreementRepository {
	
	private ADAAgreementRepository repo;
	
	private static final int PRIVATE_DOC_ID_TO_UPDATE = 19;
	
	private static String session;
	
	@Before
	public void setUp(){
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		repo = new ADAAgreementRepository(db);
		SessionManager sm = new ADASessionManager(db);
		session = sm.login("Antonio", "ajurado").toString();
	}
	
	@Test
	public void addDeletePrivate(){
		System.out.println("Executando addDeletePrivate()");
		int insertedDocId = 0;
		boolean deleted = false;
		try {
			insertedDocId = repo.storeAgreement("NewDocument.wsag", "<xml></xml>", session);
			if(insertedDocId != 0){
				deleted = repo.deleteAgreement(insertedDocId, session);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("El documento insertado tiene como id: "+insertedDocId);
		assertTrue(insertedDocId != 0);
		assertTrue(deleted);
	}
	
	@Test
	public void addPrivateExistingName(){
		System.out.println("Executando addPrivateExistingName()");
		int insertedDocId = 0;
		try {
			insertedDocId = repo.storeAgreement("ExistingDocument.wsag", "<xml></xml>", session);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("El documento insertado tiene como id: "+insertedDocId);
		assertTrue(insertedDocId == 0);
	}
	
	@Test
	public void updatePrivate(){
		System.out.println("Executando updatePrivate()");
		boolean updated = false;
		try {
			updated = repo.updateAgreement(PRIVATE_DOC_ID_TO_UPDATE, "UpdatedPrivateDoc.wsag", "<xml>Updated private document</xml>", session);
			if(updated){ //Deshacemos los cambios
				repo.updateAgreement(PRIVATE_DOC_ID_TO_UPDATE, "ExistingDocument.wsag", "<xml></xml>", session);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(updated);
	}
	
	@Test
	public void updatePrivateExistingName(){
		System.out.println("Executando updatePrivateExistingName()");
		boolean updated = false;
		try {
			updated = repo.updateAgreement(PRIVATE_DOC_ID_TO_UPDATE, "ExistingDocument.wsag", "", session);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(updated);
	}
}