/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.users;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

@RunWith(Parameterized.class)
public class TestUpdateUser {
	
	private UsersManager um;
	private SessionManager sm;

	private int userId;
	private String username;
	private String password;
	private String newUsername;
	private String newPass;
	private String company;
	private String email;
	private boolean expectedResult;

	public TestUpdateUser(int userId, String username, String password, String newUsername, String newPass, String company, String email, boolean expectedResult) {
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.newUsername = newUsername;
		this.newPass = newPass;
		this.company = company;
		this.email = email;
		this.expectedResult = expectedResult;
	}

	@Before
	public void setUp() {
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		um = new ADAUsersManager(db);
		sm = new ADASessionManager(db);
	}

	@Parameters
	public static Collection<Object[]> getParameters() {
		Object[] validUpdate = new Object[] {3, "pepe", "pepePass", "pp", "ppPass", "pepeCompaņia", "pepeCorreo", true};

		Collection<Object[]> param = new LinkedList<Object[]>();
		param.add(validUpdate);
		return param;
	}

	@Test
	public void updateUser() {
		//primero hay que abrir la sesion del usuario que queremos borrar
		String session = sm.login(username, password);
		boolean result = um.updateUser(userId, newUsername, newPass, company, email, session);
		assertEquals(expectedResult, result);
		um.updateUser(userId, username, password, company, email, session);
	}
}
