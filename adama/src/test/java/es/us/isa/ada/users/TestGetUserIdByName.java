/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.users;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

@RunWith(Parameterized.class)
public class TestGetUserIdByName {
	
	private UsersManager um;
	
	private String username;
	private int expectedId;
	
	public TestGetUserIdByName(String username, int expectedId){
		this.username = username;
		this.expectedId = expectedId;
	}
	
	@Before
	public void setUp(){
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		um = new ADAUsersManager(db);
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Object[] validNameAndId = new Object[] {"Antonio", 2};
		Object[] validNameNoId = new Object [] {"Antonio", 3};
		Object[] invalidName = new Object [] {"a", 0};
		Object[] juan = new Object[] {"juan", 4};
		
		Collection<Object[]> param = new LinkedList<Object[]>();
		param.add(validNameAndId);
		param.add(validNameNoId);
		param.add(invalidName);
		param.add(juan);
		return param;
	}
	
	@Test
	public void getUserByName(){
		int id = um.getUserIdByName(username);
		assert(id == expectedId);
	}
}
