/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;

import org.junit.Test;

import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

public class TestConnection {
	
	private DBConnection databaseConnection;
	
	@Test
	public void testConnectionToDB(){
		databaseConnection = new ADADBConnection();
		Connection conn = databaseConnection.connect();
		assertNotNull("La conexi�n no se pudo iniciar", conn);
		databaseConnection.disconnect(conn);
		boolean cClosed = false;
		try {
			cClosed = conn.isClosed();
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue("La conexi�n no se cerr�", cClosed);
	}
	
	@Test
	public void testConnectionToLocalDB(){
		databaseConnection = new ADADBConnection("resources/localDB.properties");
		Connection conn = databaseConnection.connect();
		assertNotNull("La conexi�n no se pudo iniciar", conn);
		databaseConnection.disconnect(conn);
		boolean cClosed = false;
		try {
			cClosed = conn.isClosed();
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue("La conexi�n no se cerr�", cClosed);
	}
}
