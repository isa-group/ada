/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.users;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

@RunWith(Parameterized.class)
public class TestAddUser {

	private UsersManager userMan;
	
	private String username;
	private String password;
	private String company;
	private String email;
	
	private int expectedResult;
	
	public TestAddUser(String username, String pass, String company, String email, int expectedResult){
		this.username = username;
		this.password = pass;
		this.company = company;
		this.email = email;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp(){
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		userMan = new ADAUsersManager(db);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Object[] validUser = new Object[] {"pepe", "pepePass", "pepeComp", "pepeMail", 3};
		Object[] existingName = new Object[] {"Antonio", "aa", "aa", "aa", 0};
		Object[] nullPassword = new Object[] {"loQuesea", null, "loQSea", "loQSea", 0};
		Object[] nullCompany = new Object[] {"juan", "juanPass", null, "juanMail", 4};
		
		Collection<Object[]> param = new LinkedList<Object[]>();
		param.add(validUser);
		param.add(existingName);
		param.add(nullPassword);
		param.add(nullCompany);
		return param;
	}
	
	@Test
	public void addUser(){
		int result = userMan.addUser(username, password, company, email);
		assertEquals(expectedResult, result);
	}
}
