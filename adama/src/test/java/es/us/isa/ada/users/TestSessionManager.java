/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.users;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

@RunWith(Parameterized.class)
public class TestSessionManager {

	private SessionManager sm;
	
	private String username;
	private String password;
	private boolean expectedLoginResult;
	private boolean expectedLogoutResult;
	
	private static String auxSession;
	
	public TestSessionManager(String username, String password, boolean expectedLoginResult, boolean expectedLogoutResult){
		this.username = username;
		this.password = password;
		this.expectedLoginResult = expectedLoginResult;
		this.expectedLogoutResult = expectedLogoutResult;
	}
	
	@Before
	public void setUp(){
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		sm = new ADASessionManager(db);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Object[] validLogin = new Object[] {"Antonio", "ajurado", true, true};
		Object[] invalidUsername = new Object[] {"antonio", "ajurado", false, false};
		Object[] invalidPass = new Object[] {"Antonio", "jurado", false, false};
		Object[] nullParams = new Object[] {null, null, false, false};
		
		Collection<Object[]> param = new LinkedList<Object[]>();
		param.add(validLogin);
		param.add(invalidUsername);
		param.add(invalidPass);
		param.add(nullParams);
		return param;
	}
	
	@Test
	public void login(){
		auxSession = sm.login(username, password);
		if(auxSession == null){
			assertFalse(expectedLoginResult);
		}else{
			assertTrue(expectedLoginResult);
		}
	}
	
	@Test
	public void logout(){
		boolean result = sm.logout(auxSession);
		assertEquals(expectedLogoutResult, result);
	}
}
