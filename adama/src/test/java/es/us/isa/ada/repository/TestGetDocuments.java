/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.repository;

import java.util.Map.Entry;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import es.us.isa.ada.users.ADASessionManager;
import es.us.isa.ada.users.SessionManager;
import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

public class TestGetDocuments {
	
	private AgreementRepository repo;
	private static String session;
	
	@Before
	public void setUp(){
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		repo = new ADAAgreementRepository(db);
		SessionManager sm = new ADASessionManager(db);
		session = sm.login("Antonio", "ajurado").toString();
	}
	
	@Test
	public void getPublicDocuments(){
		System.out.println("########### Ejecutando getPublicAgreements ###########");
		Set<Entry<Integer, DocumentProxy>> docs = null;
		try {
			docs = repo.getPublicAgreements().entrySet();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(docs != null){
			for(Entry<Integer, DocumentProxy> doc:docs){
				System.out.println("### Documento id: "+doc.getKey()+" ###");
				System.out.println("    - Nombre: "+doc.getValue().getName());
			}
		}else{
			System.err.println("No se han cargado los documentos desde el repositorio");
		}
	}
	
	@Test
	public void getAgreementsByUser(){
		System.out.println("########### Ejecutando getAgreementsByUser ###########");
		Set<Entry<Integer, DocumentProxy>> docs = null;
		try {
			docs = repo.getAgreementsByUser(session).entrySet();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(docs != null){
			for(Entry<Integer, DocumentProxy> doc:docs){
				System.out.println("### Documento id: "+doc.getKey()+" ###");
				System.out.println("    - Nombre: "+doc.getValue().getName());
			}
		}else{
			System.err.println("No se han cargado los documentos desde el repositorio");
		}
	}
	
	@Test
	public void getAgreementById(){
		System.out.println("########### Ejecutando getAgreementById ###########");
		DocumentProxy doc = null;
		try {
			doc = repo.getAgreementById(2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(doc != null){
			System.out.println("### Documento ###");
			System.out.println("    - Nombre: "+doc.getName());
		}else{
			System.err.println("No se han cargado los documentos desde el repositorio");
		}
	}
}
